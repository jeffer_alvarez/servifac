const { PgConexion } = require("./conexion.js")
const pool = PgConexion()
var serviceAccount = require("../../push-notification.json");
var admin = require("firebase-admin");
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://push-notification-7c993.firebaseio.com"
});

async function getUser(token) {
    const user = await pool.query(`SELECT * FROM authtoken_token INNER JOIN users_user ON users_user.id=authtoken_token.user_id WHERE authtoken_token.key='${token}'`)
    const count = user.rowCount
    if (Number(count) == 1) {
        return user.rows
    } else {
        return null
    }


}

async function enviarNotificacion(token,title,body,image,icon,link){
  
    var message = {
    "notification": {
        "title": `${title}`,
        "body": `${body}`,
        "image": `${image}`

    },
    "webpush": {
        "headers": {
            "Urgency": "high"
        },
        "fcm_options": {
            "link": `${link}`
          },
        "notification": {
            "body": `${body}`,
            "requireInteraction": "true",
            "badge": `${icon}`,
            "icon":`${icon}`,   
        }
    },
    token: token
}
try {
    let respuesta =  await admin.messaging().send(message)    
    return respuesta
} catch (error) {
    return null
}

    
}


module.exports = {
    getUser, enviarNotificacion
}