const Producto = `type Producto{establecimiento:Int
imagen:String
card:String
thumbnail:String
codigo_producto:String
codigo_barra:String
descripcion:String
cantidad:Int
precio_costo:Float
valor_iva:Float
iva:Boolean
porcentaje_iva:Float
precio_a:Float
precio_b:Float
precio_c:Float
stock_minimo:Int
fecha_vencimiento:Date
fraccionable:Boolean
estado_stock:String}
`


module.exports = {
    Producto
}