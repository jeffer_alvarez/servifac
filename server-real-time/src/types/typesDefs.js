const { ApolloServer, gql, PubSub, withFilter } = require('apollo-server');


const typeDefs = gql`
type Query {    
  ordeCompra(id: Int!,id_cliente:Int!): [OrdenCompra!]!
  allOrdenes(establecimiento:Int!):[OrdenCompra!]!
  sendSms(message: String,number:String,subject:String!):SendSms!
  sendNotifications(title:String,body:String,image:String,icon:String,link:String,is_propietario:Boolean,is_anonymus:Boolean,is_client:Boolean):MessageRetur!
  getProducto(id:Int!,id_producto_taxonomia:Int):[Producto!]!
  getTaxonomias(id_producto:Int!):[ProductoTaxonomia!]!
}
  type MessageRetur{
    estado:Boolean
    message: String
  }

  type SendSms{
    message:String!
    number:String!
    subject:String!
    success:String!
  }
  type RolCliente{
    razon_social:String!
    id:ID!
  }

  scalar Date
  type OrdenCompra {
    id:ID
    razon_social:String
    cliente_id:ID
    establecimiento:ID
    fecha:Date
    numero_orden:String
    precio_total:Float
    subtotal:Float
    pedido_despachado:Boolean
    pedido_realizado:Boolean
    pedido_incompleto:Boolean
    pedido_reconfirmado:Boolean
    pedido_entregado:Boolean
    pedido_cancelado:Boolean    
    pedido_transportando:Boolean    
    pedido_atendido:Boolean    
    tiempo_pedido_despachado:Date
    tiempo_pedido_realizado:Date
    tiempo_pedido_incompleto:Date
    tiempo_pedido_reconfirmado:Date
    tiempo_pedido_entregado:Date
    tiempo_pedido_cancelado:Date
    tiempo_pedido_transportando:Date    
    tiempo_pedido_atendido:Date 
    hash_url:String
    latitud:String
    longitud:String
    tipo_entrega:String
  }  

  type ProductoTaxonomia{
    id:Int
    nombre:String
    slug_name:String
  }

  type Notifications{
    title: String
    body: String
    image: String
    icon: String
    is_client:Boolean
    is_propietario:Boolean
  }

  type Taxonomia{
    nombre_taxonomia_id:Int
    tipo:String
  }

  type Producto{
    establecimiento_id:Int
    imagen:String
    card:String
    thumbnail:String
    codigo_producto:String
    codigo_barra:String
    descripcion:String
    cantidad:Int
    precio_costo:Float
    valor_iva:Float
    iva:Boolean
    porcentaje_iva:Float
    precio_a:Float
    precio_b:Float
    precio_c:Float
    stock_minimo:Int
    fecha_vencimiento:Date
    fraccionable:Boolean
    estado_stock:String
    taxonomias:[ProductoTaxonomia!]!
  }


  input OrdenCompraInput{
    fecha:Date
    numero_orden:String
    precio_total:Float
    subtotal:Float
    pedido_despachado:Boolean
    pedido_realizado:Boolean
    pedido_incompleto:Boolean
    pedido_reconfirmado:Boolean
    pedido_entregado:Boolean
    pedido_cancelado:Boolean
    pedido_transportando:Boolean    
    pedido_atendido:Boolean  
    tiempo_pedido_despachado:Date
    tiempo_pedido_realizado:Date
    tiempo_pedido_incompleto:Date
    tiempo_pedido_reconfirmado:Date
    tiempo_pedido_entregado:Date
    tiempo_pedido_cancelado:Date
    tiempo_pedido_transportando:Date    
    tiempo_pedido_atendido:Date  
    hash_url:String
    latitud:String
    longitud:String
    tipo_entrega:String
  }

  type Mutation {   
    changueOrdenCompra(id: Int!,tiempo:Date!,idUser:Int!,input:OrdenCompraInput!): [OrdenCompra]!    
  }
 

  type Subscription {
    updateOrcenCompra(ordenId: Int!): OrdenCompra
  }
`;


module.exports = { typeDefs }