
const { ApolloServer, gql, PubSub, withFilter, AuthenticationError } = require('apollo-server');
const { PgConexion } = require("../conecxiondb/conexion.js")

const { enviarNotificacion } = require("../conecxiondb/query_users.js")
const pool = PgConexion()
const moment = require('moment'); // require

const pubsub = new PubSub();
var AWS = require('aws-sdk');
require('dotenv').config();



const UPDATE_PEDIDO = 'UPDATE_PEDIDO';
const resolvers = {
    Query: {
        ordeCompra: async (_, args) => {
            const { id, id_cliente } = args;
            const data = await pool.query(
                `SELECT * FROM empresa_orden_compras WHERE id=${id} INNER JOIN users_rol_cliente ON users_rol_cliente.id=${id_cliente}`
            );

            return data.rows;
        },
        allOrdenes: async (_, args) => {
            const { establecimiento } = args;
            const data = await pool.query(
                `SELECT * FROM empresa_orden_compras INNER JOIN users_rol_cliente ON users_rol_cliente.id=empresa_orden_compras.cliente_id WHERE empresa_orden_compras.establecimiento_id=${establecimiento} `
            );

            return data.rows;
        },
        sendSms: async (_, args) => {
            console.log("Message = " + args.message);
            console.log("Number = " + args.number);
            console.log("Subject = " + args.subject);
            var params = {
                Message: args.message,
                PhoneNumber: '+' + args.number,
                MessageAttributes: {
                    'AWS.SNS.SMS.SenderID': {
                        'DataType': 'String',
                        'StringValue': args.subject
                    }
                }
            };
            var publishTextPromise = new AWS.SNS({ apiVersion: '2010-03-31' }).publish(params).promise();
            var data = await publishTextPromise

            args.success = "ok"

            return args
        },
        sendNotifications: async (_, args) => {
            const {title,body,image,icon,is_client, is_propietario,link,is_anonymus}= args
            let data=[]
            if (is_client && is_propietario){
                data = await pool.query(
                    `SELECT token,is_client, is_propietario FROM empresa_notifications WHERE (is_propietario=true) AND (is_client=true)`
                );
                
            }else if(is_client){                
              data = await pool.query(
                    `SELECT token,is_client, is_propietario FROM empresa_notifications WHERE is_client=true`
                );

            }else if(is_propietario){
                data = await pool.query(
                    `SELECT token,is_client, is_propietario FROM empresa_notifications WHERE is_propietario=true`
                );
            }else if(is_anonymus){
                data = await pool.query(
                    `SELECT token,is_client, is_propietario,is_anonymus FROM empresa_notifications WHERE is_anonymus=true`
                );
            }else{
                    data = await pool.query(
                        `SELECT token,is_client, is_propietario,is_anonymus FROM empresa_notifications`
                    );
            }
           let respue= await Promise.all(data.rows.map(async item=>{
                var token =item.token
                await enviarNotificacion(token,title,body,image,icon,link)               
                return true
            }))            
            
           return {estado:true,message:"Se enviaron las notificaciones"}
            
        },
        getProducto: async (_, args) => {
            const { id, id_producto_taxonomia } = args;
            const data = await pool.query(
                `SELECT * FROM empresa_producto WHERE id=${id} `
            );
            if (id_producto_taxonomia != undefined) {
                const data_taxonomias = await pool.query(
                    `SELECT empresa_nombretaxonomia.nombre,empresa_nombretaxonomia.id,empresa_nombretaxonomia.slug_name FROM empresa_productotipotaxonimia INNER JOIN empresa_taxonomia ON (empresa_taxonomia.id=empresa_productotipotaxonimia.taxonomia_id) INNER JOIN empresa_nombretaxonomia ON (empresa_nombretaxonomia.id=empresa_taxonomia.nombre_taxonomia_id) WHERE empresa_productotipotaxonimia.producto_id=${id} `
                );
                data.rows[0].taxonomias = data_taxonomias.rows
            } else {
                data.rows[0].taxonomias = []
            }

            return data.rows;
        },
        getTaxonomias: async (_, args) => {
            const { id_producto } = args;
            const data = await pool.query(
                `SELECT empresa_nombretaxonomia.nombre,empresa_nombretaxonomia.id,empresa_nombretaxonomia.slug_name FROM empresa_productotipotaxonimia INNER JOIN empresa_taxonomia ON (empresa_taxonomia.id=empresa_productotipotaxonimia.taxonomia_id) INNER JOIN empresa_nombretaxonomia ON (empresa_nombretaxonomia.id=empresa_taxonomia.nombre_taxonomia_id) WHERE empresa_productotipotaxonimia.producto_id=${id_producto} `
            );
            return data.rows;
        }
    },
    Mutation: {
        changueOrdenCompra: async (_, args, context) => {
            const { user_id, is_propietario } = context.user[0]
            const { id, tiempo, idUser } = args
            const objecto_graphql = args.input
            if (user_id != idUser) throw new Error('El ususario no pertenece a esta factura');
            pubsub.publish(UPDATE_PEDIDO, { ...args });
            const objecto_json = JSON.stringify(objecto_graphql)
            const objecto_formater = objecto_json.replace(/['}{"]+/g, '')
            const objecto_save = objecto_formater.replace(/[':]+/g, '=')
            const data = await pool.query(
                `UPDATE empresa_orden_compras SET ${objecto_save} WHERE id=${id}
              `
            );
            const data_result = await pool.query(
                `SELECT * FROM empresa_orden_compras WHERE id=${id} `
            );
            if (Number(tiempo) > 0) {
                console.log("entro")
                regresion(id, tiempo, args)
            }
            return data_result.rows;
        },
    },

    Subscription: {
        updateOrcenCompra: {
            subscribe: withFilter(
                () => pubsub.asyncIterator([UPDATE_PEDIDO]),
                (payload, args) => {
                    return (!args.ordenId || payload.id == args.ordenId)
                },
            ),
            resolve: (payload) => {
                return (payload.input)
            }
        },
    },


}



function regresion(idPedido, tiempo, objecto_graphql) {
    var duration = moment.duration({
        'minutes': tiempo,
        'seconds': 0
    });
    let data = JSON.stringify(objecto_graphql.input)
    var timestamp = new Date(0, 0, 0, 2, 10, 30);
    var interval = 1;
    let objecto_formater = JSON.parse(data)
    let compara = null;
    var timer = setInterval(async function () {
        timestamp = new Date(timestamp.getTime() + interval * 1000);
        duration = moment.duration(duration.asSeconds() - interval, 'seconds');
        var min = duration.minutes();
        var sec = duration.seconds();

        sec -= 1;
        if (min < 0) return clearInterval(timer);
        if (min < 10 && min.length != 2) min = '0' + min;
        if (sec < 0 && min != 0) {
            min -= 1;
            sec = 59;
        } else if (sec < 10 && sec.length != 2) sec = '0' + sec;



        for (const valor in objecto_formater) {
            if (valor.toString().includes("tiempo_")) {
                if (compara == null || compara == objecto_formater[valor]) {
                    if (objecto_formater[valor] > 0 || objecto_formater[valor] != "00:00") {
                        objecto_formater[valor] = min + ':' + sec
                    }
                }
                //  objecto_formater[valor] = min + ':' + sec
            }
        }
        objecto_graphql.input = objecto_formater
        pubsub.publish(UPDATE_PEDIDO, { ...objecto_graphql });
        if (min == 0 && sec == 0) {
            for (const valor in objecto_formater) {
                if (valor.toString().includes("tiempo_")) {
                    if (compara == null || compara == objecto_formater[valor]) {
                        if (objecto_formater[valor] == "00:00") {
                            objecto_formater[valor] = 0
                            const cambiar = valor.substr(7, valor.length)
                            objecto_formater[cambiar] = true
                        }
                    }
                    //  objecto_formater[valor] = min + ':' + sec
                }
            }
            objecto_graphql.input = objecto_formater
            const objecto_json = JSON.stringify(objecto_graphql.input)
            const objecto_formater_string = objecto_json.replace(/['}{"]+/g, '')
            const objecto_save = objecto_formater_string.replace(/[':]+/g, '=')
            pubsub.publish(UPDATE_PEDIDO, { ...objecto_graphql });
            const data = await pool.query(
                `UPDATE empresa_orden_compras SET ${objecto_save} WHERE id=${idPedido}
               `
            );
            clearInterval(timer);
        }
    }, 1000);
}




module.exports = { resolvers }