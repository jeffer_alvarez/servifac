const { ApolloServer, gql, PubSub, withFilter, AuthenticationError } = require('apollo-server');

const { getUser,enviarNotificacion } = require("./conecxiondb/query_users.js")


const { typeDefs } = require("./types/typesDefs.js")
const { resolvers } = require("./resolvers/resolvers.js")

const server = new ApolloServer({
  typeDefs, resolvers, context: async ({ req, connection }) => {
    if (connection) {
      return {
        ...connection.context,
      };
    } else {
      var token = req.headers.authorization || '';
      // try to retrieve a user with the token
      if (!token) {
        token = ""
      }
      const user = await getUser(token);

      // optionally block the user
      // we could also check user roles/permissions here


      // add the user to the context
      return { user };
    }
    // get the user token from the headers

  },
})

server.listen().then(({ url }) => {
  console.log(`Server is ready at ${url}`)
});
