export COMPOSE_FILE=local.yml
docker-compose up
docker-compose down
docker volume prune
docker-compose run --rm django python manage.py makemigrations
docker-compose run --rm django python manage.py migrate
docker-compose run --rm --service-ports django