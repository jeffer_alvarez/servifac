from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path
from django.views import defaults as default_views
from django.views.generic import TemplateView
from rest_framework.authtoken.views import obtain_auth_token

# Url
from servifac.users.urls import users_patterns
from servifac.empresa.urls import empresa_patterns

from graphene_django.views import GraphQLView
from django.views.decorators.csrf import csrf_exempt

from channels.routing import ProtocolTypeRouter, URLRouter
from graphene_subscriptions.consumers import GraphqlSubscriptionConsumer

print("sad")
application = ProtocolTypeRouter({
    "websocket":
    URLRouter([path('perra/', GraphqlSubscriptionConsumer)]),
})

urlpatterns = [
    #PATH USERS
    path('', include(users_patterns)),

    # empresa
    path('', include(empresa_patterns)),
    path(settings.ADMIN_URL, admin.site.urls),
    path('', include(
        ('servifac.sistema.urls', 'sistema'), namespace="sistema")),
    path('graphql/', csrf_exempt(GraphQLView.as_view(graphiql=True)))
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:

    if "debug_toolbar" in settings.INSTALLED_APPS:
        import debug_toolbar

        urlpatterns = [path("__debug__/", include(debug_toolbar.urls))
                       ] + urlpatterns
