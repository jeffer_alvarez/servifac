import graphene
from servifac.users import schema
import asyncio
from datetime import datetime
from config.graphql_jwt import ObtainJSONWebToken, Verify, Refresh

from django.contrib.auth.mixins import LoginRequiredMixin


class Query(schema.Query, graphene.ObjectType):
    pass


class Mutation(schema.Mutation, graphene.ObjectType, LoginRequiredMixin):

    pass


class Subscription(graphene.ObjectType):
    time_of_day = graphene.Field(graphene.String)

    def resolve_time_of_day(root, info):
        return "szdf"


schema = graphene.Schema(
    query=Query,
    mutation=Mutation,
    subscription=Subscription,
)
