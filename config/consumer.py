from channels import Group
from channels.sessions import channel_session


@channel_session
def ws_connect(message):
    prefix, label = message['path'].strip('/').split('/')

    Group('chat-' + label).add(message.reply_channel)
    message.channel_session['room'] = "Jeffrinpro"


@channel_session
def ws_receive(message):
    label = message.channel_session['room']
    data = json.loads(message['text'])
    Group('chat-' + label).send({'text': "dsad"})


@channel_session
def ws_disconnect(message):
    label = message.channel_session['room']
    Group('chat-' + label).discard(message.reply_channel)
