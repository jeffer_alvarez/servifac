# Generated by Django 3.0 on 2020-06-04 17:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('empresa', '0006_auto_20200603_2121'),
    ]

    operations = [
        migrations.AlterField(
            model_name='orden_compras',
            name='pedido_atendido',
            field=models.BooleanField(default=None, verbose_name='pedido atendido'),
        ),
        migrations.AlterField(
            model_name='orden_compras',
            name='pedido_cancelado',
            field=models.BooleanField(default=None, verbose_name='pedido cancelado'),
        ),
        migrations.AlterField(
            model_name='orden_compras',
            name='pedido_despachado',
            field=models.BooleanField(default=None, verbose_name='Pedido despachado'),
        ),
        migrations.AlterField(
            model_name='orden_compras',
            name='pedido_entregado',
            field=models.BooleanField(default=None, verbose_name='pedido entregado'),
        ),
        migrations.AlterField(
            model_name='orden_compras',
            name='pedido_incompleto',
            field=models.BooleanField(default=None, verbose_name='Pedido imcompleto'),
        ),
        migrations.AlterField(
            model_name='orden_compras',
            name='pedido_realizado',
            field=models.BooleanField(default=None, verbose_name='Pedido realizado'),
        ),
        migrations.AlterField(
            model_name='orden_compras',
            name='pedido_reconfirmado',
            field=models.BooleanField(default=None, verbose_name='Pedido reconfirmado'),
        ),
        migrations.AlterField(
            model_name='orden_compras',
            name='pedido_transportando',
            field=models.BooleanField(default=None, verbose_name='pedido transportando'),
        ),
    ]
