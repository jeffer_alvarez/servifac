from rest_framework.permissions import BasePermission

from servifac.empresa.models import Empresa
class IsAccountOwner(BasePermission):
    def has_permission(self, request, view):
        print("user", request.user)
        obj =view.get_object()
        print("objecto",obj)
        return bool(request.user.is_authenticated and request.user.is_propietario)
    

class IsEmpresaEncontrada(BasePermission):

    def has_permission(self, request, view):
        try:
            propietario=request.user.propietario            
        except:
            return False
        obj = view.get_object()
       
        try:
            Empresa.objects.get(pk=obj.id,propietario=propietario.id)
        except Empresa.DoesNotExist:
            return False
        
        return True
    
class IsSelfMember(BasePermission):
    def has_permission(self, request, view):
        print("entro perra")               
        obj=view.get_object()
        return self.has_object_permission(request,view,obj)

    def has_object_permission(self, request, view, obj):
        print(obj)      
        return False