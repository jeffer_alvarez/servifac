#rest framework
from rest_framework.permissions import BasePermission

#models
from servifac.empresa.models import Establecimiento, Empresa

class IsPropietario(BasePermission):
    """
    Verificar si el usuario tien el rol propietario

    """
    def has_permission(self, request, view):
        return bool(request.user.is_authenticated and request.user.is_propietario)

class IsPropietarioEstablecimiento(BasePermission):
    
    """
    Verificar si los establecimientos les pertenecen al propietario
    
    """
    def has_object_permission(self, request, view, obj):
        empresa = request.user.propietario.empresa.pk
        establecimiento_id = obj.id
        establecimiento = Establecimiento.objects.filter(pk=establecimiento_id,empresa__propietario=empresa)
        return establecimiento.exists()

class IsPropietarioEstablecimientoTaxonomia(BasePermission):
    
    """
    Verificar si los establecimientos les pertenecen al propietario
    
    """
    def has_object_permission(self, request, view, obj):
        empresa = request.user.propietario.empresa.pk
        establecimiento_id = obj.producto.establecimiento.id
        establecimiento = Establecimiento.objects.filter(pk=establecimiento_id,empresa__propietario=empresa)
        return establecimiento.exists()

class IsPropietarioEstablecimientoTaxonomiaNombres(BasePermission):
    
    """
    Verificar si los establecimientos les pertenecen al propietario
    
    """
    def has_object_permission(self, request, view, obj):
        empresa = request.user.propietario.empresa.pk
        establecimiento_id = obj.empresa.id
        establecimiento = Establecimiento.objects.filter(pk=establecimiento_id,empresa__propietario=empresa)
        return establecimiento.exists()