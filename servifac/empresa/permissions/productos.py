#rest framework
from rest_framework.permissions import BasePermission
from django.http import JsonResponse
#models
from servifac.empresa.models import Producto, Establecimiento

class IsProductoEstablecimientoEmpresa(BasePermission):

    """
    Verificar que los productos pertenezcan a los establecimientos de propietario
    """
    
    def has_permission(self, request, view):
        empresa = request.user.propietario.empresa.pk  
        print("Empresa: ", empresa)

        data_producto_dic = dict(request.data)
        print(data_producto_dic)
        imagen=data_producto_dic.get('imagen',None)
        if imagen == None:
            numero_establecimiento=data_producto_dic['establecimiento']            
            try:
                Establecimiento.objects.get(pk=numero_establecimiento, empresa=empresa)
            except Establecimiento.DoesNotExist:
                return False
        else:
            numero_establecimiento=data_producto_dic['establecimiento']            
            try:
                Establecimiento.objects.get(pk=int(numero_establecimiento[0]), empresa=empresa)
            except Establecimiento.DoesNotExist:
                return False
            
        return True


