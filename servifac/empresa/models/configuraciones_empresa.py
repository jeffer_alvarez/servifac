"""Model, Contiene a CAJA"""

# django
from django.db import models
from django.utils.translation import ugettext_lazy as _

# models
from servifac.utils.models import UtilModel


class ConfiguracionEmpresa(models.Model):
    empresa = models.ForeignKey('empresa.Empresa',
                                related_name='ConfiguracionEmpresa',
                                verbose_name=_("FK Empresa"),
                                on_delete=models.CASCADE)
    primary = models.CharField(_("Primary color"),
                               max_length=150,
                               blank=True,
                               null=True)
    secondary = models.CharField(_("Secondary color"),
                                 max_length=150,
                                 blank=True,
                                 null=True)
    dark = models.CharField(_("Dark color"),
                            max_length=150,
                            blank=True,
                            null=True)
    accent = models.CharField(_("Accent color"),
                              max_length=150,
                              blank=True,
                              null=True)
    link = models.CharField(_("Link color"),
                            max_length=150,
                            blank=True,
                            null=True)
    servicio_domicilio = models.BooleanField(_("Servicio Domicilio"),
                                             default=False)
    code_pais = models.CharField(_("Code Pais"),
                                 max_length=10,
                                 blank=True,
                                 null=True)
    pais = models.CharField(_("Pais"), max_length=15, blank=True, null=True)
    horario_atencion = models.CharField(_("Horario de atencion"),
                                        max_length=250,
                                        blank=True,
                                        null=True)
