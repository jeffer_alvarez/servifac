"""Model, Contine a Empresas.
"""

# django
from django.db import models
from django.utils.translation import ugettext_lazy as _

# Models
from servifac.utils.models import UtilModel
from servifac.storage_backends import StaticStorage


class Empresa(UtilModel):
    """Model Empresa."""

    control_secuencial = models.OneToOneField(
        'empresa.ControlSecuencial',
        verbose_name=_("Fk Control Secuencial"),
        on_delete=models.CASCADE)
    propietario = models.OneToOneField('users.Rol_propietario',
                                       on_delete=models.CASCADE)
    contador = models.ForeignKey('users.Rol_contador',
                                 verbose_name=_("Fk Contador"),
                                 on_delete=models.CASCADE,
                                 blank=True,
                                 null=True)
    clave_firma_electrinica = models.CharField(_("Clave Firma Electronica"),
                                               max_length=25,
                                               blank=True,
                                               null=True)

    tipo_negocio = models.CharField(_("Tipo de negocio"),
                                    max_length=255,
                                    blank=True,
                                    null=True)

    indexar = models.CharField(_("Indexar"),
                               max_length=255,
                               blank=True,
                               null=True)

    mostrar_precios = models.CharField(_("Mostrar Precios"),
                                       max_length=255,
                                       blank=True,
                                       null=True)

    firma_electronica = models.FileField(
        _("Firma Electronica"),
        upload_to='empresa/firma_electronicas/',
        max_length=255,
        blank=True,
        null=True)
    ambiente = models.CharField(_("Ambiente"), max_length=1, default=1)
    contribuyente_especial = models.CharField(_("Contribuyente especial"),
                                              max_length=6,
                                              default="12345",
                                              blank=True,
                                              null=True)
    direccion = models.CharField(_("Direccion"), max_length=150)
    obligado_contabilidad = models.NullBooleanField(
        _("Obligado a llevar Contabilidad"), blank=True, null=True)
    logo = models.FileField(_("Logo"),
                            storage=StaticStorage(),
                            max_length=500,
                            blank=True,
                            null=True)

    logo_small = models.FileField(_("Logo small"),
                                  storage=StaticStorage(),
                                  max_length=500,
                                  blank=True,
                                  null=True)

    logo_large = models.FileField(_("Logo large"),
                                  storage=StaticStorage(),
                                  max_length=500,
                                  blank=True,
                                  null=True)

    def __str__(self):
        return str(self.propietario.nombre_comercial)
