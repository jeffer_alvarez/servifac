"""Model, Contiene a log_transaccion"""

# django
from django.db import models
from django.utils.translation import ugettext_lazy as _

# models
from servifac.utils.models import UtilModel


class log_transaccion(UtilModel):
    """Model log_transaccion."""

    caja = models.ForeignKey(
        'empresa.Caja',
        verbose_name=_("Caja"),
        on_delete=models.CASCADE
        )
    justificacion_ingreso_egreso = models.OneToOneField(
        'empresa.Transaccion_establecimeinto_ingreso_gasto',
        verbose_name=_("Justificacion ingresoso y egresos"),
        on_delete=models.CASCADE
        )
    tipo_transaccion = models.CharField(_("Tipo de transaccion"), max_length=50)
    valor = models.DecimalField(_("Valor"), max_digits=10, decimal_places=2)
    referencia = models.IntegerField(_("Referencia"))
    descripcion = models.CharField(_("Descripcion"), max_length=150)
    banco = models.CharField(_("Banco"), max_length=50)
    numero_cuenta = models.CharField(_("Numero Cuenta"), max_length=50)
    
    #factura = models.ForeignKey('comprobantes_sri.Factura',verbose_name=_("Factura"),on_delete=models.CASCADE)
    #log_abono = models.ForeignKey('comprobantes_sri.Log_abono',verbose_name=_("Log Abono"),on_delete=models.CASCADE)
    # nota_credito: NotaCredito
    # nota_debito: NotaDebito

    def __str__(self):
        return self.descripcion
