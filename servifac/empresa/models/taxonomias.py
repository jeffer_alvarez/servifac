"""Model, Contiene a Taxonomia """

# django
from django.db import models
from django.utils.translation import ugettext_lazy as _

# models
from servifac.utils.models import UtilModel


class Taxonomia(UtilModel):
    """Model Taxonomia ."""
    empresa = models.ForeignKey(
        'empresa.Empresa',
        related_name='taxonomia',
        verbose_name=_("FK Empresa"),
        on_delete=models.CASCADE
        )
    nombre_taxonomia = models.ForeignKey(
        'empresa.NombreTaxonomia',
        related_name='taxonomia_nombre',
        verbose_name=_("FK Nombre taxonomia"),
        on_delete=models.CASCADE,
       blank=True, null=True
        )
    nombre = models.CharField(_("Nombre"), max_length=150)
    parent = models.IntegerField(_("Parent"))
    tipo = models.CharField(_("Tipo Taxonomia"), max_length=50)

    def __str__(self):
        return self.nombre
