"""Model, Contiene a Historial  Compra Ventas Productos"""

# django
from django.db import models
from django.utils.translation import ugettext_lazy as _

# models
from servifac.utils.models import UtilModel


class Log_compra_venta_producto(UtilModel):
    """Model Historial Compra Ventas Productos."""

    producto = models.ForeignKey(
        'empresa.Producto',
        verbose_name=_("Producto"),
        on_delete=models.CASCADE
        )
    factura = models.IntegerField(_("Factura"))
    tipo = models.CharField(_("Tipo"), max_length=50)
    precio_anterior = models.DecimalField(_("Precio Anterior"), max_digits=10, decimal_places=2)
    ganacia_neta = models.DecimalField(_("Ganancia Neta"), max_digits=10, decimal_places=2)
    stock_actual = models.PositiveIntegerField(_("Stock Actual"))
    stock_anterior = models.PositiveIntegerField(_("Stock Anterior"))

    def __str__(self):
        return self.producto
