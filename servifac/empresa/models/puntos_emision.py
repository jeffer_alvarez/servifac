"""Model, Contiene a puntos de emision"""

# django
from django.db import models
from django.utils.translation import ugettext_lazy as _

# models
from servifac.utils.models import UtilModel


class Punto_emision(UtilModel):
    """Model Productos Proformas."""

    """ empleado = models.ForeignKey(
        'users.Rol_empleado',
        verbose_name=_("Empleado"),
        on_delete=models.CASCADE
        ) """
    control_secuencial = models.OneToOneField(
        'empresa.ControlSecuencial',
        verbose_name=_("Fk Control Secuencial"),
        on_delete=models.CASCADE
        )
    establecimiento = models.ForeignKey(
        'empresa.Establecimiento',
        verbose_name=_("Establecimiento"),
        on_delete=models.CASCADE
        )
    numero_punto_emision = models.CharField(_("Numero Punto de Emision"), max_length=50)
    saldo_anterior = models.DecimalField(_("Saldo Anterior"), max_digits=10, decimal_places=2)

    def __str__(self):
        return self.id
 