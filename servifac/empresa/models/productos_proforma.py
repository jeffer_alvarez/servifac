"""Model, Contiene a productos proformas"""

# django
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Producto_proforma(models.Model):
    """Model Productos Proformas."""

    proforma = models.ForeignKey(
        'empresa.Proforma',
        verbose_name=_("Producto"),
        on_delete=models.CASCADE
        )
    producto = models.OneToOneField(
        'empresa.Producto',
        verbose_name=_("Producto"),
        on_delete=models.CASCADE
        )
    cantidad = models.PositiveIntegerField(_("Cantidad"))
    precio_total = models.DecimalField(_("Precio Total"), max_digits=10, decimal_places=2)
    subtotal = models.DecimalField(_("Subtotal"), max_digits=10, decimal_places=2)

    def __str__(self):
        return self.id
