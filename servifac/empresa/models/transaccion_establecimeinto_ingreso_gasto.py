"""Model, Contiene a transaccion_establecimeinto_ingreso_gasto"""

# django
from django.db import models
from django.utils.translation import ugettext_lazy as _

# models
from servifac.utils.models import UtilModel


class Transaccion_establecimeinto_ingreso_gasto(UtilModel):
    """Model transaccion_establecimeinto_ingreso_gasto."""

    establecimiento = models.ForeignKey(
        'empresa.Establecimiento',
        verbose_name=_("Establecimiento"),
        on_delete=models.CASCADE
        )
    tipo = models.CharField(_("Tipo Gasto"), max_length=50)
    nombre = models.CharField(_("Nombre"), max_length=50)
    valor_total = models.DecimalField(_("Valor Total"), max_digits=10, decimal_places=2)
    fecha = models.DateTimeField(_("Fecha"), auto_now=False, auto_now_add=False)
    slug_gasto = models.CharField(_("Slug"), max_length=50)

    def __str__(self):
        return self.nombre
