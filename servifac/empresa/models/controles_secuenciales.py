"""Model, Contine a control secuencial.
"""

# django
from django.db import models
from django.utils.translation import ugettext_lazy as _


class ControlSecuencial(models.Model):
    """Model Control Secuencial."""

    numero_facturas = models.PositiveIntegerField(_("Numero de facturas"),default=0)
    numero_retenciones = models.PositiveIntegerField(_("Numero de retenciones"),default=0)
    numero_notas_credito = models.PositiveIntegerField(_("Numero de notas de cridito"),default=0)
    numero_notas_debito = models.PositiveIntegerField(_("Numero de notas de debito"),default=0)
    nota_venta = models.PositiveIntegerField(_("Notas de Ventas"),default=0)
    numero_guias_remision = models.IntegerField(_("Numero de Guias de remision"),default=0)
    proformas = models.IntegerField(_("Proformas"))
    ordenes_compras = models.IntegerField(_("Ornedes de compras"),default=0)

    def __str__(self):
        return str(self.id)

        
