"""Model, Contiene a CAJA"""

# django
from django.db import models
from django.utils.translation import ugettext_lazy as _

# models
from servifac.utils.models import UtilModel


class Caja(UtilModel):
    """Model Caja."""

    punto_emision = models.ForeignKey(
        'empresa.Punto_emision',
        verbose_name=_("Punto Emision"),
        on_delete=models.CASCADE
        )
    saldo_inicial = models.DecimalField(_("Saldo Inicial"), max_digits=10, decimal_places=2)
    saldo_final = models.DecimalField(_("Saldo Final"), max_digits=10, decimal_places=2)
    total_retiro_dinero = models.DecimalField(_("Total Retiro de Dinero"), max_digits=10, decimal_places=2)
    total_ingreso_dinero = models.DecimalField(_("Total Ingreso Dinero"), max_digits=10, decimal_places=2)
    total_deposito_bancario = models.DecimalField(_("Total Deposito Bancario"), max_digits=10, decimal_places=2)
    
    fecha_hora_apertura = models.DateTimeField(_("Fecha-Hora Apertura"), auto_now=False, auto_now_add=False)
    fecha_hora_cierre = models.DateTimeField(_("Fecha-Hora Cierre"), auto_now=False, auto_now_add=False)
    # detalles_cierre_caja = 

    def __str__(self):
        return self.id
