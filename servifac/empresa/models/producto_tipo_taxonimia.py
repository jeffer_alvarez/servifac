"""Model, Contiene a establecimientos"""

# django
from django.db import models
from django.utils.translation import ugettext_lazy as _

# models
from servifac.utils.models import UtilModel


class ProductoTipoTaxonimia(UtilModel):
    producto = models.ForeignKey('empresa.Producto',
                                 related_name='tipo_producto',
                                 verbose_name=_("FK Producto"),
                                 on_delete=models.CASCADE,
                                 blank=True,
                                 null=True)

    taxonomia = models.ForeignKey('empresa.Taxonomia',
                                  related_name='taxonomia_taxonomia',
                                  verbose_name=_("FK tipo producto taxonomia"),
                                  on_delete=models.CASCADE,
                                  blank=True,
                                  null=True)

    def __str__(self):
        return str(self.producto.descripcion)
