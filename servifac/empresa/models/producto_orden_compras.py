"""Model, Contiene a Orden de compras"""

# django
from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.utils.translation import ugettext_lazy as _

# models
from servifac.utils.models import UtilModel


class Producto_orden_compra(UtilModel):
    """Model orden Compras."""
    orden_compras = models.ForeignKey(
        'empresa.Orden_compras',
        related_name= "orden_compra",
        verbose_name=_("Fk_orden_compras"),
        on_delete=models.CASCADE
        )
    producto = models.ForeignKey(
        'empresa.Producto',
        verbose_name=_("fk_Producto"),
        on_delete=models.CASCADE,blank=True, null=True,unique=False
    )
    cantidad = models.PositiveIntegerField(_("Cantidad"))
    cantidad_existente = models.PositiveIntegerField(_("Cantidad existente"),blank=True, null=True)
    precio_total = models.DecimalField(_("Precio total"), max_digits=10, decimal_places=2)
    precio_unitario = models.DecimalField(_("Sub total"), max_digits=10, decimal_places=2)

