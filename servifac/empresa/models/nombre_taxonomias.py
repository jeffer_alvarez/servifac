"""Model, Contiene a establecimientos"""

# django
from django.db import models
from django.utils.translation import ugettext_lazy as _

# models
from servifac.utils.models import UtilModel


class NombreTaxonomia(UtilModel):
    nombre = models.CharField(_("Nombre"), max_length=250)
    slug_name = models.CharField(_("slug_name"), max_length=250)

    def __str__(self):
        return self.nombre
