"""Model, Contiene a Orden de compras"""

# django
from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.utils.translation import ugettext_lazy as _

# models
from servifac.utils.models import UtilModel


class Orden_compras(UtilModel):
    """Model orden Compras."""

    cliente = models.ForeignKey('users.Rol_cliente',
                                verbose_name=_("Cliente"),
                                on_delete=models.CASCADE)
    fecha = models.DateTimeField(_("Fecha"), auto_now=True)
    establecimiento = models.ForeignKey('empresa.Establecimiento',
                                        verbose_name=_("FK establecimiento"),
                                        on_delete=models.CASCADE)
    numero_orden = models.CharField(_("Numero de Orden"), max_length=50)

    precio_total = models.DecimalField(_("Precio total"),
                                       max_digits=10,
                                       decimal_places=2)
    subtotal = models.DecimalField(_("Sub total"),
                                   max_digits=10,
                                   decimal_places=2)
    pedido_despachado = models.BooleanField(_("Pedido despachado"),
                                            default=None,
                                            null=True)
    pedido_realizado = models.BooleanField(_("Pedido realizado"),
                                           default=True,
                                           null=True)
    pedido_incompleto = models.BooleanField(_("Pedido imcompleto"),
                                            default=None,
                                            null=True)
    pedido_reconfirmado = models.BooleanField(_("Pedido reconfirmado"),
                                              default=None,
                                              null=True)
    pedido_entregado = models.BooleanField(_("pedido entregado"),
                                           default=None,
                                           null=True)
    pedido_cancelado = models.BooleanField(_("pedido cancelado"),
                                           default=None,
                                           null=True)

    pedido_transportando = models.BooleanField(_("pedido transportando"),
                                               default=None,
                                               null=True)

    pedido_atendido = models.BooleanField(_("pedido atendido"),
                                          default=None,
                                          null=True)

    tiempo_pedido_despachado = models.PositiveIntegerField(
        _("tiempo_pedido_despachado"), default=0)
    tiempo_pedido_realizado = models.PositiveIntegerField(
        _("tiempo_pedido_realizado"), default=0)
    tiempo_pedido_incompleto = models.PositiveIntegerField(
        _("tiempo_pedido_incompleto"), default=0)
    tiempo_pedido_reconfirmado = models.PositiveIntegerField(
        _("tiempo_pedido_reconfirmado"), default=0)
    tiempo_pedido_entregado = models.PositiveIntegerField(
        _("tiempo_pedido_entregado"), default=0)
    tiempo_pedido_cancelado = models.PositiveIntegerField(
        _("tiempo_pedido_cancelado"), default=0)
    tiempo_pedido_transportando = models.PositiveIntegerField(
        _("tiempo_pedido_transportando"), default=0)
    tiempo_pedido_atendido = models.PositiveIntegerField(
        _("tiempo_pedido_atendido"), default=0)

    hash_url = models.CharField(_("Hash url"), max_length=20)
    latitud = models.CharField(_("latitud"),
                               max_length=20,
                               blank=True,
                               null=True)
    longitud = models.CharField(_("longitud"),
                                max_length=20,
                                blank=True,
                                null=True)
    tipo_entrega = models.CharField(_("tipo_entrega"),
                                    max_length=20,
                                    blank=True,
                                    null=True)

    def __str__(self):
        return self.numero_orden

    class Meta:
        ordering = ('-created', )
