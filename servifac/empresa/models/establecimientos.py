"""Model, Contiene a establecimientos"""

# django
from django.db import models
from django.utils.translation import ugettext_lazy as _

# models
from servifac.utils.models import UtilModel


class Establecimiento(UtilModel):
    """Model Establecimiento."""

    empresa = models.ForeignKey('empresa.Empresa',
                                related_name='establecimiento',
                                verbose_name=_("FK Empresa"),
                                on_delete=models.CASCADE)
    control_secuencial = models.OneToOneField(
        'empresa.ControlSecuencial',
        verbose_name=_("Fk Control Secuencial"),
        on_delete=models.CASCADE)
    codigo = models.CharField(_("Codigo"), max_length=3)
    direccion = models.CharField(_("Direccion"),
                                 max_length=150,
                                 blank=True,
                                 null=True)
    parroquia = models.CharField(_("Parroquia"), max_length=150)
    canton = models.CharField(_("Canton"), max_length=150)
    ciudad = models.CharField(_("Ciudad"),
                              max_length=150,
                              blank=True,
                              null=True)
    latitud = models.CharField(_("Latitud"),
                               max_length=150,
                               blank=True,
                               null=True)
    longitud = models.CharField(_("Longitud"),
                                max_length=150,
                                blank=True,
                                null=True)

    def __str__(self):
        return str(self.id)
