"""Model, Contine a Galery.
"""

# django
from django.db import models
from django.utils.translation import ugettext_lazy as _

# Models
from servifac.utils.models import UtilModel
from servifac.storage_backends import StaticStorage


class Notifications(UtilModel):
    is_propietario = models.NullBooleanField(_("is propietario"),
                                             blank=True,
                                             null=True)

    is_client = models.NullBooleanField(_("is client"), blank=True, null=True)

    is_anonymus = models.NullBooleanField(_("anonymus"), blank=True, null=True)

    token = models.CharField(_("token"), max_length=250, blank=True, null=True)
    uid_device = models.CharField(_("uid device"),
                                  max_length=250,
                                  blank=True,
                                  null=True)
    id_user = models.CharField(_("id user"),
                               max_length=250,
                               blank=True,
                               null=True)
