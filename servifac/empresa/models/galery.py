"""Model, Contine a Galery.
"""

# django
from django.db import models
from django.utils.translation import ugettext_lazy as _

# Models
from servifac.utils.models import UtilModel
from servifac.storage_backends import StaticStorage


class Gallery(UtilModel):
    img_small = models.FileField(_("img small"),
                                 storage=StaticStorage(),
                                 max_length=500,
                                 blank=True,
                                 null=True)
    img_large = models.FileField(_("img small"),
                                 storage=StaticStorage(),
                                 max_length=500,
                                 blank=True,
                                 null=True)
    img_medium = models.FileField(_("img medium"),
                                  storage=StaticStorage(),
                                  max_length=500,
                                  blank=True,
                                  null=True)

    producto_id = models.CharField(_("Producto id"),
                                   max_length=255,
                                   blank=True,
                                   null=True)

    establecimiento_id = models.CharField(_("Establecimiento id"),
                                          max_length=255,
                                          blank=True,
                                          null=True)
