"""Model, Contiene a Proformas"""

# django
from django.db import models
from django.utils.translation import ugettext_lazy as _

# models
from servifac.utils.models import UtilModel


class Proforma(UtilModel):
    """Model  Proformas."""

    establecimiento = models.ForeignKey(
        'empresa.Establecimiento',
        verbose_name=_("Establecimiento"),
        on_delete=models.CASCADE
        )
    """ cliente = models.ForeignKey(
        'users.Rol_cliente',
        verbose_name=_("Cliente"),
        on_delete=models.CASCADE
        ) """
    fecha = models.DateTimeField(_("Fecha"), auto_now=False, auto_now_add=False)
    numero_proforma = models.CharField(_("Numero Proforma"), max_length=50)
    subtotal = models.DecimalField(_("Subtotal"), max_digits=10, decimal_places=2)
    iva = models.DecimalField(_("Iva"), max_digits=10, decimal_places=2)
    suma_total = models.DecimalField(_("Suma Total"), max_digits=10, decimal_places=2)
    titulo = models.CharField(_("Titulo"), max_length=50)
    descripcion = models.TextField(_("Descripcion"))

    def __str__(self):
        return self.titulo
