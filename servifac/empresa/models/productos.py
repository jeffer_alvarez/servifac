"""Model, Contiene a Productos"""

# django
from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.utils.translation import ugettext_lazy as _
from servifac.storage_backends import StaticStorage

# models
from servifac.utils.models import UtilModel


class Producto(UtilModel):
    """Model Productos."""

    establecimiento = models.ForeignKey(
        'empresa.Establecimiento',
        related_name='productos_establecimiento',
        verbose_name=_("FK establecimiento"),
        on_delete=models.CASCADE)
    imagen = models.FileField(_("Imagen"),
                              storage=StaticStorage(),
                              max_length=500,
                              blank=True,
                              null=True)
    card = models.FileField(_("Imagen Mediana"),
                            storage=StaticStorage(),
                            max_length=500,
                            blank=True,
                            null=True)
    thumbnail = models.FileField(_("Imagen Miniatura"),
                                 storage=StaticStorage(),
                                 max_length=500,
                                 blank=True,
                                 null=True)

    codigo_producto = models.CharField(_("Codigo Producto"),
                                       max_length=50,
                                       blank=True,
                                       null=True)
    codigo_barra = models.CharField(_("Codigo Barras"),
                                    max_length=50,
                                    blank=True,
                                    null=True)
    descripcion = models.TextField(_("Descripcion"), max_length=500)
    cantidad = models.PositiveIntegerField(_("Cantidad"),
                                           blank=True,
                                           null=True)
    precio_costo = models.DecimalField(_("Precio Costo"),
                                       max_digits=10,
                                       decimal_places=2,
                                       blank=True,
                                       null=True)
    valor_iva = models.DecimalField(_("Valor iva"),
                                    max_digits=10,
                                    decimal_places=2,
                                    blank=True,
                                    null=True)
    iva = models.NullBooleanField(_("iva"), blank=True, null=True)
    porcentaje_iva = models.PositiveIntegerField(_("Porcentaje Iva"),
                                                 blank=True,
                                                 null=True)
    precio_a = models.DecimalField(_("Precio A"),
                                   max_digits=10,
                                   decimal_places=2)
    precio_b = models.DecimalField(_("Precio B"),
                                   max_digits=10,
                                   decimal_places=2,
                                   blank=True,
                                   null=True)
    precio_c = models.DecimalField(_("Precio C"),
                                   max_digits=10,
                                   decimal_places=2,
                                   blank=True,
                                   null=True)
    stock_minimo = models.PositiveIntegerField(_("Stock Minimo"),
                                               blank=True,
                                               null=True)
    fecha_vencimiento = models.DateField(_("Fecha Vencimiento"),
                                         auto_now=False,
                                         auto_now_add=False,
                                         blank=True,
                                         null=True)
    fraccionable = models.NullBooleanField(_("Fraccionable"),
                                           blank=True,
                                           null=True)
    estado_stock = models.CharField(_("estado stock"),
                                    max_length=50,
                                    blank=True,
                                    null=True)
    # arrays
    taxonomias_categoria = ArrayField(models.IntegerField(),
                                      blank=True,
                                      null=True)
    taxonomias_tags = ArrayField(models.IntegerField(), blank=True, null=True)

    def __str__(self):
        return self.descripcion
