"""Model, Contiene a Proveedores"""

# django
from django.db import models
from django.utils.translation import ugettext_lazy as _

# models
from servifac.utils.models import UtilModel


class Proveedor(UtilModel):
    """Model Proveedores."""

    establecimiento = models.ForeignKey(
        'empresa.Establecimiento',
        verbose_name=_("FK establecimiento"),
        on_delete=models.CASCADE
        )
    razon_social = models.CharField(_("Razon Social"), max_length=150)
    identificacion = models.CharField(_("Identificacion"), max_length=13)
    direccion_matriz = models.CharField(_("Direccion Matriz"), max_length=150)
    direccion_sucursal = models.CharField(_("Direccion Sucursal"), max_length=150)
    obligado_contabilidd = models.BooleanField(_("Obligado a llevar Contabilidad"))
    telefono = models.CharField(_("Telefono"), max_length=13)
    email = models.EmailField(_("Correo Electronico"), max_length=254)

    def __str__(self):
        return self.razon_social