"""Model, Contiene a Detalles """

# django
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Detalles(models.Model):
    """Model Detalles """

    tipo = models.CharField(_("Tipo"), max_length=50)
    referencia = models.IntegerField(_("Referencia"))
    titulo = models.CharField(_("Titulo"), max_length=50)
    detalles = models.TextField(_("Detalles"))
    is_active = models.BooleanField(_("esta Activo"), default=True)

    def __str__(self):
        return self.descripcion
