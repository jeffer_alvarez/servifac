#django
from django.urls import include, path

#rest framework
from rest_framework import routers
#views
from servifac.empresa import views as empresa_views

router = routers.DefaultRouter()
#Empresa
router.register(r'propietario/(?P<slug_name>[-a-zA-Z0-9]+)/empresa',
                empresa_views.EmpresaViewSet,
                basename='empresa')
router.register(r'propietario/empresa',
                empresa_views.EmpresaViewSetAdmin,
                basename='empresa_admin')

#Establecimiento
router.register(r'propietario/establecimientos',
                empresa_views.EstablecimientoViewSet,
                basename='establecimiento')

#Productos
router.register(r'propietario/(?P<slug_name>[-a-zA-Z]+)/productos',
                empresa_views.ProductoViewSet,
                basename='productos')

router.register(r'propietario/taxonomias_productos',
                empresa_views.ViewTaxonomiaProductos,
                basename='taxonomias_productos')

router.register(r'propietario/lista-taxonomias',
                empresa_views.NombreTaxonomias,
                basename='lista-taxonomias')

router.register(r'propietario/gallery',
                empresa_views.GalleryViset,
                basename='lista-gallery')

router.register(r'propietario/notifications',
                empresa_views.NotificationsViset,
                basename='lista-notifications')

router.register(r'propietario/configuracion-empresa',
                empresa_views.ConfiguracionEmpresaView,
                basename='configuracion-empresa')
#router.register(r'propietario/productos', empresa_views.ProductoViewSetProducto, basename='productos-admin')
router.register(r'propietario/(?P<slug_name>[-a-zA-Z]+)/filter-productos',
                empresa_views.ProductoViewSetFilter,
                basename='filter-productos')

router.register(r'propietario/ordenes_compras',
                empresa_views.OrdenesCompraViewSet,
                basename='ordenes_compras')
empresa_patterns = ([
    path('', include(router.urls)),
], 'empresa')
