# Django REST Framework
from rest_framework import mixins, status, viewsets
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from django.conf import settings
import random
import string
from django.http import JsonResponse
from rest_framework import serializers
from servifac.empresa.permissions import IsPropietario, IsPropietarioEstablecimiento, IsPropietarioEstablecimientoTaxonomia
from rest_framework.permissions import IsAuthenticated
from rest_framework.generics import get_object_or_404
from rest_framework.exceptions import NotFound
from servifac.empresa.serializers import TiposTaxonomiaProductoSerializer, TaxonomiaModelSerializerRelacion, ProductoListOrdenCompra, TaxonomiaProductosModelSerializer, RelacionTaxonomiaProductoSerializer
from servifac.empresa.models import ProductoTipoTaxonimia, Producto, Taxonomia
from rest_framework.filters import SearchFilter, OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend


class ViewTaxonomiaProductos(viewsets.GenericViewSet, mixins.ListModelMixin,
                             mixins.CreateModelMixin, mixins.UpdateModelMixin,
                             mixins.DestroyModelMixin,
                             mixins.RetrieveModelMixin):
    filter_backends = (SearchFilter, OrderingFilter, DjangoFilterBackend)
    ordering = ('producto__precio_a', "taxonomia")
    ordering_fields = ('producto__precio_a', "taxonomia")
    search_fields = ('producto__precio_a', "producto__id",
                     "taxonomia__nombre_taxonomia_id", "taxonomia__tipo",
                     "taxonomia_id")
    filter_fields = ('producto__precio_a', "producto__id",
                     "taxonomia__nombre_taxonomia_id", "taxonomia__tipo",
                     "taxonomia_id")
    serializer_class = TiposTaxonomiaProductoSerializer

    def get_queryset(self):
        establecimiento_id = self.request.query_params['id_establecimiento']
        try:
            queryset = ProductoTipoTaxonimia.objects.filter(
                producto__establecimiento=establecimiento_id).distinct()
        except ProductoTipoTaxonimia.DoesNotExist:
            pass

        return queryset

    def get_permissions(self):
        print(self.action)
        if self.action in ['retrieve', 'update', 'partial_update', 'create']:
            permission_classes = [
                IsAuthenticated, IsPropietarioEstablecimientoTaxonomia,
                IsPropietario
            ]
        else:
            permission_classes = [AllowAny]

        return [permission() for permission in permission_classes]

    def create(self, request, *args, **kwargs):
        if type(request.data) == list:
            taxonomias_producto = RelacionTaxonomiaProductoSerializer(
                data=request.data, many=True)
            taxonomias_producto.is_valid(raise_exception=True)
            data_save = taxonomias_producto.save()

            data_result = TiposTaxonomiaProductoSerializer(data_save,
                                                           many=True).data

            data = {
                "estado": True,
                "message": "Se agrego correctamente la taxonomia al producto",
                "result": data_result
            }
        else:
            try:
                existe = ProductoTipoTaxonimia.objects.get(
                    producto=request.data['producto'],
                    taxonomia=request.data['taxonomia'])
                return Response({
                    "estado": False,
                    "message": "Ya existe esta relacion"
                })
            except ProductoTipoTaxonimia.DoesNotExist:
                taxonomias_productos = RelacionTaxonomiaProductoSerializer(
                    data=request.data)
                taxonomias_productos.is_valid(raise_exception=True)
                data_save = taxonomias_productos.save()
                data_result = TiposTaxonomiaProductoSerializer(data_save).data
                data_result = TiposTaxonomiaProductoSerializer(data_save).data

                data = {
                    "estado": True,
                    "message":
                    "Se agrego correctamente la taxonomia al producto",
                    "result": data_result
                }
        return Response(data)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        instancia_id = instance.taxonomia.id
        id_taxonomia = request.data['taxonomia']
        if int(id_taxonomia) == int(instancia_id):
            return Response({
                "estado":
                False,
                "message":
                "La taxonomia ya pertenece a este producto"
            })
        serializers = RelacionTaxonomiaProductoSerializer(instance,
                                                          data=request.data,
                                                          partial=partial)
        serializers.is_valid(raise_exception=True)
        serializers.save()

        data = {"estado": True, "message": "Categoria actualizada"}
        return Response(data)
