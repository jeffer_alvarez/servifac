# rest framework
from rest_framework import viewsets, mixins, status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.generics import get_object_or_404
from django.http import QueryDict
import requests
from django.http import JsonResponse
from urllib.parse import urlparse
from django.core.files.base import ContentFile
import base64
from rest_framework.filters import SearchFilter, OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend
from decimal import Decimal
# permission
from rest_framework.permissions import IsAuthenticated, AllowAny
from servifac.empresa.permissions import IsPropietario, IsProductoEstablecimientoEmpresa
import json
import ast
# models
from servifac.empresa.models import Producto, Establecimiento, Empresa, Taxonomia
from servifac.users.models import Rol_propietario
from servifac.sistema.models import Producto as ProductoSistema

# serializer
from servifac.empresa.serializers import (
    ProductoModelSerializer, ProductoList, ProductoFilterModelSerializer,
    ProductoTipoTaxonimia, TaxonomiaModelSerializerRelacion,
    ProductoModelSerializerMetas, TiposTaxonomiaProductoSerializerProducto,
    TaxonomiaModelSerializer)


class ProductoViewSet(mixins.RetrieveModelMixin, mixins.CreateModelMixin,
                      mixins.UpdateModelMixin, mixins.DestroyModelMixin,
                      viewsets.GenericViewSet, mixins.ListModelMixin):
    filter_backends = (SearchFilter, OrderingFilter, DjangoFilterBackend)
    ordering = (
        'descripcion',
        'precio_a',
    )
    ordering_fields = ('descripcion', "precio_a")
    search_fields = ('descripcion', "precio_a")
    filter_fields = ('precio_a', "precio_a")
    serializer_class = ProductoModelSerializer

    def dispatch(self, request, *args, **kwargs):
        """Verify that the circle exists."""
        slug_name = kwargs['slug_name']
        lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field
        try:
            filter_kwargs = {self.lookup_field: self.kwargs[lookup_url_kwarg]}
            self.producto = filter_kwargs['pk']
        except:
            self.producto = None
        try:
            self.propietario = Rol_propietario.objects.get(slug_name=slug_name)
        except Rol_propietario.DoesNotExist:
            data = {"estado": "Tienda no encontrada", "slug_name": slug_name}
            return JsonResponse(data)
        return super(ProductoViewSet, self).dispatch(request, *args, **kwargs)

    def get_object(self):
        # Perform the lookup filtering.
        lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field
        filter_kwargs = {self.lookup_field: self.kwargs[lookup_url_kwarg]}

        lista_prductos = self.get_queryset()
        print(get_object_or_404(lista_prductos, **filter_kwargs))
        return get_object_or_404(lista_prductos, **filter_kwargs)

    # Permisos
    def get_permissions(self):

        if self.action in ['update', 'partial_update', 'create']:
            permission_classes = [IsAuthenticated]
        else:
            permission_classes = [AllowAny]

        return [permission() for permission in permission_classes]

    # Filtrar todos los productos del propietario
    def get_queryset(self):
        establecimiento_id = self.request.query_params.get(
            "establecimiento", None)
        empresa = Empresa.objects.get(propietario=self.propietario)
        queryset = Producto.objects.filter(establecimiento__empresa=empresa.id,
                                           is_active=True)
        return queryset

    # Lista de los productos pertenecientes a empresas de propietario
    """ def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        data_establecimiento = ProductoList(queryset, many=True).data
        data = {
            "estado": True,
            "message": "Lista de todos los Productos",
            "objeto": ProductoList(queryset, many=True).data
        }
        return Response(data, status=status.HTTP_200_OK) """

    # Crear nuevo producto
    """http://127.0.0.1:8000/sistema/productos/"""

    def create(self, request, *args, **kwargs):
        imagen = request.data.get("img_original", None)
        print(request.data)
        ''' if categorias:
            categoriasDic = dict(request.data['categorias'])
            categorias = []
            categoria = {}
            for key, value in request.data.dict().items():
                categoria[key] = int(value)
                categorias.append(categoria)
            request.data.pop('categorias')
            request.data['categorias'] = categorias '''

        if type(imagen) == str:
            referencia_producto = request.data.get("referencia_producto", None)
            if referencia_producto == None:
                return JsonResponse({
                    "estado":
                    True,
                    "message":
                    "No existe la referencia del producto"
                })
            else:
                data_producto = request.data.copy()
                producto_sistema = ProductoSistema.objects.filter(
                    id=referencia_producto).values("original", "card",
                                                   "thumbnail")[:1]
                data_producto[
                    'usuario_empresa'] = request.user.propietario.empresa
                serializer = self.get_serializer(data=data_producto)
                serializer.is_valid(raise_exception=True)
                serializer.save()
                data = {
                    "estado": True,
                    "message": "Producto creado exitosamente",
                }

        elif imagen:
            myDict = dict(request.data)
            producto = {}
            for key, value in request.data.dict().items():
                if key == 'precio_a':
                    producto[key] = float(value)
                if key == 'establecimiento':
                    print(value)
                    producto[key] = int(value)
                if key == 'descripcion':
                    producto[key] = value
                if key == 'img_original':
                    producto['imagen'] = value
                if key == 'card':
                    producto['card'] = value
                if key == 'thumbnail':
                    producto['thumbnail'] = value
            producto['categorias'] = request.data['categorias']
            producto['usuario_empresa'] = request.user.propietario.empresa
            serializer = self.get_serializer(data=producto)
            serializer.is_valid(raise_exception=True)
            producto_save = serializer.save()
            data = {
                "estado": True,
                "message": "Producto creado exitosamente",
            }
        else:
            request.data['usuario_empresa'] = request.user.propietario.empresa
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            producto_save = serializer.save()
            data = {
                "estado": True,
                "message": "Producto creado exitosamente",
            }

        return Response(data, status=status.HTTP_201_CREATED)

    # Actualizar producto
    def update(self, request, *args, **kwargs):
        print("llego actualizar", self.producto)
        partial = kwargs.pop('partial', False)
        producto = Producto.objects.get(id=self.producto)
        request.data['usuario_empresa'] = request.user.propietario.empresa
        serializer = self.get_serializer(producto,
                                         data=request.data,
                                         partial=partial)
        serializer.is_valid(raise_exception=True)
        producto = serializer.save()

        data = {
            "estado": True,
            "message": "Producto actualizado con exito",
            "objeto": ProductoModelSerializer(producto).data
        }

        return Response(data, status=status.HTTP_200_OK)

    # Retrive del producto
    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        producto = Producto.objects.get(id=instance.id)
        return Response(ProductoModelSerializerMetas(producto).data,
                        status=status.HTTP_200_OK)

    # Eliminar producto
    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        data = {
            'estado': "Autorizado",
            'message': "Producto eliminado con exito"
        }
        return Response(data, status=status.HTTP_204_NO_CONTENT)

    def perform_destroy(self, instance):
        """Disable producto."""
        instance.is_active = False
        instance.save()


class ProductoViewSetFilter(viewsets.GenericViewSet, mixins.ListModelMixin):
    filter_backends = (SearchFilter, OrderingFilter, DjangoFilterBackend)
    ordering = (
        'descripcion',
        'precio_a',
    )
    ordering_fields = ('descripcion', "precio_a", "created")
    search_fields = (
        'descripcion',
        "precio_a",
        "tipo_producto__taxonomia__tipo",
        "tipo_producto__taxonomia__nombre_taxonomia__nombre",
        "tipo_producto__taxonomia__nombre_taxonomia__slug_name",
    )
    filter_fields = ('precio_a', "precio_a")
    serializer_class = ProductoFilterModelSerializer
    permissions = (AllowAny)

    def dispatch(self, request, *args, **kwargs):
        """Verify that the circle exists."""
        slug_name = kwargs['slug_name']
        self.propietario = get_object_or_404(Rol_propietario,
                                             slug_name=slug_name)
        return super(ProductoViewSetFilter,
                     self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        estado = self.request.query_params.get("is_active", True)
        establecimiento = Establecimiento.objects.get(
            empresa__propietario=self.propietario)
        queryset = Producto.objects.filter(establecimiento_id=establecimiento,
                                           is_active=estado)

        return queryset


def retornarImagenes(url):
    print(url)
    img_url = "https://mitiendavirtual.s3.amazonaws.com/static/" + url
    name = urlparse(img_url).path.split('/')[-1]
    response = requests.get(img_url)
    print("lego a descargar")
    print(response.status_code)
    if response.status_code == 200:
        print("lego a descargar y descargo")
        print(type(response.content))
        content = ContentFile(base64.b64decode(response.content))
        return content
    return False