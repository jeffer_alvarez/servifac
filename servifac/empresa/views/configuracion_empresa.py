# Django REST Framework
from rest_framework import mixins, status, viewsets
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from django.conf import settings
import random
import string
from django.http import JsonResponse
from rest_framework import serializers
from rest_framework.permissions import IsAuthenticated
from rest_framework.generics import get_object_or_404
from rest_framework.exceptions import NotFound

from servifac.empresa.models import ConfiguracionEmpresa
from servifac.empresa.serializers import ConfiguracionEmpresaSerializer


class ConfiguracionEmpresaView(viewsets.GenericViewSet,
                               mixins.CreateModelMixin,
                               mixins.UpdateModelMixin,
                               mixins.RetrieveModelMixin):
    serializer_class = ConfiguracionEmpresaSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        id_empresa = self.request.query_params.get("id_empresa", None)
        try:
            queryset = ConfiguracionEmpresa.objects.all()
        except ConfiguracionEmpresa.DoesNotExist:
            queryset = []

        return queryset

    def create(self, request, *args, **kwargs):
        request.data['empresa'] = request.user.propietario.empresa.id
        try:
            ConfiguracionEmpresa.objects.get(
                empresa=request.user.propietario.empresa.id)
            return Response({
                "estado":
                False,
                "message":
                "Ya existe una configuracion para esta empresa"
            })
        except ConfiguracionEmpresa.DoesNotExist:
            serializers = self.get_serializer(data=request.data)
            serializers.is_valid(raise_exception=True)
            serializers.save()
            data_configuracion = self.get_serializer(serializers).data
        return Response(data_configuracion)
