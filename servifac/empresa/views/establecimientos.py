#rest framework
from rest_framework import viewsets, status, mixins
from rest_framework.decorators import action
from rest_framework.response import Response

#permission
from rest_framework.permissions import IsAuthenticated
from servifac.empresa.permissions import IsPropietario, IsPropietarioEstablecimiento

#models
from servifac.empresa.models import Establecimiento, Empresa
from servifac.users.models import Rol_propietario

#serializers
from servifac.empresa.serializers import EstablecimientoModelSerializer, EmpresaModelSerializer

class EstablecimientoViewSet(mixins.CreateModelMixin,
                            mixins.RetrieveModelMixin,
                            mixins.ListModelMixin,
                            mixins.UpdateModelMixin,
                            mixins.DestroyModelMixin,
                            viewsets.GenericViewSet):


    serializer_class = EstablecimientoModelSerializer

    def get_queryset(self):
        queryset = Establecimiento.objects.filter(empresa=self.request.user.propietario.empresa, is_active=True)              
        return queryset

    # Permission
    def get_permissions(self):
        permission_classes = [IsPropietario]
        if self.action in ["list","retrieve", 'update', 'partial_update', 'finish']:
            permission_classes.append(IsPropietarioEstablecimiento)

        return [permission() for permission in permission_classes]

    
    # Lista de los establecimientos pertenecientes a los propietarios
    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        data_establecimiento=self.get_serializer(queryset, many=True).data
        data = {
            "estado": True,
            "message": "Lista de todos los establecimientos",
            "objeto": self.get_serializer(queryset, many=True).data
        }
        return Response(data, status=status.HTTP_200_OK)


    # Crear nuevo establecimiento
    """http://127.0.0.1:8000/propietario/establecimientos/"""
    def create(self, request, *args, **kwargs):
        request.data['empresa'] = request.user.propietario.empresa
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        establecimiento = serializer.save()
        # add extra data
        data = {
            "estado": "Creado",
            "message": "Establecimiento creado con exito",
            "objeto": EstablecimientoModelSerializer(establecimiento).data
        }
        return Response(data, status=status.HTTP_201_CREATED)
    
    # Eliminar establecimiento
    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        data = {
            'estado': "Eliminado",
            'message': "Establecimiento eliminado con exito"
        }
        return Response(data, status=status.HTTP_200_OK)

    def perform_destroy(self, instance):
        """Disable establecimiento."""
        instance.is_active = False
        instance.save()

    # Actualizar establecimiento
    def update(self, request, *args, **kwargs):
        partial=request.method=='PATCH'
        instance = self.get_object()
        print(partial)
        print(instance)
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        establecimiento=serializer.save()

        data = {
            "estado":True,
            "message":"Establecimiento actualizado con exito",
            "objeto": EstablecimientoModelSerializer(establecimiento).data
        }

        return Response(data, status=status.HTTP_200_OK)
