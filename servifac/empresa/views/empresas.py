# Django REST Framework
from rest_framework import mixins, status, viewsets
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from django.conf import settings
import random
import string
from django.http import JsonResponse
from rest_framework import serializers
from rest_framework.permissions import IsAuthenticated
from rest_framework.generics import get_object_or_404
from rest_framework.exceptions import NotFound
# models
from servifac.empresa.models import Empresa, Establecimiento, Taxonomia, ConfiguracionEmpresa, NombreTaxonomia, ProductoTipoTaxonimia
# serializers
from servifac.empresa.serializers import EmpresaModelSerializer
from servifac.users.models import Rol_propietario, User, Rol_cliente
from rest_framework.authtoken.models import Token

from servifac.empresa.permissions import IsAccountOwner, IsEmpresaEncontrada, IsSelfMember
from servifac.users.permissions import IsPropietario
from servifac.empresa.serializers import ViewOrdenCompra, ProductoOrdenModelSerializer, ListaOrdenesCompras, TaxonomiaModelSerializer
from servifac.empresa.models import Establecimiento, Empresa, Orden_compras, Producto_orden_compra, Producto, Taxonomia

from servifac.users.serializers import (
    PropietarioLogInSerializer,
    PropietarioModelSerializer,
    PropietarioSignUpSerializer,
    UserModelSerializer,
)
from servifac.empresa.serializers import (EmpresaModelSerializer,
                                          EstablecimientoModelSerializer,
                                          TaxonomiaProductosModelSerializer)


class EmpresaViewSet(mixins.ListModelMixin, viewsets.GenericViewSet,
                     mixins.RetrieveModelMixin, mixins.UpdateModelMixin):

    serializer_class = EmpresaModelSerializer

    def dispatch(self, request, *args, **kwargs):
        slug_name = kwargs['slug_name']
        lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field
        try:
            filter_kwargs = {self.lookup_field: self.kwargs[lookup_url_kwarg]}
            self.empresa = filter_kwargs['pk']
        except:
            self.empresa = None
        try:
            self.propietario = Rol_propietario.objects.get(slug_name=slug_name)
        except Rol_propietario.DoesNotExist:
            data = {"estado": "Tienda no encontrada", "slug_name": slug_name}
            return JsonResponse(data)
        return super(EmpresaViewSet, self).dispatch(request, *args, **kwargs)

    def get_object(self):
        lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field
        filter_kwargs = {self.lookup_field: self.kwargs[lookup_url_kwarg]}
        self.empresa = get_object_or_404(Empresa, **filter_kwargs)
        return get_object_or_404(Empresa, **filter_kwargs)

    def get_permissions(self):
        print(self.action)
        if self.action in [
                'retrieve', 'update', 'partial_update', 'ordenes_compras',
                "categorias"
        ]:
            permission_classes = [IsAuthenticated, IsEmpresaEncontrada]
        else:
            permission_classes = [AllowAny]

        return [permission() for permission in permission_classes]

    def get_queryset(self):
        queryset = Empresa.objects.get(propietario=self.propietario.id)
        return queryset

    def retrieve(self, request, *args, **kwargs):

        empresa = Empresa.objects.get(propietario=self.propietario.id)
        data_empresa = EmpresaModelSerializer(empresa).data

        return Response(data_empresa)

    def list(self, request, *args, **kwargs):

        queryset = self.filter_queryset(self.get_queryset())
        data_empresa = self.get_serializer(queryset).data
        token_user_auth = self.request.query_params.get("token", None)
        try:
            token_consulta = Token.objects.get(user=self.propietario.user)
        except Token.DoesNotExist:
            token_consulta = ''
        token = "{}".format(token_consulta)
        if token_user_auth == token:
            user = Token.objects.get(key=token).user
            data_empresa['sesion_iniciada'] = True
            data_empresa['is_propietario'] = user.is_propietario
            usuario_sesion_iniciada = {
                "identificacion": user.identificacion,
                "tipo_usuario": "propietario",
                "first_name": user.first_name,
                "phone_number": user.phone_number,
            }
            data_empresa['is_cliente'] = user.is_client
            data_empresa['is_contador'] = user.is_contador
            data_empresa['is_empleado'] = user.is_empleado
        else:
            try:
                user = Token.objects.get(key=token_user_auth).user
                try:
                    cliente = Rol_cliente.objects.get(user=user)
                    data_empresa['cliente_id'] = cliente.id
                    usuario_sesion_iniciada = {
                        "tipo_usuario": "cliente",
                        "identificacion": cliente.user.identificacion,
                        "first_name": cliente.user.first_name,
                        "phone_number": cliente.user.phone_number,
                    }
                    data_empresa['sesion_iniciada'] = True
                    data_empresa['is_propietario'] = False
                    data_empresa['is_cliente'] = True
                    data_empresa['is_contador'] = False
                    data_empresa['is_empleado'] = False
                except Rol_cliente.DoesNotExist:
                    data_empresa['sesion_iniciada'] = False
                    usuario_sesion_iniciada = {
                        "tipo_usuario": None,
                        "identificacion": None,
                        "first_name": None,
                        "phone_number": None,
                    }
                    data_empresa['is_propietario'] = False
                    data_empresa['is_cliente'] = False
                    data_empresa['is_contador'] = False
                    data_empresa['is_empleado'] = False
            except Token.DoesNotExist:
                data_empresa['sesion_iniciada'] = False
                data_empresa['is_propietario'] = False
                usuario_sesion_iniciada = {
                    "tipo_usuario": None,
                    "identificacion": None,
                    "first_name": None,
                    "phone_number": None,
                }
                data_empresa['is_cliente'] = False
                data_empresa['is_contador'] = False
                data_empresa['is_empleado'] = False

        #data_propietario = self.get_serializer(queryset).data
        data_empresa['empresa_id'] = data_empresa['id']
        data_empresa.pop('control_secuencial')
        data_empresa.pop('contador')

        data_empresa.pop('clave_firma_electrinica')
        data_empresa.pop('firma_electronica')
        data_empresa.pop('ambiente')
        data_empresa.pop('contribuyente_especial')
        data_empresa.pop('direccion')
        data_empresa.pop('obligado_contabilidad')
        data_empresa.pop('propietario')

        establecimiento = Establecimiento.objects.get(
            empresa=data_empresa['empresa_id'])
        data_establecimiento = EstablecimientoModelSerializer(
            establecimiento).data
        data_empresa['establecimiento_id'] = data_establecimiento['id']
        data_empresa['canton'] = data_establecimiento['canton']
        data_empresa['ciudad'] = data_establecimiento['ciudad']
        data_empresa['direccion'] = data_establecimiento['direccion']
        data_empresa['latitud'] = data_establecimiento['latitud']
        data_empresa['longitud'] = data_establecimiento['longitud']
        data_empresa['parroquia'] = data_establecimiento['parroquia']

        #Datos propietario
        propietario = Rol_propietario.objects.get(
            id=data_empresa['empresa_id'])
        data_propietario = PropietarioModelSerializer(propietario).data
        data_empresa['propietario_id'] = data_propietario['id']
        data_empresa['nombre_comercial'] = data_propietario['nombre_comercial']
        data_empresa['slug_name'] = data_propietario['slug_name']
        data_empresa['descripcion'] = data_propietario['descripcion']
        data_empresa['portada'] = data_propietario['portada']
        data_empresa['portada_large'] = data_propietario['portada_large']
        data_empresa['portada_small'] = data_propietario['portada_small']

        data_empresa['first_name'] = data_propietario['user']['first_name']
        data_empresa['foto'] = data_propietario['user']['foto']
        data_empresa['identificacion'] = data_propietario['user'][
            'identificacion']
        data_empresa['user_id'] = data_propietario['user']['id']
        data_empresa['phone_number'] = data_propietario['user']['phone_number']
        data_empresa['usuario_sesion_iniciada'] = usuario_sesion_iniciada
        try:
            data_configuracion = ConfiguracionEmpresa.objects.get(
                empresa=data_empresa['id'])
            configuracion_empresa = {
                "id_configuracion_empresa": data_configuracion.id,
                "primary": data_configuracion.primary,
                "secondary": data_configuracion.secondary,
                "dark": data_configuracion.dark,
                "accent": data_configuracion.accent,
                "link": data_configuracion.link,
                "servicio_domicilio": data_configuracion.servicio_domicilio,
                "code_pais": data_configuracion.code_pais,
                "pais": data_configuracion.pais,
                "horario_atencion": data_configuracion.horario_atencion,
            }
        except ConfiguracionEmpresa.DoesNotExist:
            configuracion_empresa = {
                "primary": None,
                "secondary": None,
                "dark": None,
                "accent": None,
                "link": None,
                "servicio_domicilio": False,
                "code_pais": None,
                "pais": None,
                "horario_atencion": None,
            }
        data_empresa.pop('id')
        data_empresa['configuracion_empresa'] = configuracion_empresa
        data = {
            "tienda": data_empresa,
        }
        lista_prductos = Producto.objects.filter(
            establecimiento=data_establecimiento['id'])

        return Response(data, status=status.HTTP_200_OK)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_queryset()
        serializer = self.get_serializer(instance,
                                         data=request.data,
                                         partial=partial)
        serializer.is_valid(raise_exception=True)
        empresa = serializer.save()
        empresa_data = EmpresaModelSerializer(empresa).data

        return Response(empresa_data)

    @action(detail=True, methods=['get'])
    def ordenes_compras(self, request, *args, **kwargs):
        instance = self.empresa
        limit = self.request.query_params.get("limit", None)
        pedido_despachado = self.request.query_params.get(
            "pedido_despachado", None)
        pedido_realizado = self.request.query_params.get(
            "pedido_realizado", None)
        pedido_incompleto = self.request.query_params.get(
            "pedido_incompleto", None)
        pedido_reconfirmado = self.request.query_params.get(
            "pedido_reconfirmado", None)
        pedido_entregado = self.request.query_params.get(
            "pedido_entregado", None)
        pedido_cancelado = self.request.query_params.get(
            "pedido_cancelado", None)
        establecimiento = Establecimiento.objects.get(empresa=instance)
        ordenes_compras = Orden_compras.objects.filter(
            establecimiento=establecimiento.id)
        if limit:
            if bool(pedido_despachado) == True or False:
                ordenes_compras = Orden_compras.objects.filter(
                    establecimiento=establecimiento.id,
                    pedido_despachado=pedido_despachado)[:int(limit)]
            elif bool(pedido_realizado) == True or False:
                ordenes_compras = Orden_compras.objects.filter(
                    establecimiento=establecimiento.id,
                    pedido_realizado=pedido_realizado)[:int(limit)]
            elif bool(pedido_incompleto) == True or False:
                ordenes_compras = Orden_compras.objects.filter(
                    establecimiento=establecimiento.id,
                    pedido_incompleto=pedido_incompleto)[:int(limit)]
            elif bool(pedido_reconfirmado) == True or False:
                ordenes_compras = Orden_compras.objects.filter(
                    establecimiento=establecimiento.id,
                    pedido_reconfirmado=pedido_reconfirmado)[:int(limit)]
            elif bool(pedido_entregado) == True or False:
                ordenes_compras = Orden_compras.objects.filter(
                    establecimiento=establecimiento.id,
                    pedido_entregado=pedido_entregado)[:int(limit)]
            elif bool(pedido_cancelado) == True or False:
                ordenes_compras = Orden_compras.objects.filter(
                    establecimiento=establecimiento.id,
                    pedido_cancelado=pedido_cancelado)[:int(limit)]
            else:
                ordenes_compras = Orden_compras.objects.filter(
                    establecimiento=establecimiento.id)[:int(limit)]
        else:
            if bool(pedido_despachado) == True or False:
                ordenes_compras = Orden_compras.objects.filter(
                    establecimiento=establecimiento.id,
                    pedido_despachado=pedido_despachado)
            elif bool(pedido_realizado) == True or False:
                ordenes_compras = Orden_compras.objects.filter(
                    establecimiento=establecimiento.id,
                    pedido_realizado=pedido_realizado)
            elif bool(pedido_incompleto) == True or False:
                ordenes_compras = Orden_compras.objects.filter(
                    establecimiento=establecimiento.id,
                    pedido_incompleto=pedido_incompleto)
            elif bool(pedido_reconfirmado) == True or False:
                ordenes_compras = Orden_compras.objects.filter(
                    establecimiento=establecimiento.id,
                    pedido_reconfirmado=pedido_reconfirmado)
            elif bool(pedido_entregado) == True or False:
                ordenes_compras = Orden_compras.objects.filter(
                    establecimiento=establecimiento.id,
                    pedido_entregado=pedido_entregado)
            elif bool(pedido_cancelado) == True or False:
                ordenes_compras = Orden_compras.objects.filter(
                    establecimiento=establecimiento.id,
                    pedido_cancelado=pedido_cancelado)
            else:
                ordenes_compras = Orden_compras.objects.filter(
                    establecimiento=establecimiento.id)
        lista_ordenes = ViewOrdenCompra(ordenes_compras, many=True).data
        for lista in lista_ordenes:
            try:
                productos_orden = Producto_orden_compra.objects.filter(
                    orden_compras=lista['id'])
                productos_data = ListaOrdenesCompras(productos_orden,
                                                     many=True).data
                lista['productos'] = productos_data
            except Producto_orden_compra.DoesNotExist:
                lista['productos'] = []
        return Response(lista_ordenes)

    @action(detail=True, methods=['get'])
    def categorias(self, request, *args, **kwargs):
        categorias = Taxonomia.objects.filter(
            empresa=request.user.propietario.empresa.id).values("nombre")
        categorias_unicas = []
        for categoria in categorias:
            if categoria not in categorias_unicas:
                categorias_unicas.append(categoria)
        data_categorias = TaxonomiaModelSerializer(categorias_unicas,
                                                   many=True).data
        return Response(data_categorias)

    @action(detail=True, methods=['get'])
    def productos_empresa(self, request, *args, **kwargs):
        productos_categorias = []
        categoria = self.request.query_params.get("categoria", None)
        if categoria:
            lista_prductos_categorias = Taxonomia.objects.filter(
                empresa=request.user.propietario.empresa.id,
                nombre=categoria)[:20]
        else:
            lista_prductos_categorias = Taxonomia.objects.filter(
                empresa=request.user.propietario.empresa.id)[:20]

        productos_data = TaxonomiaProductosModelSerializer(
            lista_prductos_categorias, many=True).data

        return Response(productos_data)


#Actualizar Empresa
class EmpresaViewSetAdmin(mixins.DestroyModelMixin, mixins.RetrieveModelMixin,
                          viewsets.GenericViewSet, mixins.CreateModelMixin,
                          mixins.UpdateModelMixin):
    lookup_field = 'pk'
    serializer_class = EmpresaModelSerializer
    permission_classes = [IsAuthenticated, IsEmpresaEncontrada]
    queryset = Empresa.objects.all()

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    # Update

    # Delete
    def perform_destroy(self, instance):
        """Disable propietario."""
        instance.is_active = False
        instance.save()

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        data = {
            'estado': "Autorizado",
            'message': "Empresa eliminado con exito"
        }
        return Response(data, status=status.HTTP_200_OK)
