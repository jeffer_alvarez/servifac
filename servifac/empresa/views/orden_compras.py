#rest framework
from rest_framework import viewsets, mixins, status
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny, IsAuthenticated
import boto3
from django.conf import settings
from servifac.users.permissions import IsCliente, IsClienteUser

#models
from servifac.empresa.models import Orden_compras, Producto_orden_compra, Producto, Establecimiento
from servifac.users.models import Rol_cliente

#serializers
from servifac.empresa.serializers import OrdenComprasModelSerializer, CreateOrdenComprasModelSerializer, ProductoModelSerializer, ViewOrdenCompra, ListaOrdenesCompras


class OrdenesCompraViewSet(mixins.CreateModelMixin, mixins.RetrieveModelMixin,
                           viewsets.GenericViewSet, mixins.UpdateModelMixin):

    queryset = Orden_compras.objects.all()

    def get_permissions(self):
        if self.action in [
                'retrieve',
                'update',
                'partial_update',
        ]:
            permission_classes = [IsAuthenticated]
        if self.action in [
                'create',
        ]:
            permission_classes = [IsAuthenticated, IsCliente]

        return [permission() for permission in permission_classes]

    def get_serializer_class(self):
        if self.action == 'create':
            self.serializer_class = CreateOrdenComprasModelSerializer

        elif self.action in [
                'retrieve',
                'update',
                'partial_update',
        ]:
            self.serializer_class = ViewOrdenCompra

        return self.serializer_class

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        orden_compra = serializer.save()
        data_orden_compra = ViewOrdenCompra(orden_compra).data
        try:
            productos_orden = Producto_orden_compra.objects.filter(
                orden_compras=data_orden_compra['id'])
            productos_data = ListaOrdenesCompras(productos_orden,
                                                 many=True).data
            data_orden_compra['productos'] = productos_data
        except Producto_orden_compra.DoesNotExist:
            pass

        datos_propietario = Establecimiento.objects.filter(
            id=request.data['establecimiento']).values(
                "empresa__propietario__user__phone_number",
                "empresa__propietario__slug_name",
                "empresa__propietario__nombre_comercial")
        datos_envio = [{
            'phone_number':
            esta['empresa__propietario__user__phone_number'],
            'slug_name':
            esta['empresa__propietario__slug_name'],
            "nombre_comercial":
            esta['empresa__propietario__nombre_comercial']
        } for esta in datos_propietario]
        hash_url = data_orden_compra['hash_url']
        nombre_comercial = datos_envio[0]['nombre_comercial']
        sms_status = enviar_sms(datos_envio[0]['phone_number'],
                                datos_envio[0]['slug_name'], hash_url,
                                nombre_comercial)
        data = {
            "estado": True,
            "status_sms": sms_status,
            "mensaje": "Orden de compra registrada exitosamente",
            "objeto": data_orden_compra
        }

        return Response(data, status=status.HTTP_201_CREATED)

    def retrieve(self, request, *args, **kwargs):
        print("hola")
        instance = self.get_object()
        print(instance)
        serializer = ViewOrdenCompra(instance)
        return Response(serializer.data)


''' client = boto3.client('sns',
                          "us-east-1",
                          aws_access_key_id="AKIAIU5CK7TDFYPWOFBQ",
                          aws_secret_access_key="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")
            
        client.publish(PhoneNumber=request.data['phone_number'],Message='') 
        data = {
        "estado":False,
        "phone_number":request.data['phone_number'],
        "message":"Número de teléfono valido"
        } '''


def enviar_sms(phone_number, slug_name, hash_url, nombre_comercial):
    client = boto3.client(
        'sns',
        "us-east-1",
        aws_access_key_id="AKIAIU5CK7TDFYPWOFBQ",
        aws_secret_access_key="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")
    try:
        client.publish(
            PhoneNumber=phone_number,
            Message="Nuevo pedido en {} https://mitiendavirtual.ec/{}?io={}".
            format(nombre_comercial, slug_name, hash_url))
    except:
        return False
    return True
