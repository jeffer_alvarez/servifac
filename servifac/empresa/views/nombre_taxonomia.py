# rest framework
from rest_framework import viewsets, mixins, status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.generics import get_object_or_404
from django.http import QueryDict
import requests
from django.http import JsonResponse
from urllib.parse import urlparse
from django.core.files.base import ContentFile
import base64
from rest_framework.filters import SearchFilter, OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend
from decimal import Decimal
# permission
from rest_framework.permissions import IsAuthenticated, AllowAny
from servifac.empresa.permissions import IsPropietario, IsProductoEstablecimientoEmpresa, IsPropietarioEstablecimientoTaxonomiaNombres

# models
from servifac.empresa.models import Producto, Establecimiento, Empresa, Taxonomia, NombreTaxonomia
from servifac.users.models import Rol_propietario
from servifac.sistema.models import Producto as ProductoSistema

# serializer
from servifac.empresa.serializers import (
    ProductoModelSerializer, ProductoList, ProductoFilterModelSerializer,
    TaxonomiaModelSerializer, NombreProductosModelSerializer,
    TaxonomiaProductosModelSerializer, TaxonomiasNombreSerializer,
    TaxonomiasNombreSerializerSave)


class NombreTaxonomias(viewsets.GenericViewSet, mixins.ListModelMixin,
                       mixins.CreateModelMixin, mixins.UpdateModelMixin,
                       mixins.RetrieveModelMixin):
    serializer_class = TaxonomiaProductosModelSerializer

    def get_permissions(self):
        print(self.action)
        if self.action in ['retrieve', 'update', 'partial_update', 'create']:
            permission_classes = [
                IsAuthenticated, IsPropietarioEstablecimientoTaxonomiaNombres,
                IsPropietario
            ]
        elif self.action in ['list']:
            permission_classes = [AllowAny]

        return [permission() for permission in permission_classes]

    def get_queryset(self):
        try:
            empresa = Empresa.objects.get(
                id=self.request.query_params['id_empresa'])
        except Empresa.DoesNotExist:
            empresa = None
        queryset = Taxonomia.objects.filter(empresa=empresa)
        return queryset

    def create(self, request, *args, **kwargs):
        try:
            existe_empresa = Empresa.objects.get(id=request.data['empresa'])
            try:
                existe_taxonomia = Taxonomia.objects.get(
                    empresa=request.data['empresa'],
                    nombre_taxonomia__nombre=request.data['nombre'],
                    tipo=request.data['tipo'])
                data_result = {
                    "estado": False,
                    "message": "Ya existe esta taxonomia"
                }
                return Response(data_result)
            except Taxonomia.DoesNotExist:
                serializer = TaxonomiasNombreSerializerSave(data=request.data)
                serializer.is_valid(raise_exception=True)
                data_save = serializer.save()
                data_serializer = TaxonomiasNombreSerializerSave(
                    data_save).data
                data_result = {
                    "estado": True,
                    "message": "Se creo taxonomia exitosamente",
                    "result": data_serializer
                }
        except Empresa.DoesNotExist:
            data_result = {"estado": False, "message": "no existe la empresa"}

        return Response(data_result)

    def update(self, request, *args, **kwargs):

        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = TaxonomiasNombreSerializer(instance,
                                                data=request.data,
                                                partial=partial)
        serializer.is_valid(raise_exception=True)
        daat_save = serializer.save()
        data_result = TaxonomiasNombreSerializer(daat_save).data
        return Response(data_result)