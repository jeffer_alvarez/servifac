from rest_framework import viewsets, mixins, status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.generics import get_object_or_404
from django.http import QueryDict
import requests
from django.http import JsonResponse
from urllib.parse import urlparse
from django.core.files.base import ContentFile
import base64
from rest_framework.filters import SearchFilter, OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend
from decimal import Decimal
# permission
from rest_framework.permissions import IsAuthenticated, AllowAny
from servifac.empresa.permissions import IsPropietario

# models
from servifac.empresa.models import Notifications
# serializer
from servifac.empresa.serializers import (NotificationsSerializer)


class NotificationsViset(viewsets.GenericViewSet, mixins.ListModelMixin,
                         mixins.CreateModelMixin, mixins.UpdateModelMixin,
                         mixins.RetrieveModelMixin):
    filter_backends = (SearchFilter, OrderingFilter, DjangoFilterBackend)
    search_fields = ('token', "uid_device", "id_user")
    filter_fields = ('token', "uid_device", "id_user")
    serializer_class = NotificationsSerializer

    def get_queryset(self):
        queryset = Notifications.objects.all()
        return queryset

    def create(self, request, *args, **kwargs):
        uid_device = request.data['uid_device']
        try:
            existe_uid_device = Notifications.objects.get(
                uid_device=uid_device)
            existe_uid_device.is_propietario = request.data['is_propietario']
            existe_uid_device.is_client = request.data['is_client']
            existe_uid_device.is_anonymus = request.data['is_anonymus']
            existe_uid_device.token = request.data['token']
            existe_uid_device.uid_device = request.data['uid_device']
            existe_uid_device.id_user = request.data['id_user']
            existe_uid_device.save()
            serializer_data = self.get_serializer(existe_uid_device).data
            return Response(serializer_data)
        except Notifications.DoesNotExist:
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            serializer = serializer.save()
            serializer_data = self.get_serializer(serializer).data
            return Response(serializer_data)
