from rest_framework import viewsets, mixins, status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.generics import get_object_or_404
from django.http import QueryDict
import requests
from django.http import JsonResponse
from urllib.parse import urlparse
from django.core.files.base import ContentFile
import base64
from rest_framework.filters import SearchFilter, OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend
from decimal import Decimal
# permission
from rest_framework.permissions import IsAuthenticated, AllowAny
from servifac.empresa.permissions import IsPropietario

# models
from servifac.empresa.models import Gallery
# serializer
from servifac.empresa.serializers import (GallerySerializer)


class GalleryViset(viewsets.GenericViewSet, mixins.ListModelMixin,
              mixins.CreateModelMixin, mixins.UpdateModelMixin,
              mixins.RetrieveModelMixin):
    filter_backends = (SearchFilter, OrderingFilter, DjangoFilterBackend)
    search_fields = ('producto_id', "establecimiento_id")
    filter_fields = ('producto_id', "establecimiento_id")
    serializer_class = GallerySerializer

    def get_queryset(self):
        queryset = Gallery.objects.all()
        return queryset