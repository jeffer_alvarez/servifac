# rest framework
from rest_framework import serializers

# models
from servifac.empresa.models import ControlSecuencial


class ControlSecuencialModelSerializer(serializers.ModelSerializer):
    """Model serializer ControlSecuencial."""

    class Meta:
        model = ControlSecuencial
        fields = [
            'numero_facturas',
            'numero_retenciones',
            'numero_notas_credito',
            'numero_notas_debito',
            'nota_venta',
            'numero_guias_remision',
            'proformas',
            'ordenes_compras',
        ]
