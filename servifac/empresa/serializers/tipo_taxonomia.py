# rest framework
from rest_framework import serializers
from collections import OrderedDict
# models
from servifac.empresa.models import Taxonomia, Producto, ProductoTipoTaxonimia, NombreTaxonomia


class NombreProductosModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = NombreTaxonomia
        fields = [
            "id",
            "nombre",
            "slug_name",
        ]


class TaxonomiaProductosModelSerializer(serializers.ModelSerializer):
    nombre_taxonomia = NombreProductosModelSerializer()

    class Meta:
        model = Taxonomia
        fields = ["id", "tipo", "nombre_taxonomia"]


class ProductosModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Producto
        fields = ["id", "descripcion", "precio_a"]


class TiposTaxonomiaProductoSerializer(serializers.ModelSerializer):
    taxonomia = TaxonomiaProductosModelSerializer()

    class Meta:
        model = ProductoTipoTaxonimia
        fields = ["id", "producto", "taxonomia"]


class TiposTaxonomiaProductoSerializerProducto(serializers.ModelSerializer):
    taxonomia = TaxonomiaProductosModelSerializer()

    class Meta:
        model = ProductoTipoTaxonimia
        fields = ["id", "taxonomia"]


class RelacionTaxonomiaProductoSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductoTipoTaxonimia
        fields = ["id"]

    def create(self, data):
        lista_ralciones = self._kwargs['data']
        if type(self._kwargs['data']) == list:
            for relacion in lista_ralciones:
                try:
                    existe = ProductoTipoTaxonimia.objects.get(
                        producto=relacion['producto'],
                        taxonomia=relacion['taxonomia'])
                    if existe:
                        raise serializers.ValidationError(
                            "ya existe esta relación")
                except ProductoTipoTaxonimia.DoesNotExist:
                    taxonomia = Taxonomia.objects.get(id=relacion['taxonomia'])
                    producto = Producto.objects.get(id=relacion['producto'])
                    data = ProductoTipoTaxonimia.objects.create(
                        producto=producto, taxonomia=taxonomia)
        else:
            taxonomia = Taxonomia.objects.get(
                id=self._kwargs['data']['taxonomia'])
            producto = Producto.objects.get(
                id=self._kwargs['data']['producto'])
            data = ProductoTipoTaxonimia.objects.create(producto=producto,
                                                        taxonomia=taxonomia)
        return data

    def update(self, instance, validated_data):
        id_taxonomia = self._kwargs['data']['taxonomia']
        taxonomia = Taxonomia.objects.get(id=id_taxonomia)
        instance.taxonomia = taxonomia
        instance.save()
        return super(RelacionTaxonomiaProductoSerializer,
                     self).update(instance, validated_data)
