# rest framework
from rest_framework import serializers

# models
from servifac.empresa.models import ConfiguracionEmpresa, Empresa
from servifac.empresa.serializers import EmpresaModelSerializer


class ConfiguracionEmpresaSerializer(serializers.ModelSerializer):
    class Meta:
        model = ConfiguracionEmpresa

        fields = ("id", "primary", "secondary", "dark", "accent", "link",
                  "servicio_domicilio", "code_pais", "pais",
                  "horario_atencion")

    def create(self, data):
        empresa_id = self._kwargs['data']['empresa']
        empresa = Empresa.objects.get(id=empresa_id)

        data = ConfiguracionEmpresa.objects.create(**data, empresa=empresa)

        return data


class ConfiguracionEmpresaSerializerTienda(serializers.ModelSerializer):
    class Meta:
        model = ConfiguracionEmpresa
        fields = ("servicio_domicilio", "horario_atencion")
