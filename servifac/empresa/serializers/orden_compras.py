#rest framework
from rest_framework import serializers

#models
from servifac.empresa.models import Orden_compras, Producto, Producto_orden_compra, Establecimiento
from servifac.users.models import Rol_cliente, User
from servifac.users.serializers.users import UserModelSerializerOrden

import copy
import random
import string
#serializers
from servifac.empresa.serializers import ProductoOrdenModelSerializer, CreateProductoOrdenModelSerializer, EstablecimientoModelSerializer


class ConsultaClienteModelSerializer(serializers.ModelSerializer):
    user = UserModelSerializerOrden(required=True, many=False)

    class Meta:
        model = Rol_cliente
        fields = [
            "id",
            "user",
        ]


class OrdenComprasModelSerializer(serializers.ModelSerializer):

    productos = ProductoOrdenModelSerializer(many=True, required=True)

    class Meta:
        model = Orden_compras
        fields = [
            'id', 'numero_orden', 'precio_total', 'subtotal', "productos"
        ]


#Crear orden de compra
class CreateOrdenComprasModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Orden_compras
        fields = ['id', 'precio_total', 'subtotal']

    def validate(self, data):
        productos = self._kwargs['data']['productos']
        cliente = self._kwargs['data']['cliente']
        try:
            cliente_instance = Rol_cliente.objects.get(id=cliente)
        except Rol_cliente.DoesNotExist:
            raise serializers.ValidationError('El cliente no existe')

        establecimiento_cliente = self._kwargs['data']['establecimiento']

        establecimiento_orden = Establecimiento.objects.get(
            id=establecimiento_cliente)
        #validar subtotal de los productos
        subtotal_productos = []
        total_productos = []
        for producto in productos:
            obj_producto = producto['producto']
            #validar que el producto pertenezca al establecimiento del cliente
            try:
                Producto.objects.get(pk=obj_producto,
                                     establecimiento=establecimiento_cliente)
            except Producto.DoesNotExist:
                raise serializers.ValidationError(
                    'El producto {} no pertenece a su establecimiento'.format(
                        obj_producto))

            total = producto['precio_unitario'] * producto['cantidad']

            if total != producto['precio_total']:
                raise serializers.ValidationError(
                    'El total del {} es incorrecto'.format(obj_producto))
            else:
                subtotal_productos.append(total)
                total_productos.append(producto['precio_total'])

        #validar subtotal de orden de compra

        if float(data['precio_total']) != float(sum(subtotal_productos)):
            raise serializers.ValidationError(
                'El subtotal de la orden de compra es incorrecto')

        if float(data['precio_total']) != float(sum(total_productos)):
            raise serializers.ValidationError(
                'El total de la orden de compra es incorrecto')
        data['cliente'] = cliente_instance
        data['productos'] = productos
        data['cliente_numero'] = cliente
        data['establecimiento_orden'] = establecimiento_orden
        return data

    def create(self, data):
        productos = data['productos']
        data.pop('productos')

        establecimiento = data['establecimiento_orden']
        hash_url = ''.join(
            random.choices(string.ascii_lowercase + string.digits, k=20))
        #crear orden de compra
        numero_cliente = data['cliente_numero']
        data.pop('cliente_numero')
        data.pop('establecimiento_orden')
        numero_ordenes = Orden_compras.objects.filter(
            cliente=numero_cliente).count()

        if len(str(numero_ordenes)) == 1:
            numero_orden_suma = "00000000{}".format(numero_ordenes + 1)

        if len(str(numero_ordenes)) == 2:
            numero_orden_suma = "0000000{}".format(numero_ordenes + 1)
        if len(str(numero_ordenes)) == 3:
            numero_orden_suma = "000000{}".format(numero_ordenes + 1)

        orden_compra = Orden_compras.objects.create(
            **data,
            establecimiento=establecimiento,
            numero_orden=numero_orden_suma,
            hash_url=hash_url,
            pedido_realizado=True)

        #crear producto orden compra
        for producto in productos:
            producto_empresa = Producto.objects.get(id=producto['producto'])
            Producto_orden_compra.objects.create(
                orden_compras=orden_compra,
                producto=producto_empresa,
                cantidad=producto['cantidad'],
                precio_total=producto['precio_total'],
                precio_unitario=producto['precio_unitario'])
        return orden_compra


class ViewOrdenCompra(serializers.ModelSerializer):
    cliente = ConsultaClienteModelSerializer(required=True)

    class Meta:
        model = Orden_compras
        fields = [
            'id', "establecimiento", "created", 'cliente', 'precio_total',
            'subtotal', 'numero_orden', "hash_url", "pedido_despachado",
            "pedido_realizado", "pedido_incompleto", "pedido_reconfirmado",
            "pedido_entregado", "pedido_cancelado", "pedido_transportando",
            "pedido_atendido"
        ]
