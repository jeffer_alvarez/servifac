from rest_framework import serializers
from collections import OrderedDict
from django.db import connection, reset_queries

from servifac.empresa.models import NombreTaxonomia, Taxonomia, Empresa

from .tipo_taxonomia import NombreProductosModelSerializer


class TaxonomiasNombreSerializer(serializers.ModelSerializer):
    nombre_taxonomia = NombreProductosModelSerializer(read_only=True)

    class Meta:
        model = Taxonomia
        fields = ("id", "tipo", "parent", "nombre_taxonomia")

    def update(self, instance, validated_data):
        data_taxonomia = self._kwargs['data'].get("nombre_taxonomia", None)
        if data_taxonomia:
            nombre_taxonomia = NombreTaxonomia.objects.get(
                id=data_taxonomia['id'])
            nombre_taxonomia.nombre = data_taxonomia['nombre']
            nombre_taxonomia.save()
        instance.save()

        return super(TaxonomiasNombreSerializer,
                     self).update(instance, validated_data)


class TaxonomiasNombreSerializerSave(serializers.ModelSerializer):
    nombre_taxonomia = NombreProductosModelSerializer(read_only=True)

    class Meta:
        model = Taxonomia
        fields = ("id", "tipo", "nombre", "parent", "nombre_taxonomia")

    def create(self, data):
        print("llego")
        empresa_id = self._kwargs['data']["empresa"]
        slug_name = self._kwargs['data']["slug_name"]
        empresa = Empresa.objects.get(id=empresa_id)
        nombre_taxonomia = NombreTaxonomia.objects.create(
            nombre=data['nombre'], slug_name=slug_name)
        taxonomia = Taxonomia.objects.create(**data,
                                             empresa=empresa,
                                             nombre_taxonomia=nombre_taxonomia)

        return taxonomia