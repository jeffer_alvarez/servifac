# rest framework
from rest_framework import serializers
from collections import OrderedDict
from django.db import connection, reset_queries
# models
from servifac.empresa.models import Notifications


class NotificationsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Notifications
        fields = ("id", "is_propietario", "is_client", "is_anonymus", "token",
                  "uid_device", "id_user")
