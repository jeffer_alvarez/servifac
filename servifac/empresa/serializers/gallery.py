# rest framework
from rest_framework import serializers
from collections import OrderedDict
from django.db import connection, reset_queries
# models
from servifac.empresa.models import Gallery


class GallerySerializer(serializers.ModelSerializer):
    class Meta:
        model = Gallery
        fields = "__all__"
