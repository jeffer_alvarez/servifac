# rest framework
from rest_framework import serializers
from collections import OrderedDict
# models
from servifac.empresa.models import Producto, Establecimiento, Taxonomia, Empresa, ProductoTipoTaxonimia, NombreTaxonomia
from urllib.parse import urlparse
import requests
import ast
from django.core.files.base import ContentFile
# serializers
from servifac.empresa.serializers import EstablecimientoModelSerializer

from .taxonomias import TaxonomiaProductosModelSerializer

from .tipo_taxonomia import TiposTaxonomiaProductoSerializerProducto


class TaxonomiaModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Taxonomia
        fields = "__all__"


class TaxonomiaModelSerializerRelacion(serializers.ModelSerializer):
    class Meta:
        model = Taxonomia
        fields = ['id', "nombre_taxonomia"]


class NombreTaxonomiaSerializer(serializers.ModelSerializer):
    class Meta:
        model = NombreTaxonomia
        fields = "__all__"


class TiposTaxonomiaSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductoTipoTaxonimia
        fields = "__all__"


class ProductoModelSerializerMetas(serializers.ModelSerializer):
    class Meta:
        model = Producto
        fields = ("id", "imagen", "card", "thumbnail", "descripcion")


class ProductoModelSerializer(serializers.ModelSerializer):
    descripcion = serializers.CharField(max_length=500,
                                        allow_blank=False,
                                        required=True)

    precio_a = serializers.DecimalField(max_digits=10,
                                        decimal_places=2,
                                        allow_null=False,
                                        required=True)

    class Meta:
        model = Producto
        fields = ("establecimiento", "id", "imagen", "card", "thumbnail",
                  "descripcion", "precio_a", "codigo_producto", "codigo_barra",
                  "cantidad", "precio_costo", "valor_iva", "iva",
                  "porcentaje_iva", "precio_b", "precio_c", "stock_minimo",
                  "fecha_vencimiento", "fraccionable", "is_active",
                  "estado_stock")
        read_only_fields = (
            "establecimiento",
            "codigo_producto",
            "codigo_barra",
            "cantidad",
            "precio_costo",
            "valor_iva",
            "iva",
            "porcentaje_iva",
            "precio_b",
            "precio_c",
            "stock_minimo",
            "fecha_vencimiento",
            "fraccionable",
        )

    def validate(self, data):
        establecimiento_id = self._kwargs['data'].get('establecimiento', None)
        try:
            if type(establecimiento_id) == str:
                establecimiento_id = int(establecimiento_id)
        except:
            pass
        # Validar que el establecimiento solo reciba valores númericos
        if establecimiento_id == None or type(establecimiento_id) == str:
            raise serializers.ValidationError(
                'El campo establecimiento solo acepta valores númericos')
        # Validar que el establecimiento exista en la base de datos
        else:
            establecimiento_exists = Establecimiento.objects.filter(
                pk=establecimiento_id).exists()
            if establecimiento_exists == False:
                raise serializers.ValidationError(
                    'El establecimiento ingresado no existe')

            # Validar que el establecimiento ingresado pertenezca a la empresa de propietario
            else:
                usuario_empresa = self._kwargs['data'].get('usuario_empresa')
                establecimiento = Establecimiento.objects.get(
                    pk=establecimiento_id)
                establecimiento_empresa = establecimiento.empresa
                if establecimiento_empresa != usuario_empresa:
                    raise serializers.ValidationError(
                        'El establecimiento no pertenece a tu empresa')
        print("salio del validate")
        return data

    def create(self, data):
        establecimiento_id = self._kwargs['data']['establecimiento']
        establecimiento = Establecimiento.objects.get(pk=establecimiento_id)
        original = self._kwargs['data'].get("img_original", None)
        card = self._kwargs['data'].get("card", None)
        thumbnail = self._kwargs['data'].get("thumbnail", None)
        categorias_data = self._kwargs['data'].get("categorias", None)
        if categorias_data:
            categorias = ast.literal_eval(categorias_data)
        else:
            categorias = None
        if original == None:
            producto = Producto.objects.create(**data,
                                               establecimiento=establecimiento)
        else:
            name_original = urlparse(original).path.split('/')[-1]
            name_card = urlparse(card).path.split('/')[-1]
            name_thumbnail = urlparse(thumbnail).path.split('/')[-1]
            img_url = 'https://mitiendavirtual.s3.amazonaws.com/static/'
            response_original = requests.get(img_url + name_original)
            response_card = requests.get(img_url + name_card)
            response_thumbnail = requests.get(img_url + name_thumbnail)
            if response_original.status_code == 200:
                if response_card.status_code == 200:
                    if response_thumbnail.status_code == 200:
                        producto = Producto()
                        producto.descripcion = data['descripcion']
                        producto.establecimiento = establecimiento
                        producto.precio_a = data['precio_a']
                        producto.imagen.save(name_original,
                                             ContentFile(
                                                 response_original.content),
                                             save=True)
                        producto.card.save(name_card,
                                           ContentFile(response_card.content),
                                           save=True)
                        producto.thumbnail.save(
                            name_thumbnail,
                            ContentFile(response_thumbnail.content),
                            save=True)
                        producto.save()
        if categorias:
            if type(categorias) == list:
                for relacion in categorias:
                    try:
                        existe = ProductoTipoTaxonimia.objects.get(
                            producto=producto, taxonomia=int(relacion))
                        if existe:
                            pass
                    except ProductoTipoTaxonimia.DoesNotExist:
                        print("se creo")
                        taxonomia = Taxonomia.objects.get(id=int(relacion))
                        data = ProductoTipoTaxonimia.objects.create(
                            producto=producto, taxonomia=taxonomia)
            else:
                pass
                """ taxonomia = Taxonomia.objects.get(
                    id=self._kwargs['data']['categorias']['taxonomia'])
                data = ProductoTipoTaxonimia.objects.create(
                    producto=producto, taxonomia=taxonomia) """

        return producto


class ProductoList(serializers.ModelSerializer):
    class Meta:
        model = Producto
        fields = (
            "id",
            "descripcion",
            "precio_a",
        )


class ProductoListOrdenCompra(serializers.ModelSerializer):
    class Meta:
        model = Producto
        fields = ("descripcion", "imagen", "card", "thumbnail", "precio_a")


class ProductoFilterModelSerializer(serializers.ModelSerializer):
    descripcion = serializers.CharField(max_length=500,
                                        allow_blank=False,
                                        required=True)
    precio_a = serializers.DecimalField(max_digits=10,
                                        decimal_places=2,
                                        allow_null=False,
                                        required=True)
    tipo_producto = TiposTaxonomiaProductoSerializerProducto(many=True,
                                                             read_only=True)

    class Meta:
        model = Producto
        fields = ("establecimiento", "id", "imagen", "thumbnail", "card",
                  "descripcion", "precio_a", "codigo_producto", "codigo_barra",
                  "cantidad", "precio_costo", "valor_iva", "iva",
                  "porcentaje_iva", "precio_b", "precio_c", "stock_minimo",
                  "fecha_vencimiento", "fraccionable", "is_active",
                  "tipo_producto", "estado_stock")
        read_only_fields = (
            "establecimiento",
            "codigo_producto",
            "codigo_barra",
            "cantidad",
            "precio_costo",
            "valor_iva",
            "iva",
            "porcentaje_iva",
            "precio_b",
            "precio_c",
            "stock_minimo",
            "fecha_vencimiento",
            "fraccionable",
        )