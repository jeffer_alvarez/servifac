#rest framework
from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from collections import OrderedDict

#models
from servifac.empresa.models import Establecimiento, ControlSecuencial

#serializers
from servifac.empresa.serializers import EmpresaModelSerializer, ControlSecuencialModelSerializer, EmpresaModelSerializerTienda


class EstablecimientoModelSerializer(serializers.ModelSerializer):

    codigo = serializers.CharField(max_length=3)

    class Meta:
        model = Establecimiento
        fields = [
            "id", 'codigo', 'direccion', "parroquia", "canton", "latitud",
            "longitud", "ciudad"
        ]
        read_only_fields = ("id", )

    def validate(self, data):
        data_establecimiento = dict(OrderedDict(data))

        if data_establecimiento.get("codigo"):
            pass

        return data

    def create(self, data):
        dict_control_secuencial = {
            'numero_facturas': 1,
            'numero_retenciones': 1,
            'numero_notas_credito': 1,
            'numero_notas_debito': 1,
            'nota_venta': 1,
            'numero_guias_remision': 1,
            'proformas': 1,
            'ordenes_compras': 1
        }
        # Creamos un control secuencial para nuevo establecimiento
        control_secuencial_establecimiento = ControlSecuencial.objects.create(
            **dict_control_secuencial)

        empresa = self._kwargs['data']['empresa']
        establecimiento = Establecimiento.objects.create(
            **data,
            empresa=empresa,
            control_secuencial=control_secuencial_establecimiento)

        return establecimiento


class EstablecimientoModelSerializerTienda(serializers.ModelSerializer):
    empresa = EmpresaModelSerializerTienda(required=True)

    class Meta:
        model = Establecimiento
        fields = ['latitud', 'longitud', "ciudad", "empresa"]
