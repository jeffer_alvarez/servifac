# rest framework
from rest_framework import serializers
from collections import OrderedDict
from django.db import connection, reset_queries
# models
from servifac.empresa.models import Empresa, ConfiguracionEmpresa
from servifac.users.models import Rol_contador, Rol_propietario, User

# Serializer
from .controles_secuenciales import ControlSecuencialModelSerializer


class ConfiguracionEmpresaSerializerTienda(serializers.ModelSerializer):
    class Meta:
        model = ConfiguracionEmpresa
        fields = ("servicio_domicilio", "horario_atencion")


class UserModelSerializerTienda(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['phone_number']


class FiltroPropietarioModelSerializer(serializers.ModelSerializer):
    user = UserModelSerializerTienda(required=True, many=False)

    class Meta:
        model = Rol_propietario
        fields = [
            "id", "portada", "portada_small", "portada_large", 'slug_name',
            "nombre_comercial", "descripcion", "user"
        ]


class EmpresaModelSerializer(serializers.ModelSerializer):
    """Model serializer empreza."""

    control_secuencial = ControlSecuencialModelSerializer(read_only=True)

    class Meta:
        model = Empresa
        fields = [
            'id', 'control_secuencial', 'propietario', 'contador',
            'clave_firma_electrinica', 'firma_electronica', 'ambiente',
            'contribuyente_especial', 'direccion', 'obligado_contabilidad',
            'logo', "tipo_negocio", "indexar", "mostrar_precios", "logo_small",
            "logo_large"
        ]
        read_only_fields = (
            "propietario",
            "control_secuencial",
        )

    def validate(self, data):
        data_dic = dict(OrderedDict(data))
        contador = data_dic.get("contador", None)
        if contador != None:
            try:
                go = Rol_contador.objects.get(pk=data_dic['contador'])
            except Rol_contador.DoesNotExist:
                raise serializers.ValidationError({
                    "estado":
                    False,
                    "message":
                    "El contador ingresado no esta registrado"
                })

        return data


class EmpresaModelSerializerTienda(serializers.ModelSerializer):
    """Model serializer empreza."""
    propietario = FiltroPropietarioModelSerializer(read_only=True)
    ConfiguracionEmpresa = ConfiguracionEmpresaSerializerTienda(read_only=True,
                                                                many=True)

    class Meta:
        model = Empresa
        fields = [
            "id", 'logo', "logo_small", "logo_large", "propietario",
            "ConfiguracionEmpresa"
        ]
