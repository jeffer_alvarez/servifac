#rest framework
from rest_framework import serializers

#models
from servifac.empresa.models import Producto_orden_compra, Producto
from servifac.empresa.serializers import ProductoListOrdenCompra

class CreateProductoOrdenModelSerializer(serializers.ModelSerializer):


    class Meta:
        model = Producto_orden_compra
        fields = ['producto','cantidad','precio_total','precio_unitario']


class ProductoOrdenModelSerializer(serializers.ModelSerializer):

    producto=ProductoListOrdenCompra(many=True,required=True)
    class Meta:
        model = Producto_orden_compra
        fields = ['orden_compras','producto','cantidad','precio_total','precio_unitario']

class ListaOrdenesCompras(serializers.ModelSerializer):
    
    producto = ProductoListOrdenCompra(read_only=True)

    class Meta:
        model = Producto_orden_compra
        fields = ["id","orden_compras",'producto','cantidad','precio_total','precio_unitario']
