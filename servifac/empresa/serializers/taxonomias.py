# rest framework
from rest_framework import serializers
from collections import OrderedDict
# models
from servifac.empresa.models import Taxonomia, Producto


class TaxonomiaModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Taxonomia
        fields = "__all__"


class ProductoListOrdenCompra(serializers.ModelSerializer):
    class Meta:
        model = Producto
        fields = ("descripcion", "imagen", "card", "thumbnail", "precio_a")


class TaxonomiaProductosModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Taxonomia
        fields = ["id", "nombre", "tipo"]
