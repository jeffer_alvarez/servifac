"""Empresa Admin models."""

# Django
from django.contrib import admin

# Models
from .models import *


@admin.register(Establecimiento)
class EstablecimientoAdmin(admin.ModelAdmin):
    list_display = (
        'empresa',
        'control_secuencial',
        'codigo',
        'direccion',
        'is_active',
    )
    list_filter = ('is_active', 'created', 'modified')


@admin.register(Empresa)
class EmpresaAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "propietario",
        'control_secuencial',
        'contador',
        'clave_firma_electrinica',
        'firma_electronica',
        'direccion',
        'is_active',
    )
    list_filter = ('is_active', 'created', 'modified')


@admin.register(ControlSecuencial)
class ControlSecuencialAdmin(admin.ModelAdmin):
    list_display = (
        'numero_facturas',
        'numero_retenciones',
        'numero_notas_credito',
        'numero_notas_debito',
        'numero_guias_remision',
        'proformas',
        'ordenes_compras',
    )


@admin.register(Orden_compras)
class OrdenComprasAdmin(admin.ModelAdmin):
    list_display = ('cliente', 'numero_orden', 'precio_total', 'subtotal',
                    'pedido_despachado', 'pedido_realizado',
                    'pedido_incompleto', 'pedido_reconfirmado',
                    'pedido_entregado', 'pedido_cancelado', 'hash_url')


@admin.register(Producto)
class ProductoAdmin(admin.ModelAdmin):
    list_display = ("id", 'establecimiento', 'descripcion', 'precio_costo')


@admin.register(Producto_orden_compra)
class ProductoOrdenCompraAdmin(admin.ModelAdmin):
    list_display = ('orden_compras', 'producto', 'cantidad',
                    'cantidad_existente', 'precio_total', 'precio_unitario')


@admin.register(Taxonomia)
class TaxonomiaOrdenCompraAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        'empresa',
        'nombre_taxonomia',
        'nombre',
        'parent',
        'tipo',
    )


@admin.register(ConfiguracionEmpresa)
class ConfiguracionEmpresaAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        'empresa',
        'primary',
        'secondary',
        'dark',
        'accent',
        'link',
        'servicio_domicilio',
        'code_pais',
        'pais',
        'horario_atencion',
    )


@admin.register(NombreTaxonomia)
class ConfiguracionNombreTaxonomiaAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        'nombre',
        'slug_name',
    )


@admin.register(Gallery)
class GalleryAdmin(admin.ModelAdmin):
    list_display = ("id", "img_small", 'img_large', 'img_medium',
                    'producto_id', 'establecimiento_id')


@admin.register(Notifications)
class NotificationsAdmin(admin.ModelAdmin):
    list_display = ("id", "is_propietario", 'is_client', 'is_anonymus',
                    'token', 'uid_device', 'id_user')


@admin.register(ProductoTipoTaxonimia)
class ConfiguracionProductoTipoTaxonimiaAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        'producto',
        'taxonomia',
    )
