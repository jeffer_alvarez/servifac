from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class ComprobantesConfig(AppConfig):
    name = "servifac.comprobantes_sri"
    verbose_name = _("Comprobantes Sri")

    
