"""Model, Contiene a Factura Auxiliares Mensuales"""

# django
from django.db import models
from django.utils.translation import ugettext_lazy as _

# models
from servifac.utils.models import UtilModel


class Factura_auxiliar_mensual(UtilModel):
    """Model Factura Auxiliar Mensual."""

    auxiliar_mensual = models.ForeignKey(
        'comprobantes_sri.Auxiliar_mensual',
        verbose_name=_("Auxiliar Mnejsual"),
        on_delete=models.CASCADE
        )
    fecha = models.DateTimeField(_("Fecha"), auto_now=False, auto_now_add=False)
    nombre = models.CharField(_("Nombre"), max_length=50)
    detalles = models.TextField(_("Detalles"))
    subtotal_cero = models.DecimalField(_("Subtotal 0"), max_digits=10, decimal_places=2)
    subtotal_doce = models.DecimalField(_("Subtotal 12"), max_digits=10, decimal_places=2)
    iva = models.DecimalField(_("Iva"), max_digits=10, decimal_places=2)
    total = models.DecimalField(_("Total"), max_digits=10, decimal_places=2)

    def __str__(self):
        return self.nombre
