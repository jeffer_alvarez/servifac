"""Model, Contine a Pagos.
tiene un ForeignKey con Informacion factura
"""

# django
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Pagos(models.Model):
    """Model pagos."""

    info_factura = models.ForeignKey(
        'comprobantes_sri.Info_factura',
        verbose_name=_("Fk informacion factura"),
        on_delete=models.CASCADE
        )
    forma_pago = models.CharField(_("Forma de Pago"), max_length=2)
    dias_plazo = models.IntegerField(_("Dias Plazo"))
    total = models.DecimalField(_("Total"), max_digits=10, decimal_places=2)

    def __str__(self):
        return self.forma_pago
