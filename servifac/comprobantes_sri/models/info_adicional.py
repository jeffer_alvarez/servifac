"""Model, Contiene a informacion adicional."""

# django
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Info_adicional(models.Model):
    """Model info adicional."""

    factura = models.ForeignKey(
        'comprobantes_sri.Factura',
        verbose_name=_("Fk factura"),
        on_delete=models.CASCADE
        )
    nombre = models.CharField(_("Nombre"), max_length=2)
    descripcion = models.CharField(_("Descripcion"), max_length=2)

    def __str__(self):
        return self.nombre
