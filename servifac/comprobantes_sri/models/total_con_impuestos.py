"""Model, Contine para Total con Impuestos.
tiene un ForeignKey con Informacion factura
"""

# django
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Total_con_impuestos(models.Model):
    """Model Total con Impuestos."""

    info_factura = models.ForeignKey(
        'comprobantes_sri.info_factura',
        verbose_name=_("Fk informacion factura"),
        on_delete=models.CASCADE) 
    codigo = models.CharField(_("Codigo"), max_length=3)
    codigo_porcentaje = models.CharField(_("Codigo porcentaje"), max_length=3)
    base_imponible = models.DecimalField(_("Base imponible"), max_digits=10, decimal_places=2)
    valor = models.DecimalField(_("Valor"), max_digits=10, decimal_places=2)

    def __str__(self):
        return self.codigo
