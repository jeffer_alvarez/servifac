"""Model, Contine a  Informacion factura."""

# django
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Info_factura(models.Model):
    """Model Informacion factura."""

    fecha_emision = models.DateTimeField(_("Fecha emision"), auto_now=False, auto_now_add=False)
    dir_establecimiento = models.CharField(_("Direccion establecimiento"), max_length=150)
    obligado_contabilidad = models.BooleanField(_("Obligado a llevar contabilidad"))
    tipo_identificacion_comprador = models.CharField(_("Tipo de identificacion del comprador"), max_length=150)
    razon_social_comprador = models.CharField(_("razon social del comprador"), max_length=150)
    identificacion_comprador = models.CharField(_("Identificación del comprador"), max_length=150)
    direccion_comprador = models.CharField(_("Dirección del comprador"), max_length=150)
    total_sin_impuestos = models.DecimalField(_("Total sin impuestos"), max_digits=10, decimal_places=2)
    total_desceunto = models.DecimalField(_("Total descuento"), max_digits=10, decimal_places=2)
    propina = models.DecimalField(_("Propina"), max_digits=10, decimal_places=2)
    importe_total = models.DecimalField(_("Importe total"), max_digits=10, decimal_places=2)
    moneda = models.CharField(_("Moneda"), max_length=10)

    def __str__(self):
        return self.razon_social_comprador
