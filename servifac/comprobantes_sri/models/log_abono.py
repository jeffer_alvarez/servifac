"""Model, Contine log abono."""

# django
from django.db import models
from django.utils.translation import ugettext_lazy as _

# models
from servifac.utils.models import UtilModel


class Log_abono(UtilModel):
    """Model Log abono."""

    factura = models.ForeignKey(
        'comprobantes_sri.Factura',
        verbose_name=_("Fk Factura"),
        on_delete=models.CASCADE
    )
    fecha_abono = models.DateTimeField(_("Fecha Abono"), auto_now=False, auto_now_add=False)
    valor_pendiente = models.DecimalField(_("Valor Pendiente"), max_digits=10, decimal_places=2)
    valor = models.DecimalField(_("Valor"), max_digits=10, decimal_places=2)
    descripcion = models.CharField(_("Descripcion"), max_length=150)
    efectivo = models.BooleanField(_("Efectivo"))
    tipo_abono = models.CharField(_("Tipo abono"), max_length=50)
    numero_tarjeta = models.CharField(_("Numero Tarjeta"), max_length=50)
    nombre_banco = models.CharField(_("Nombre Banco"), max_length=50)
    numero_cheque = models.CharField(_("Numero Cheque"), max_length=50)

    def __str__(self):
        return self.descripcion
