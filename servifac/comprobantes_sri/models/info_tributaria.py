"""Model, Contiene a informacion tributaria."""

# django
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Info_tributaria(models.Model):
    """Model Informacion tributaria """

    ambiente = models.CharField(_("Ambiente"), max_length=1)
    tipo_emision = models.CharField(_("Tipo emision"), max_length=15)
    razon_social = models.CharField(_("Razon social"), max_length=150)
    nombre_comercial = models.CharField(_("Nombre comercial"), max_length=150)
    ruc = models.CharField(_("Ruc"), max_length=13)
    clave_acceso = models.CharField(_("Clave de acceso"), max_length=49)
    cod_doc = models.CharField(_("Codigo documento"), max_length=2)
    estab = models.CharField(_("establecimiento"), max_length=3)
    pto_emi = models.CharField(_("Punto de emicion"), max_length=3)
    secuencial = models.CharField(_("Secuencial"), max_length=9)
    dir_matriz = models.CharField(_("Direccion matriz"), max_length=150)

    def __str__(self):
        return self.razon_social
