"""Model, Contiene a Impuestos SRI."""

# django
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Impuestos(models.Model):
    """Model impuestos Sri."""

    detalle_sri = models.ForeignKey(
        'comprobantes_sri.Detalles_sri',
        verbose_name=_("Detallle sri"),
        on_delete=models.CASCADE
        )
    codigo = models.CharField(_("Codigo"), max_length=2)
    codigo_porcentaje = models.CharField(_("Codigo Porcentaje"), max_length=2)
    tarifa = models.CharField(_("tarifa"), max_length=2)
    base_imponible = models.DecimalField(_("Base Imponible"), max_digits=10, decimal_places=2)
    valor = models.DecimalField(_("Valor"), max_digits=10, decimal_places=2)

    def __str__(self):
        return self.codigo
