"""Model, Contiene a Detalles del comprobante sri."""

# django
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Detalles_sri(models.Model):
    """Model Detalles Sri."""

    factura = models.ForeignKey(
        'comprobantes_sri.Factura',
        verbose_name=_("Fk factura"),
        on_delete=models.CASCADE
        )
    codigo_principal = models.CharField(_("Codigo Principal"), max_length=25)
    descripcion = models.CharField(_("Descripcion"), max_length=150)
    cantidad = models.CharField(_("Cantidad"), max_length=5)
    precio_unitario = models.DecimalField(_("Precio Unitario"), max_digits=10, decimal_places=2)
    descuento = models.DecimalField(_("Descuento"), max_digits=10, decimal_places=2)
    precio_total_sin_impuestos = models.DecimalField(_("Precio Total sin impuestos"), max_digits=5, decimal_places=2)
    moneda = models.CharField(_("Modena"), max_length=5)
    

    def __str__(self):
        return self.codigo_principal
