"""Model, Contine a Factua SRI ."""

# django
from django.db import models
from django.utils.translation import ugettext_lazy as _

# Models
from servifac.utils.models import UtilModel


class Factura(UtilModel):
    """Model factura."""

    tipo_factura = models.CharField(_("Tipo Factura"), max_length=50)
    referencia = models.IntegerField(_("Referencia"))
    establecimiento = models.ForeignKey(
        'empresa.Establecimiento',
        verbose_name=_("Establecimiento"),
        on_delete=models.CASCADE
        )    
    credito = models.BooleanField(_("Credito"))
    dias_plazo = models.PositiveIntegerField(_("Dias Plazo"))
    fecha_vencida = models.DateTimeField(_("Fecha Vencimiento"), auto_now=False, auto_now_add=False)
    dias_mora = models.PositiveIntegerField(_("Dias Mora"))

    info_factura = models.OneToOneField(
        'comprobantes_sri.Info_factura',
        verbose_name=_("Informacion Factura"),
        on_delete=models.CASCADE
    )
    info_tributaria = models.OneToOneField(
        'comprobantes_sri.Info_tributaria',
        verbose_name=_("Informacion Tributaria"),
        on_delete=models.CASCADE
    )
    
    def __str__(self):
        return self.id
