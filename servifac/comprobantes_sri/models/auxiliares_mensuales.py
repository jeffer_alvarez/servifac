"""Model, Contiene a Auxiliares Mensuales"""

# django
from django.db import models
from django.utils.translation import ugettext_lazy as _

# models
from servifac.utils.models import UtilModel


class Auxiliar_mensual(UtilModel):
    """Model Auxiliar Mensual."""

    nombre = models.CharField(_("Nombre"), max_length=50)

    propietario = models.ForeignKey(
        'users.Rol_propietario',
        verbose_name=_("Propietario"),
        on_delete=models.CASCADE
        )
    tipo_auxiliar_mesual = models.CharField(_("Tipo auxiliar mensual"), max_length=50)
    fecha_inicio = models.DateTimeField(_("Fecha inicio"), auto_now=False, auto_now_add=False)
    decha_fin = models.DateTimeField(_("Fecha fin"), auto_now=False, auto_now_add=False)
    subtotal_cero = models.DecimalField(_("Subtotal 0"), max_digits=10, decimal_places=2)
    subtotal_doce = models.DecimalField(_("Subtotal 12"), max_digits=10, decimal_places=2)
    iva = models.DecimalField(_("Iva"), max_digits=10, decimal_places=2)
    total = models.DecimalField(_("Total"), max_digits=10, decimal_places=2)

    def __str__(self):
        return self.nombre
