"""Model, Contiene a log_notificaciones_facturas_vencidas"""

# django
from django.db import models
from django.utils.translation import ugettext_lazy as _

# models
from servifac.utils.models import UtilModel


class log_notificaciones_facturas_vencidas(UtilModel):
    """Model log_notificaciones_facturas_vencidas."""

    cliente = models.ForeignKey(
        'users.Rol_cliente',
        verbose_name=_("Cliente"),
        on_delete=models.CASCADE
        )
    factura = models.ForeignKey(
        'comprobantes_sri.Factura',
        verbose_name=_("Factura"),
        on_delete=models.CASCADE
        )
    mensaje = models.CharField(_("Mensajes"), max_length=150)
    fecha_notificacion = models.DateTimeField(_("Fecha Notificacion"), auto_now=False, auto_now_add=False)
    total_deuda = models.DecimalField(_("Total Deuda"), max_digits=10, decimal_places=2)
    total_abonos = models.DecimalField(_("Total Abonos"), max_digits=10, decimal_places=2)
    dias_mora = models.PositiveIntegerField(_("Dias Mora"))
    saldo_pendiente = models.DecimalField(_("Saldo Pendiente"), max_digits=10, decimal_places=2)

    
        