from .clientes import (ClienteModelSerializer, ConsultaClienteModelSerializer,
                       ClienteSignUpSerializer, ClienteLogInSerializer)
from .contador import (
    ContadorLogInSerializer,
    ContadorModelSerializer,
    ContadorSignUpSerializer,
)
from .propietarios import (PropietarioLogInSerializer,
                           PropietarioModelSerializer,
                           PropietarioSignUpSerializer,
                           ConsultaPropietarioModelSerializer,
                           FiltroPropietarioModelSerializer)
from .users import (UserModelSerializer, UserModelSerializerOrden)
