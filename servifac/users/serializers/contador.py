# Django
from django.contrib.auth import password_validation, authenticate
from django.core.validators import RegexValidator

# rest framework
from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from rest_framework.authtoken.models import Token

# models
from servifac.users.models import Rol_contador, User

# serializers
from .users import UserModelSerializer


# Model Serializer
class ContadorModelSerializer(serializers.ModelSerializer):

    user = UserModelSerializer(read_only=True)

    class Meta:
        model = Rol_contador
        fields = ['user', ]


# Signup Serializer
class ContadorSignUpSerializer(serializers.Serializer):

    email = serializers.EmailField(
        allow_blank=True, required=False,
    )

    password = serializers.CharField(min_length=8, max_length=64, allow_blank=False, required=True)

    # Datos personales
    identificacion = serializers.CharField(
        min_length=2,
        max_length=13,
        allow_blank=False,
        required=True,
    )

    first_name = serializers.CharField(min_length=2, max_length=30, allow_blank=False)
    last_name = serializers.CharField(min_length=2, max_length=30, allow_blank=True, required=False)
    direccion = serializers.CharField(max_length=150)

    phone_regex = RegexValidator(
        regex=r'\+?1?\d{9,15}$',
        message="Phone number must be entered in the format: +999999999. Up to 15 digits allowed."
    )
    phone_number = serializers.CharField(
        validators=[phone_regex]
    )

    def create(self, data):
        user = data.copy()
        user = User.objects.create_user(**user, is_contador=True)
        contador = Rol_contador.objects.create(
            user=user
        )

        return contador




# Login Serializer
class ContadorLogInSerializer(serializers.Serializer):

    identificacion = serializers.CharField(min_length=9, max_length=64)
    password = serializers.CharField(min_length=8, max_length=64)

    def validate(self, data):
        """ Verificar credenciales and is_contador """
        print(data['identificacion'])

        user = User.objects.get(identificacion=data['identificacion'], is_contador=True)

        if not user:
            raise serializers.ValidationError('El password o usuario son incorrectos')

        elif user.check_password(data['password']):
            self.context['user'] = user
            return data
            
        else:
            raise serializers.ValidationError('El password o usuario son incorrectos')
    def create(self, data):
        """Generate or retrieve new token."""
        token, created = Token.objects.get_or_create(user=self.context['user'])
        return self.context['user'], token.key
