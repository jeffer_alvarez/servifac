# Django
from django.contrib.auth import password_validation
from django.core.validators import RegexValidator
from collections import OrderedDict

# rest framework
from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from rest_framework.authtoken.models import Token
from urllib.parse import urlparse
from django.http import JsonResponse
from django.core.files.base import ContentFile
import requests

# models
from servifac.users.models import Rol_cliente, User
from servifac.empresa.models import Establecimiento
# serializers
from servifac.empresa.serializers import EstablecimientoModelSerializer
from .users import UserModelSerializer

from servifac.utils.authenticate import AuthenticateCliente


class ClienteModelSerializer(serializers.ModelSerializer):
    """Cliente Model Serializer """
    user = UserModelSerializer(required=True)
    establecimiento = EstablecimientoModelSerializer(read_only=True)

    class Meta:
        model = Rol_cliente
        fields = [
            'id',
            'user',
            'establecimiento',
            'razon_social',
        ]

    def update(self, instance, validated_data):
        """Update User and Cliente """
        if 'user' in validated_data:
            user_data = validated_data.pop('user')
            user = User.objects.get(
                identificacion=instance.user.identificacion, is_client=True)
            user_serializer = UserModelSerializer(user,
                                                  data=user_data,
                                                  partial=True)
            if user_serializer.is_valid(raise_exception=True):
                user_serializer.save()

        instance.save()
        return super(ClienteModelSerializer,
                     self).update(instance, validated_data)


class ConsultaClienteModelSerializer(serializers.ModelSerializer):
    user = UserModelSerializer(required=True, many=False)

    class Meta:
        model = Rol_cliente
        fields = [
            "user",
        ]


class ClienteSignUpSerializer(serializers.Serializer):
    # Credenciales para el sistema
    email = serializers.EmailField(
        allow_blank=True,
        required=False,
    )
    password = serializers.CharField(min_length=8,
                                     max_length=64,
                                     allow_blank=False,
                                     required=True)
    # Datos personales
    identificacion = serializers.CharField(
        min_length=2,
        max_length=75,
        allow_blank=False,
    )
    first_name = serializers.CharField(min_length=2,
                                       max_length=30,
                                       allow_blank=False)
    facebook_id = serializers.CharField(min_length=2,
                                        max_length=70,
                                        allow_blank=False)
    last_name = serializers.CharField(min_length=2,
                                      max_length=30,
                                      allow_blank=True,
                                      required=False)
    phone_regex = RegexValidator(
        regex=r'\+?1?\d{9,15}$',
        message=
        "Phone number must be entered in the format: +999999999. Up to 15 digits allowed."
    )

    foto = serializers.CharField()

    phone_number = serializers.CharField(validators=[phone_regex],
                                         required=False,
                                         allow_blank=True)

    # datos de la empresa
    razon_social = serializers.CharField(min_length=3,
                                         max_length=150,
                                         allow_blank=False,
                                         required=False)

    def validate(self, data):
        establecimiento = self._kwargs['data'].get('establecimiento')
        data_dic = dict(OrderedDict(data))
        identificacion = data_dic['identificacion']
        phone_number = data_dic['phone_number']
        cliente = User.objects.filter(identificacion=identificacion,
                                      is_client=True).exists()
        cliente_phone = User.objects.filter(
            is_client=True, phone_number=phone_number).exists()
        if cliente:
            if cliente_phone:
                pass
            else:
                raise serializers.ValidationError({
                    "estado":
                    False,
                    "message":
                    "Ya existe un cliente con estos datos"
                })
        else:
            pass

        #validar establecimiento usuario
        if establecimiento == None or type(establecimiento) == str:
            raise serializers.ValidationError(
                {"establecimiento": "Has ingresado un valor invalido."})

        else:
            establecimiento_exists = Establecimiento.objects.filter(
                pk=establecimiento).exists()
            if establecimiento_exists == False:
                raise serializers.ValidationError(
                    {"establecimiento": "El establecimiento no existe"})

        data['establecimiento'] = establecimiento
        return data

    def create(self, data):
        print(data['foto'])
        img_url = data['foto']
        name = data['first_name'].replace(" ", "")
        response = requests.get(img_url)
        if response.status_code == 200:
            user = data.copy()
            user.pop('razon_social')
            user.pop('establecimiento')
            #https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=2994841533929829&height=50&width=50&ext=1593399298&hash=AeQFN4mD1LQi9Mr8
            #User
            user_save = User.objects.create_user(**user, is_client=True)
            user = User.objects.get(pk=user_save.pk)
            print(user)
            user.foto.save(name + ".jpg",
                           ContentFile(response.content),
                           save=True)
            user.save()
            print(user.foto)
        else:
            user = data.copy()
            user.pop('razon_social')
            user.pop('establecimiento')
            #https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=2994841533929829&height=50&width=50&ext=1593399298&hash=AeQFN4mD1LQi9Mr8
            #User
            user = User.objects.create_user(**user, is_client=True)

        #CLiente
        establecimiento = Establecimiento.objects.get(
            pk=data['establecimiento'])
        cliente = Rol_cliente.objects.create(
            user=user,
            establecimiento=establecimiento,
            razon_social=data['razon_social'],
        )

        return cliente


class ClienteLogInSerializer(serializers.Serializer):
    identificacion = serializers.CharField(min_length=7, max_length=64)
    password = serializers.CharField(min_length=8, max_length=64)

    def validate(self, data):
        """ Verificar credenciales and is_cliente """

        user = AuthenticateCliente(username=data['identificacion'],
                                   password=data['password'])

        if not user:
            raise serializers.ValidationError(
                'El password o usuario son incorrectos')

        self.context['user'] = user
        return data

    def create(self, data):
        """Generate or retrieve new token."""
        token, created = Token.objects.get_or_create(user=self.context['user'])
        return self.context['user'], token.key
