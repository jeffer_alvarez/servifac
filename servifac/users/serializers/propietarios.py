# Django
from django.contrib.auth import password_validation
from django.core.validators import RegexValidator
from collections import OrderedDict
from urllib.parse import urlparse
from django.http import JsonResponse
import requests
from django.core.files.base import ContentFile
# rest framework
from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from rest_framework.authtoken.models import Token

# models
from servifac.users.models import Rol_propietario, User
from servifac.empresa.models import ControlSecuencial, Empresa, Establecimiento, ConfiguracionEmpresa

#serializers
from .users import UserModelSerializer, UserModelSerializerOrden

from servifac.utils.authenticate import AuthenticatePropietario


class PropietarioModelSerializer(serializers.ModelSerializer):
    """ Model PropietarioSerializer"""

    # tiene que ser requerido par apoderlo actualizar
    user = UserModelSerializer(required=True, many=False)

    class Meta:
        model = Rol_propietario
        fields = ("id", 'user', 'razon_social', 'ruc', 'nombre_comercial',
                  'slug_name', "descripcion", "portada", "portada_small",
                  "portada_large")

    def update(self, instance, validated_data):
        """Update User and Propietario """

        if 'user' in validated_data:
            user_data = validated_data.pop('user')
            user = User.objects.get(
                identificacion=instance.user.identificacion,
                is_propietario=True)
            user_serializer = UserModelSerializer(user,
                                                  data=user_data,
                                                  partial=True)
            if user_serializer.is_valid(raise_exception=True):
                user_serializer.save()

        instance.save()
        return super(PropietarioModelSerializer,
                     self).update(instance, validated_data)


class ConsultaPropietarioModelSerializer(serializers.ModelSerializer):
    user = UserModelSerializer(required=True, many=False)

    class Meta:
        model = Rol_propietario
        fields = ['slug_name', "user"]


class FiltroPropietarioModelSerializer(serializers.ModelSerializer):
    user = UserModelSerializerOrden(required=True, many=False)

    class Meta:
        model = Rol_propietario
        fields = ["id", 'slug_name', "nombre_comercial", "descripcion", "user"]


# Signup Serializer
class PropietarioSignUpSerializer(serializers.Serializer):
    # Credenciales para el sistema
    email = serializers.EmailField(
        allow_blank=True,
        required=False,
    )
    password = serializers.CharField(min_length=8,
                                     max_length=64,
                                     allow_blank=False,
                                     required=True)
    slug_name = serializers.CharField(
        min_length=3,
        max_length=150,
        allow_blank=False,
        required=True,
        validators=[UniqueValidator(queryset=Rol_propietario.objects.all())])
    # Datos personales
    identificacion = serializers.CharField(
        min_length=2,
        max_length=13,
        allow_blank=False,
        required=True,
    )
    first_name = serializers.CharField(min_length=2,
                                       max_length=150,
                                       allow_blank=False)
    last_name = serializers.CharField(min_length=2,
                                      max_length=150,
                                      allow_blank=True,
                                      required=False)
    phone_regex = RegexValidator(
        regex=r'\+?1?\d{9,15}$',
        message=
        "Phone number must be entered in the format: +999999999. Up to 15 digits allowed."
    )
    phone_number = serializers.CharField(validators=[phone_regex])

    # datos de la empresa
    nombre_comercial = serializers.CharField(min_length=3,
                                             max_length=150,
                                             allow_blank=False,
                                             required=True)
    parroquia = serializers.CharField(min_length=3,
                                      max_length=150,
                                      allow_blank=False,
                                      required=True)
    canton = serializers.CharField(min_length=3,
                                   max_length=150,
                                   allow_blank=False,
                                   required=True)
    ciudad = serializers.CharField(min_length=3,
                                   max_length=150,
                                   allow_blank=False,
                                   required=True)
    latitud = serializers.CharField(min_length=3,
                                    max_length=150,
                                    allow_blank=False,
                                    required=True)
    longitud = serializers.CharField(min_length=3,
                                     max_length=150,
                                     allow_blank=False,
                                     required=True)
    razon_social = serializers.CharField(min_length=3,
                                         max_length=150,
                                         allow_blank=True,
                                         required=False)
    ruc = serializers.CharField(
        max_length=13,
        allow_blank=True,
        required=False,
        validators=[UniqueValidator(queryset=Rol_propietario.objects.all())])

    def validate(self, data):
        data_dic = dict(OrderedDict(data))
        identificacion = data_dic['identificacion']
        phone_number = data_dic['phone_number']
        propietario = User.objects.filter(identificacion=identificacion,
                                          is_propietario=True,
                                          is_active=True).exists()
        propietario_phone = User.objects.filter(is_propietario=True,
                                                phone_number=phone_number,
                                                is_active=True).exists()
        if propietario:
            if propietario_phone:
                raise serializers.ValidationError({
                    "estado":
                    False,
                    "message":
                    "Ya existe un propietario con estos datos"
                })
            else:
                raise serializers.ValidationError({
                    "estado":
                    False,
                    "message":
                    "Ya existe un propietario con este numero de telefono registrado"
                })
        else:
            if propietario_phone:
                raise serializers.ValidationError({
                    "estado":
                    False,
                    "message":
                    "Ya existe un propietario con este numero de telefono"
                })

        return data

    def create(self, data):
        img_url = 'https://mitiendavirtual.s3-sa-east-1.amazonaws.com/imagenes_por_defecto/mitiendavirtual-imagen-defecto.png'
        name = urlparse(img_url).path.split('/')[-1]

        #User
        print(data['identificacion'])
        user = User()
        response = requests.get(img_url)
        if response.status_code == 200:
            user.foto.save(name, ContentFile(response.content), save=True)
            user.first_name = data['first_name']
            user.identificacion = data['identificacion']
            password_llego = data['password']
            user.set_password(password_llego)
            user.phone_number = data['phone_number']
            user.is_propietario = True
        else:
            print(data['identificacion'])
            user.first_name = data['first_name']
            user.identificacion = data['identificacion']
            password_llego = data['password']
            user.set_password(password_llego)
            user.phone_number = data['phone_number']
            user.is_propietario = True
        #Propietario
        descripcion_empresa = data.get("descripcion", None)

        if descripcion_empresa == None:
            print(user)
            user.save()
            propietario = Rol_propietario.objects.create(
                nombre_comercial=data['nombre_comercial'],
                slug_name=data['slug_name'],
                user=user)
        else:
            user.save()
            propietario = Rol_propietario.objects.create(
                nombre_comercial=data['nombre_comercial'],
                slug_name=data['slug_name'],
                descripcion=data['descripcion'],
                user=user)

        #Control secuencial
        print("paso control_secuencial")
        dict_control_secuencial = {
            'numero_facturas': 1,
            'numero_retenciones': 1,
            'numero_notas_credito': 1,
            'numero_notas_debito': 1,
            'nota_venta': 1,
            'numero_guias_remision': 1,
            'proformas': 1,
            'ordenes_compras': 1
        }
        # Creamos un control secuencial para empresa y establecimiento
        control_secuencial_empresa = ControlSecuencial.objects.create(
            **dict_control_secuencial)
        control_secuencial_establecimiento = ControlSecuencial.objects.create(
            **dict_control_secuencial)

        # Creamos la Empresa
        empresa = Empresa.objects.create(
            control_secuencial=control_secuencial_empresa,
            propietario=propietario,
            ambiente="1",
            direccion=data['parroquia'])

        # Creamos el Establecimeinto
        establecimiento = Establecimiento.objects.create(
            empresa=empresa,
            control_secuencial=control_secuencial_establecimiento,
            codigo="001",
            direccion=data['parroquia'],
            parroquia=data['parroquia'],
            canton=data['canton'],
            ciudad=data['ciudad'],
            latitud=data['latitud'],
            longitud=data['longitud'],
        )

        configuracion_empresa = self._kwargs['data'].get(
            "configuracion_empresa", None)
        if configuracion_empresa is not None:
            save_empresa = ConfiguracionEmpresa.objects.create(
                **configuracion_empresa, empresa=empresa)

        return propietario


# Login Serializer
class PropietarioLogInSerializer(serializers.Serializer):
    identificacion = serializers.CharField(min_length=8, max_length=64)
    password = serializers.CharField(min_length=8, max_length=64)

    def validate(self, data):
        """ Verificar credenciales and is_propietario """
        print("llego validate")

        user = AuthenticatePropietario(username=data['identificacion'],
                                       password=data['password'])

        if not user:
            raise serializers.ValidationError(
                'El password o usuario son incorrectos')

        self.context['user'] = user
        return data

    def create(self, data):
        """Generate or retrieve new token."""
        token, created = Token.objects.get_or_create(user=self.context['user'])
        return self.context['user'], token.key
