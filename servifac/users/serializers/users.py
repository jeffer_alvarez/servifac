# Django
from django.contrib.auth import password_validation, authenticate
from django.core.validators import RegexValidator

#rest framework
from rest_framework import serializers
from rest_framework.validators import UniqueValidator

#models
from servifac.users.models import User


class UserModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            "id", 'foto', 'identificacion', 'first_name', 'last_name',
            'fecha_nacimiento', 'direccion', 'email', "password_is_temporal",
            'phone_number'
        ]


class UserModelSerializerOrden(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["id", 'foto', 'identificacion', 'first_name', 'phone_number']
