from django.contrib import admin
from django.contrib.auth import admin as auth_admin
from django.contrib.auth import get_user_model

# Models
from .models import (
    Rol_cliente,
    Rol_contador,
    Rol_empleado,
    Rol_propietario,
    cuentas_plataforma_contribuyente,
)

User = get_user_model()


@admin.register(User)
class UserAdmin(admin.ModelAdmin):

    list_display = (
        'pk',
        'identificacion',
        'first_name',
        'last_name',
        'is_active',
        'is_propietario',
        'is_client',
        'is_empleado',
        'is_contador',
        'is_staff',

    )

    list_display_links = ('pk', 'identificacion',)

    list_editable = (
        'is_active',
        'is_propietario',
        'is_client',
        'is_empleado',
        'is_contador',
    )    

    list_filter = ('is_staff', 'created', 'modified')

    search_fields = (
        'email',
        'first_name',
        'last_name',
        'phone_number'
    )
    ordering = ('email',)

    readonly_fields = ('created', 'modified',)

@admin.register(Rol_cliente)
class Rol_clienteAdmin(admin.ModelAdmin):
    list_display = (
        'user',
        'establecimiento',
        'razon_social',
        'is_active'
    )
    list_filter = ('is_active', 'created', 'modified')


@admin.register(Rol_contador)
class Rol_contadorAdmin(admin.ModelAdmin):
    list_display = (
        'user',
        'is_active'
    )
    list_filter = ('is_active', 'created', 'modified')


@admin.register(Rol_empleado)
class Rol_empleadoAdmin(admin.ModelAdmin):
    list_display = (
        'user',
        'establecimiento',
        'is_active'
    )
    list_filter = ('is_active', 'created', 'modified')


@admin.register(Rol_propietario)
class Rol_propietarioAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'user',
        'razon_social',
        'ruc',
        'nombre_comercial',
        'slug_name',
        'is_active'
    )
    list_filter = ('is_active', 'created', 'modified')
