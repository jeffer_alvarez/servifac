#rest framework
from rest_framework.permissions import BasePermission

#models
from servifac.empresa.models import Establecimiento, Empresa
from servifac.users.models import Rol_propietario, Rol_cliente

#Permisos propietario
class IsPropietario(BasePermission):
    """
    Verificar si el usuario tien el rol propietario

    """
    def has_permission(self, request, view):
        return bool(request.user.is_authenticated and request.user.is_propietario)

class IsPropietarioUser(BasePermission):
    
    """
    Verificar si los establecimientos les pertenecen al propietario
    
    """
    def has_object_permission(self, request, view, obj):
        user_id = request.user
        propietario_id = obj.pk
        #import ipdb; ipdb.set_trace()
        try:
            Rol_propietario.objects.get(pk=propietario_id,user=user_id)            
        except Rol_propietario.DoesNotExist:
            return False
        return True

#Permisos clientes
class IsCliente(BasePermission):
    """
    Verificar si el usuario tiene el rol cliente

    """
    def has_permission(self, request, view):
        print(request.user.is_authenticated)
        print(request.user.is_client)
        return bool(request.user.is_authenticated and request.user.is_client)

class IsClienteUser(BasePermission):
    
    """
    Verificar si los establecimientos les pertenecen al propietario
    
    """
    def has_object_permission(self, request, view, obj):
        user_id = request.user
        cliente_id = obj.pk
        #import ipdb; ipdb.set_trace()
        cliente = Rol_cliente.objects.filter(pk=cliente_id,user=user_id)
        return cliente.exists()

