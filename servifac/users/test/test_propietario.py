from django.test import TestCase
from servifac.empresa.models import Empresa
from servifac.users.models import User,Rol_propietario

class PropietarioTestCase(TestCase):
    """Rol PropietarioTestCase"""
    def setUp(self):
        self.user = User.objects.create(
            identificacion="1900691930",
            username="1900691930",
            is_propietario=True,
            password="10011995dar",
            phone_number="+593985874084",
            first_name="JeffrinPro",
            direccion="Zumbi"
        )
        
    def test_code_generation(self):
        propietario=Rol_propietario.objects.create(
            user=self.user,
            nombre_comercial="Jefferson",
            slug_name="jefferson")

        print(propietario.id)
        print(propietario.user.id)