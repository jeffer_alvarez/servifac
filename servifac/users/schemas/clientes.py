import graphene

from graphene_django.types import DjangoObjectType

from servifac.users.models import Rol_cliente, User
from servifac.empresa.models import Establecimiento, Empresa, Orden_compras
from rest_framework.authtoken.models import Token
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ValidationError


class OrdenCompraType(DjangoObjectType):
    class Meta:
        model = Orden_compras


class EstablecimientoType(DjangoObjectType):
    class Meta:
        model = Establecimiento


class UserType(DjangoObjectType):
    class Meta:
        model = User


class TokenType(DjangoObjectType):
    class Meta:
        model = Token


class ClientesType(DjangoObjectType):
    class Meta:
        model = Rol_cliente

    token = graphene.String()

    def resolve_token(self, info):
        if self is not None:
            token, created = Token.objects.get_or_create(user=self.user)
            return token  # self is the current Project object
        return ''


class TokenInput(graphene.InputObjectType):
    id = graphene.ID()
    key = graphene.String()


class UserInput(graphene.InputObjectType):
    id = graphene.ID()
    identificacion = graphene.String()
    facebook_id = graphene.String()
    first_name = graphene.String()

    last_name = graphene.String()
    email = graphene.String()
    direccion = graphene.String()
    phone_number = graphene.String()
    is_client = graphene.Boolean()
    is_propietario = graphene.Boolean()
    token = graphene.String()


class ClienteInput(graphene.InputObjectType):
    """Esto que paso puto"""
    user = graphene.Field(UserInput)
    establecimiento = graphene.ID()
    razon_social = graphene.String()
    token = graphene.String()


class CreateCliente(graphene.Mutation):
    class Arguments:
        input = ClienteInput()

    cliente = graphene.Field(ClientesType)

    @staticmethod
    def mutate(root, info, input=None):
        existe_user_facebook = Rol_cliente.objects.filter(
            user__facebook_id=input.user.facebook_id,
            user__is_client=True).values("id", "user")
        existe_user_normal = Rol_cliente.objects.filter(
            user__identificacion=input.user.identificacion,
            user__is_client=True).values("id", "user")
        if existe_user_facebook:
            user = User.objects.get(pk=existe_user_facebook[0]['user'])
            token, created = Token.objects.get_or_create(user=user)
            print(token)
            cliente_instance = Rol_cliente.objects.get(
                pk=existe_user_facebook[0]['id'])
            cliente_instance.token = token
            return CreateCliente(cliente=cliente_instance)
        elif existe_user_normal:
            user = User.objects.get(pk=existe_user_facebook[0]['user'])
            token, created = Token.objects.get_or_create(user=user)
            cliente_instance = Rol_cliente.objects.get(
                pk=existe_user_normal[0]['id'])
            cliente_instance.token = token
            return CreateCliente(cliente=cliente_instance)
        else:
            establecimiento = Establecimiento.objects.get(
                pk=input.establecimiento)
            user = User(**input.user)
            user.username = input.user.facebook_id
            user.set_password(input.user.facebook_id)
            user.save()
            input.pop('user')
            input.pop('establecimiento')
            cliente_instance = Rol_cliente(**input,
                                           user=user,
                                           establecimiento=establecimiento)
            cliente_instance.save()
            token, created = Token.objects.get_or_create(user=user)
            cliente_instance.token = token
            # Notice we return an instance of this mutation
            return CreateCliente(cliente=cliente_instance)


class UpdateCliente(graphene.Mutation):
    class Arguments:
        id = graphene.Int(required=True)
        input = ClienteInput(required=True)

    cliente = graphene.Field(ClientesType)

    @staticmethod
    def mutate(root, info, id, input=None):
        cliente_instance = Rol_cliente.objects.get(pk=id, user__is_client=True)
        token = input.token
        if cliente_instance:
            try:
                token_existe = Token.objects.get(key=token,
                                                 user=cliente_instance.user.id)
            except Token.DoesNotExist:
                raise Exception("Token no valido")
            cliente_instance.razon_social = input.razon_social
            cliente_instance.save()

            return UpdateCliente(cliente=cliente_instance)

        return UpdateCliente(cliente=None)


class UpdateUser(graphene.Mutation):
    class Arguments:
        id = graphene.Int(required=True)
        input = UserInput(required=True)

    user = graphene.Field(UserType)

    @staticmethod
    def mutate(root, info, id, input=None):
        user_instance = User.objects.get(pk=id)
        token = input.token
        if user_instance:
            try:
                token_existe = Token.objects.get(key=token,
                                                 user=user_instance.id)
            except Token.DoesNotExist:
                raise Exception("Token no valido")

            for k, v in input.items():
                if (k == 'password') and (v is not None):
                    user_instance.set_password(input.password)
                else:
                    print(k, v)
                    setattr(user_instance, k, v)
            try:
                user_instance.full_clean()
                user_instance.save()
                return UpdateUser(user=user_instance)
            except ValidationError as e:
                return UpdateUser(user=user, errors=e)

        return UpdateUser(user=None)