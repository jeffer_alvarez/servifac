import graphene

from graphene_django.types import DjangoObjectType

from servifac.users.models import Rol_cliente, User
from servifac.empresa.models import Establecimiento, Empresa
from pusher import Pusher

from servifac.users.schemas import ClientesType, CreateCliente, UserType, UpdateCliente, UpdateUser, OrdenCompraType
from rest_framework.authtoken.models import Token


class Mutation(graphene.ObjectType):
    create_cliente = CreateCliente.Field()
    update_cliente = UpdateCliente.Field()
    update_user = UpdateUser.Field()


class Query(object):
    rol_cliente = graphene.Field(ClientesType, id=graphene.Int())
    rol_clientes = graphene.List(ClientesType)
    me = graphene.Field(UserType)

    def resolve_me(self, info):
        user = info.context.user
        if user.is_anonymous:
            raise Exception('Not logged in!')

        return user

    def resolve_rol_clientes(self, info, **kwargs):
        return Rol_cliente.objects.all()

    def resolve_rol_cliente(self, info, **kwargs):
        id = kwargs.get('id')
        if id is not None:
            return Rol_cliente.objects.get(pk=id)
        return None
