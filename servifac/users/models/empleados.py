"""Model, Contiene a Empleados"""

# django
from django.db import models
from django.utils.translation import ugettext_lazy as _
# models
from servifac.utils.models import UtilModel


class Rol_empleado(UtilModel): 
    """Model Propietario."""

    user = models.OneToOneField(
        'users.User',
        on_delete=models.CASCADE
        )
    establecimiento = models.ForeignKey(
        'empresa.Establecimiento',
        verbose_name=_("Establecimiento"),
        on_delete=models.CASCADE
        )

    def __str__(self):
        """Return user's str representation."""
        return str(self.user)
