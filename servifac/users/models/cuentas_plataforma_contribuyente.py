"""Model, Contiene a cuentas plataforma contribuyente"""

# django
from django.db import models
from django.utils.translation import ugettext_lazy as _

# models
from servifac.utils.models import UtilModel


class Cuentas_plataforma_contribuyente(UtilModel):
    """Model Cuentas plataforma contribuyectes."""

    propietario = models.ForeignKey(
        'users.Rol_propietario',
        verbose_name=_("Propietario"),
        on_delete=models.CASCADE
        )
    tipo_cuenta = models.CharField(_("Tipo cuenta"), max_length=50)
    usuario_o_correo = models.CharField(_("User o email"), max_length=150)
    password = models.CharField(_("Contraseña"), max_length=50)

    def __str__(self):
        return self.usuario_o_correo
                