"""Model, Contiene a Propietario"""

# django
from django.db import models
from django.utils.translation import ugettext_lazy as _
# models
from servifac.utils.models import UtilModel
from servifac.storage_backends import StaticStorage


class Rol_propietario(UtilModel):
    """Model Propietario."""

    user = models.OneToOneField('users.User',
                                related_name='propietario',
                                on_delete=models.CASCADE)
    razon_social = models.CharField(_("Razon Social"),
                                    max_length=150,
                                    blank=True,
                                    null=True)
    ruc = models.CharField(_("Ruc"), max_length=13, blank=True, null=True)
    nombre_comercial = models.CharField(_("Nombre Comercial"), max_length=150)
    slug_name = models.CharField(_("Slug Name"), max_length=150, unique=True)
    descripcion = models.CharField(_("Slug Name"),
                                   max_length=250,
                                   blank=True,
                                   null=True)
    portada = models.FileField(_("Portada"),
                               storage=StaticStorage(),
                               max_length=500,
                               blank=True,
                               null=True)
    portada_small = models.FileField(_("Portada small"),
                                     storage=StaticStorage(),
                                     max_length=500,
                                     blank=True,
                                     null=True)
    portada_large = models.FileField(_("Portada large"),
                                     storage=StaticStorage(),
                                     max_length=500,
                                     blank=True,
                                     null=True)

    def __str__(self):
        """Return user's str representation."""
        return str(self.user)
