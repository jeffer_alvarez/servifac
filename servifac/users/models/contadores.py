"""Model, Contiene a Contadores"""

# django
from django.db import models
from django.utils.translation import ugettext_lazy as _
# models
from servifac.utils.models import UtilModel


class Rol_contador(UtilModel):
    """Model contador."""

    user = models.OneToOneField(
        'users.User',
        on_delete=models.CASCADE
        )
        
    def __str__(self):
        """Return user's str representation."""
        return str(self.user)
