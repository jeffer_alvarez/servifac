"""Model, Contiene a Clientes"""

# django
from django.db import models
from django.utils.translation import ugettext_lazy as _
# models
from servifac.utils.models import UtilModel


class Rol_cliente(UtilModel):
    """Model cliente."""

    user = models.OneToOneField('users.User',
                                related_name='cliente',
                                on_delete=models.CASCADE)
    establecimiento = models.ForeignKey('empresa.Establecimiento',
                                        verbose_name=_("Establecimiento"),
                                        on_delete=models.CASCADE)
    razon_social = models.CharField(_("Razon Social"), max_length=150)
