import graphene

from graphene_django.types import DjangoObjectType

from servifac.users.models import Rol_cliente, User
from servifac.empresa.models import Establecimiento, Empresa


class EstablecimientoType(DjangoObjectType):
    class Meta:
        model = Establecimiento


class UserType(DjangoObjectType):
    class Meta:
        model = User


class ClientesType(DjangoObjectType):
    class Meta:
        model = Rol_cliente
