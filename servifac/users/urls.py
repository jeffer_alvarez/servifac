#django
from django.urls import path, include

#rest framework

#views
from servifac.users import views as users_views

from rest_framework import routers
router = routers.DefaultRouter()
router.register(r'user/contador',
                users_views.ContadorViewSet,
                basename='contador')
router.register(r'user/propietario',
                users_views.PropietarioViewSet,
                basename='propietario')
router.register(r'user/cliente',
                users_views.ClienteViewSet,
                basename='cliente')

#router.register(r'users/propietario/signup', users_views.PropietarioSignUpViewSet ,basename='signup')

# Contador
users_patterns = ([
    path('', include(router.urls)),
], 'users')
