# Django REST Framework
from rest_framework import mixins, status, viewsets
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny, IsAuthenticated
from servifac.users.permissions import IsPropietario, IsPropietarioUser
from rest_framework.response import Response
from rest_framework.generics import get_object_or_404
import boto3
# models
from servifac.users.models import Rol_propietario, User
from servifac.empresa.models import Empresa, Establecimiento
# serializers
from servifac.users.serializers import (
    PropietarioLogInSerializer,
    PropietarioModelSerializer,
    PropietarioSignUpSerializer,
    UserModelSerializer,
)
from servifac.empresa.serializers import (EmpresaModelSerializer,
                                          EstablecimientoModelSerializer)


class PropietarioViewSet(mixins.DestroyModelMixin, mixins.UpdateModelMixin,
                         mixins.RetrieveModelMixin, viewsets.GenericViewSet):

    queryset = Rol_propietario.objects.all()
    lookup_field = 'id'

    # permisos
    def get_permissions(self):
        if self.action in ['signup', 'login']:
            permission_classes = [AllowAny]
        else:
            permission_classes = [
                IsAuthenticated, IsPropietario, IsPropietarioUser
            ]
        return [permission() for permission in permission_classes]

    # Serializer según la acción
    def get_serializer_class(self):
        if self.action == 'signup':
            self.serializer_class = PropietarioSignUpSerializer

        elif self.action == 'login':
            self.serializer_class = PropietarioLogInSerializer

        else:
            self.serializer_class = PropietarioModelSerializer
        return self.serializer_class

    # SIGNUP
    """http://127.0.0.1:8000/user/propietario/signup/"""

    @action(detail=False, methods=['POST'])
    def signup(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        propietario = serializer.save()
        # add extra data

        data = {
            "message": "Propietario creado exitosamente",
            "estado": True,
        }
        return Response(data, status=status.HTTP_201_CREATED)

    # LOGIN
    """http://127.0.0.1:8000/user/propietario/login/ asda"""

    @action(detail=False, methods=['POST'])
    def login(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user, token = serializer.save()
        # Propietario
        propietario = Rol_propietario.objects.get(user=user)
        data_propietario = PropietarioModelSerializer(propietario).data
        password_is_temporal = data_propietario['user']['password_is_temporal']
        first_name = data_propietario['user']['first_name']
        identificacion = data_propietario['user']['identificacion']
        phone_number = data_propietario['user']['phone_number']
        data_propietario.pop("user")
        data = {
            "is_propietario": True,
            "is_cliente": False,
            "is_contador": False,
            "is_empleado": False,
            "password_is_temporal": password_is_temporal,
            "first_name": first_name,
            "identificacion": identificacion,
            "phone_number": phone_number,
            "message": "Inicio de sesión exitoso",
            'access_token': token,
            "id_propietario": data_propietario['id'],
            "slug_name": data_propietario['slug_name']
        }
        return Response(data, status=status.HTTP_201_CREATED)

    # RETRIEV GET
    """http://127.0.0.1:8000/user/propietario/{id}/"""

    def retrieve(self, request, *args, **kwargs):
        propietario = self.get_object()
        propietario = Rol_propietario.objects.get(user=propietario.user)
        data_propietario = PropietarioModelSerializer(propietario).data
        #Datos User
        data_user = data_propietario['user']
        #Data Propietario
        data_propietario.pop('user')
        data_propietario['user'] = data_user['id']
        #Data Empresa
        empresa = Empresa.objects.get(propietario=data_propietario['id'])
        data_empresa = EmpresaModelSerializer(empresa).data
        data_empresa.pop('control_secuencial')
        #Data Establecimientos
        establecimiento = Establecimiento.objects.get(
            empresa=data_empresa['id'])
        data_establecimiento = EstablecimientoModelSerializer(
            establecimiento).data
        data_establecimiento['empresa'] = data_empresa['id']
        data = {
            "user": data_user,
            "propietario": data_propietario,
            'empresa': data_empresa,
            "establecimiento": data_establecimiento
        }

        return Response(data)

    # Update Post and Pach
    """http://127.0.0.1:8000/user/propietario/{id}/"""

    def update(self, request, *args, **kwargs):
        foto = request.data.get("foto", None)
        portada = request.data.get("portada", None)
        if foto:
            user = User.objects.get(id=request.user.id)
            foto_nueva = {"user": {"foto": foto}}
            partial = kwargs.pop('partial', False)
            instance = self.get_object()
            serializer = self.get_serializer(instance,
                                             data=foto_nueva,
                                             partial=partial)
            serializer.is_valid(raise_exception=True)
            serializer.save()
            propietario = self.get_object()
            propietario_serializer = self.get_serializer(propietario)
            data = {
                "estado": "autorizado",
                "message": "Propietario actualizado exitosamente foto",
                "objeto": propietario_serializer.data
            }

        else:
            partial = kwargs.pop('partial', False)
            instance = self.get_object()
            serializer = self.get_serializer(instance,
                                             data=request.data,
                                             partial=partial)
            serializer.is_valid(raise_exception=True)
            serializer.save()

            propietario = self.get_object()
            propietario_serializer = self.get_serializer(propietario)

            data = {
                "estado": "autorizado",
                "message": "Propietario actualizado exitosamente",
                "objeto": propietario_serializer.data
            }

        return Response(data)

    # Delete

    def perform_destroy(self, instance):
        """Disable propietario."""
        instance.is_active = False
        instance.save()

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        data = {
            'estado': "Autorizado",
            'message': "Propietario eliminado con exito"
        }
        return Response(data, status=status.HTTP_200_OK)

    @action(detail=True, methods=['get'])
    def recuperar_password(self, request, *args, **kwargs):
        instance = self.get_object()
        ordenes_compras = Orden_compras.objects.filter(cliente=instance.id)
        lista_ordenes = ViewOrdenCompra(ordenes_compras, many=True).data
        for lista in lista_ordenes:
            try:
                productos_orden = Producto_orden_compra.objects.filter(
                    orden_compras=lista['id'])
                productos_data = ListaOrdenesCompras(productos_orden,
                                                     many=True).data
                lista['productos'] = productos_data
            except Producto_orden_compra.DoesNotExist:
                lista['productos'] = []
        return Response(lista_ordenes)