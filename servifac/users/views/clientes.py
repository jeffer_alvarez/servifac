# Django REST Framework
from rest_framework import mixins, status, viewsets
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny, IsAuthenticated
from servifac.users.permissions import IsCliente, IsClienteUser
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from rest_framework.generics import get_object_or_404

# models
from servifac.users.models import Rol_cliente, User
from servifac.empresa.models import Establecimiento, Empresa, Orden_compras, Producto_orden_compra, Producto

#serializer
from servifac.empresa.serializers import ViewOrdenCompra, ProductoOrdenModelSerializer, ListaOrdenesCompras
from servifac.users.serializers import (
    ClienteModelSerializer,
    ClienteLogInSerializer,
    ClienteSignUpSerializer,
    UserModelSerializer,
)

from servifac.empresa.serializers import (
    EstablecimientoModelSerializer, )


class ClienteViewSet(mixins.DestroyModelMixin, mixins.UpdateModelMixin,
                     mixins.RetrieveModelMixin, viewsets.GenericViewSet):

    queryset = Rol_cliente.objects.all()
    lookup_field = 'id'

    # permisos
    def get_permissions(self):
        if self.action in ['signup', 'login']:
            permission_classes = [AllowAny]
        else:
            permission_classes = [IsAuthenticated, IsCliente, IsClienteUser]
        return [permission() for permission in permission_classes]

    # Serializer según la acción
    def get_serializer_class(self):
        if self.action == 'signup':
            self.serializer_class = ClienteSignUpSerializer

        elif self.action == 'login':
            self.serializer_class = ClienteLogInSerializer

        else:
            self.serializer_class = ClienteModelSerializer
        return self.serializer_class

    # SIGNUP
    """http://127.0.0.1:8000/user/cliente/signup/"""

    @action(detail=False, methods=['POST'])
    def signup(self, request, *args, **kwargs):
        existe_user_facebook = Rol_cliente.objects.filter(
            user__facebook_id=request.data['facebook_id'],
            user__is_client=True).values("id", "user")
        existe_user_normal = Rol_cliente.objects.filter(
            user__identificacion=request.data['identificacion'],
            user__is_client=True).values("id", "user")
        print(existe_user_normal)
        if existe_user_facebook:
            user = User.objects.get(pk=existe_user_facebook[0]['user'])
            cliente_instance = Rol_cliente.objects.get(
                pk=existe_user_normal[0]['id'])
            token, created = Token.objects.get_or_create(user=user)
            data = data = {
                "is_propietario":
                False,
                "is_cliente":
                True,
                "is_contador":
                False,
                "is_empleado":
                False,
                "password_is_temporal":
                user.password_is_temporal,
                "first_name":
                user.first_name,
                "identificacion":
                user.identificacion,
                "phone_number":
                user.phone_number,
                "message":
                "Inicio de sesión exitoso",
                'access_token':
                token.key,
                "id_cliente":
                cliente_instance.id,
                "id_user":
                cliente_instance.user.id,
                "foto":
                "https://mitiendavirtual.s3.sa-east-1.amazonaws.com/static/" +
                str(user.foto)
            }
            return Response(data)
        elif existe_user_normal:
            user = User.objects.get(pk=existe_user_normal[0]['user'])
            token, created = Token.objects.get_or_create(user=user)
            cliente_instance = Rol_cliente.objects.get(
                pk=existe_user_normal[0]['id'])
            data = data = {
                "is_propietario":
                False,
                "is_cliente":
                True,
                "is_contador":
                False,
                "is_empleado":
                False,
                "password_is_temporal":
                user.password_is_temporal,
                "first_name":
                user.first_name,
                "identificacion":
                user.identificacion,
                "phone_number":
                user.phone_number,
                "message":
                "Inicio de sesión exitoso",
                'access_token':
                token.key,
                "id_cliente":
                cliente_instance.id,
                "id_user":
                cliente_instance.user.id,
                "foto":
                "https://mitiendavirtual.s3.sa-east-1.amazonaws.com/static/" +
                str(user.foto)
            }
            return Response(data)
        else:
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            cliente = serializer.save()
            user = User.objects.get(facebook_id=request.data['facebook_id'])
            token, created = Token.objects.get_or_create(user=user)
            cliente_instance = Rol_cliente.objects.get(
                user__identificacion=request.data['identificacion'])
            data = data = {
                "is_propietario":
                False,
                "is_cliente":
                True,
                "is_contador":
                False,
                "is_empleado":
                False,
                "password_is_temporal":
                False,
                "first_name":
                request.data['first_name'],
                "identificacion":
                request.data['identificacion'],
                "phone_number":
                request.data['phone_number'],
                "message":
                "Inicio de sesión exitoso",
                'access_token':
                token.key,
                "id_cliente":
                cliente_instance.id,
                "foto":
                "https://mitiendavirtual.s3.sa-east-1.amazonaws.com/static/" +
                str(user.foto)
            }

            return Response(data)
        ''' data = ClienteSignUpSerializer(cliente).data
        print(data) '''
        # add extra data
        ''' serializer = ClienteLogInSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user, token = serializer.save()
        # cliente
        cliente = Rol_cliente.objects.get(user=user)    
        data_cliente = ClienteModelSerializer(cliente).data
        password_is_temporal = data_cliente['user']['password_is_temporal']
        first_name=data_cliente['user']['first_name']
        identificacion=data_cliente['user']['identificacion']
        phone_number=data_cliente['user']['phone_number']
        data_cliente.pop("user")      
        data = {
            "is_propietario":False,
            "is_cliente":True,
            "is_contador":False,
            "is_empleado": False,
            "password_is_temporal": password_is_temporal,
            "first_name":first_name,
            "identificacion":identificacion,
            "phone_number":phone_number,
            "message":"Inicio de sesión exitoso",     
            'access_token': token,
            "id_cliente": data_cliente['id'],
        } '''
        data = {
            "message": "Cliente creado exitosamente",
            "estado": True,
        }
        return Response(data, status=status.HTTP_201_CREATED)

    # LOGIN
    """http://127.0.0.1:8000/user/propietario/login/ asda"""

    @action(detail=False, methods=['POST'])
    def login(self, request):
        """User sign in."""
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user, token = serializer.save()
        # cliente
        cliente = Rol_cliente.objects.get(user=user)
        data_cliente = ClienteModelSerializer(cliente).data
        password_is_temporal = data_cliente['user']['password_is_temporal']
        first_name = data_cliente['user']['first_name']
        identificacion = data_cliente['user']['identificacion']
        phone_number = data_cliente['user']['phone_number']
        data_cliente.pop("user")
        data = {
            "is_propietario": False,
            "is_cliente": True,
            "is_contador": False,
            "is_empleado": False,
            "password_is_temporal": password_is_temporal,
            "first_name": first_name,
            "identificacion": identificacion,
            "phone_number": phone_number,
            "message": "Inicio de sesión exitoso",
            'access_token': token,
            "id_cliente": data_cliente['id'],
        }
        return Response(data, status=status.HTTP_201_CREATED)

    #RETRIEV
    def retrieve(self, request, *args, **kwargs):
        user = self.get_object()
        cliente = Rol_cliente.objects.get(user=user.user)
        data_cliente = ClienteModelSerializer(cliente).data
        #Datos User
        data_user = data_cliente['user']
        data_cliente.pop('user')
        data_cliente['user'] = data_user['id']
        #Data Establecimientos
        establecimiento = Establecimiento.objects.get(
            pk=data_cliente['establecimiento']['id'])
        data_establecimiento = EstablecimientoModelSerializer(
            establecimiento).data
        data = {
            "user": data_user,
            "cliente": data_cliente,
            "establecimiento": data_establecimiento
        }

        return Response(data)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance,
                                         data=request.data,
                                         partial=partial)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        cliente = self.get_object()
        cliente_serializer = self.get_serializer(cliente)

        return Response(cliente_serializer.data)

    def perform_destroy(self, instance):
        """Disable cliente."""
        instance.is_active = False
        instance.save()

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        data = {
            'estado': "Autorizado",
            'message': "Cliente eliminado con exito"
        }
        return Response(data, status=status.HTTP_200_OK)

    @action(detail=True, methods=['get'])
    def ordenes_compras(self, request, *args, **kwargs):
        instance = self.get_object()
        limit = self.request.query_params.get("limit", None)
        pedido_despachado = self.request.query_params.get(
            "pedido_despachado", None)
        pedido_realizado = self.request.query_params.get(
            "pedido_realizado", None)
        pedido_incompleto = self.request.query_params.get(
            "pedido_incompleto", None)
        pedido_reconfirmado = self.request.query_params.get(
            "pedido_reconfirmado", None)
        pedido_entregado = self.request.query_params.get(
            "pedido_entregado", None)
        pedido_cancelado = self.request.query_params.get(
            "pedido_cancelado", None)
        if limit:
            if bool(pedido_despachado) == True or False:
                ordenes_compras = Orden_compras.objects.filter(
                    cliente=instance.id,
                    pedido_despachado=pedido_despachado)[:int(limit)]
            elif bool(pedido_realizado) == True or False:
                ordenes_compras = Orden_compras.objects.filter(
                    cliente=instance.id,
                    pedido_realizado=pedido_realizado)[:int(limit)]
            elif bool(pedido_incompleto) == True or False:
                ordenes_compras = Orden_compras.objects.filter(
                    cliente=instance.id,
                    pedido_incompleto=pedido_incompleto)[:int(limit)]
            elif bool(pedido_reconfirmado) == True or False:
                ordenes_compras = Orden_compras.objects.filter(
                    cliente=instance.id,
                    pedido_reconfirmado=pedido_reconfirmado)[:int(limit)]
            elif bool(pedido_entregado) == True or False:
                ordenes_compras = Orden_compras.objects.filter(
                    cliente=instance.id,
                    pedido_entregado=pedido_entregado)[:int(limit)]
            elif bool(pedido_cancelado) == True or False:
                ordenes_compras = Orden_compras.objects.filter(
                    cliente=instance.id,
                    pedido_cancelado=pedido_cancelado)[:int(limit)]
            else:
                ordenes_compras = Orden_compras.objects.filter(
                    cliente=instance.id)[:int(limit)]
        else:
            if bool(pedido_despachado) == True or False:
                ordenes_compras = Orden_compras.objects.filter(
                    cliente=instance.id, pedido_despachado=pedido_despachado)
            elif bool(pedido_realizado) == True or False:
                ordenes_compras = Orden_compras.objects.filter(
                    cliente=instance.id, pedido_realizado=pedido_realizado)
            elif bool(pedido_incompleto) == True or False:
                ordenes_compras = Orden_compras.objects.filter(
                    cliente=instance.id, pedido_incompleto=pedido_incompleto)
            elif bool(pedido_reconfirmado) == True or False:
                ordenes_compras = Orden_compras.objects.filter(
                    cliente=instance.id,
                    pedido_reconfirmado=pedido_reconfirmado)
            elif bool(pedido_entregado) == True or False:
                ordenes_compras = Orden_compras.objects.filter(
                    cliente=instance.id, pedido_entregado=pedido_entregado)
            elif bool(pedido_cancelado) == True or False:
                ordenes_compras = Orden_compras.objects.filter(
                    cliente=instance.id, pedido_cancelado=pedido_cancelado)
            else:
                ordenes_compras = Orden_compras.objects.filter(
                    cliente=instance.id)

        lista_ordenes = ViewOrdenCompra(ordenes_compras, many=True).data

        for lista in lista_ordenes:
            try:
                establecimiento_propietario = Establecimiento.objects.get(
                    id=lista['establecimiento'])
                lista[
                    'tienda'] = establecimiento_propietario.empresa.propietario.nombre_comercial
                lista[
                    'slug_name'] = establecimiento_propietario.empresa.propietario.slug_name
                productos_orden = Producto_orden_compra.objects.filter(
                    orden_compras=lista['id'])
                productos_data = ListaOrdenesCompras(productos_orden,
                                                     many=True).data
                lista['productos'] = productos_data
            except Producto_orden_compra.DoesNotExist:
                lista['productos'] = []
        return Response(lista_ordenes)
