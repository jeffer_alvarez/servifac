# Django REST Framework
from rest_framework import mixins, status, viewsets
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
# models
from servifac.users.models import Rol_contador, User
# serializers
from servifac.users.serializers import (UserModelSerializer,
                                        ContadorLogInSerializer,
                                        ContadorModelSerializer,
                                        ContadorSignUpSerializer)


class ContadorViewSet(mixins.ListModelMixin,
                      mixins.DestroyModelMixin,
                      mixins.UpdateModelMixin,
                      mixins.RetrieveModelMixin,
                      viewsets.GenericViewSet):

    queryset = Rol_contador.objects.all()
    lookup_field = 'id'

    # permisos

    def get_permissions(self):
        if self.action in ['signup', 'login']:
            permission_classes = [AllowAny]
        else:
            permission_classes = [IsAuthenticated]
        return [permission() for permission in permission_classes]

    # Serializer según la acción
    def get_serializer_class(self):
        if self.action == 'signup':
            self.serializer_class = ContadorSignUpSerializer

        elif self.action == 'login':
            self.serializer_class = ContadorLogInSerializer

        else:
            self.serializer_class = ContadorModelSerializer
        return self.serializer_class

    # SIGNUP
    """http://127.0.0.1:8000/user/contador/signup/"""
    @action(detail=False, methods=['POST'])
    def signup(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        contador = serializer.save()
        # add extra data
        data = {
            "estado": "autorizado",
            "message": "contador creado exitosamente",
            "objeto": ContadorModelSerializer(contador).data
        }
        return Response(data, status=status.HTTP_201_CREATED)

    # LOGIN
    """http://127.0.0.1:8000/user/contador/login/"""
    @action(detail=False, methods=['POST'])
    def login(self, request):
        """User sign in."""
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user, token = serializer.save()
        data = {
            'user': UserModelSerializer(user).data,
            'access_token': token
        }
        return Response(data, status=status.HTTP_201_CREATED)

    # RETRIEV
    """http://127.0.0.1:8000/user/contador/{username}/"""

    # Update Post and Pach
    """http://127.0.0.1:8000/user/contador/{username}/"""

    # Delete
    def perform_destroy(self, instance):
        """Disable contador."""
        instance.is_active = False
        instance.save()

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        data = {
            'estado': "Autorizado",
            'message': "Contador eliminado con exito"
        }
        return Response(data, status=status.HTTP_204_NO_CONTENT)
