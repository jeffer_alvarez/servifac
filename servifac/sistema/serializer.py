"""Rides serializers."""

# Django REST Framework
from rest_framework import serializers

# Models
from servifac.sistema.models import Producto, ClientesCerveceria, Categoria, Ciudad
# Utilities
from datetime import timedelta
from django.utils import timezone


class ProductosModelSerializer(serializers.ModelSerializer):
    """Productos Sistema model serializer."""
    class Meta:
        """Meta class."""
        model = Producto
        fields = ("id", "descripcion", "categoria", "original", "card",
                  "thumbnail")


class CategoriaModelSerializer(serializers.ModelSerializer):
    """Productos Sistema model serializer."""
    class Meta:
        """Meta class."""
        model = Categoria
        fields = "__all__"


class CiudadModelSerializer(serializers.ModelSerializer):
    """Productos Sistema model serializer."""
    class Meta:
        """Meta class."""
        model = Ciudad
        fields = "__all__"


class ClientesCerveceriaModelSerializer(serializers.ModelSerializer):
    """Productos Sistema model serializer."""
    class Meta:
        """Meta class."""
        model = ClientesCerveceria
        fields = (
            "identi",
            "title",
            "lat",
            "lng",
            "address",
            "phone",
            "country_code",
            "icon",
            "code",
            "delivery",
            "store_url",
            "promo1",
            "promo2",
            "promo3",
        )
