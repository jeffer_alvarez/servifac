"""Empresa Admin models."""

# Django
from django.contrib import admin

# Models
from .models import *


@admin.register(Ciudad)
class CiudadAdmin(admin.ModelAdmin):
    list_display = ('nombre', )
    list_filter = ('nombre', )


@admin.register(Categoria)
class CartegoriaAdmin(admin.ModelAdmin):
    list_display = ('nombre', )
    list_filter = ('nombre', )


@admin.register(Producto)
class ProductoSistemaAdmin(admin.ModelAdmin):
    list_display = (
        'descripcion',
        'categoria',
        'original',
        'card',
        'thumbnail',
    )
    list_filter = ('descripcion', "categoria")
