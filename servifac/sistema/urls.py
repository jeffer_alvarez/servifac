"""Main URLs module."""
from django.urls import path, include
from rest_framework.routers import DefaultRouter
from servifac.sistema import view as sistema_view
router = DefaultRouter()
router.register(r'sistema/productos',
                sistema_view.Productos,
                basename='productos')
router.register(r'sistema/usuarios',
                sistema_view.Usuarios,
                basename='usuarios')
router.register(r'sistema/propietario/comprobar_slug_name',
                sistema_view.SlugName,
                basename='slug_name')
router.register(r'sistema/propietario/identificacion',
                sistema_view.Identificacion,
                basename='identificacion')
router.register(r'sistema/propietario/phone_number',
                sistema_view.PhoneNumber,
                basename='phone_number')
router.register(r'sistema/ciudades',
                sistema_view.Ciudades,
                basename='ciudades')

#devolcer pedido por hash
router.register(r'pedidos/(?P<hash_url>[-a-zA-Z0-9]+)',
                sistema_view.Pedidos,
                basename='pedidos')

#RecuperarPassword
router.register(r'reset_password',
                sistema_view.RecuperarPassword,
                basename='reset_password')

#categorias
router.register(r'sistema/categorias',
                sistema_view.Categorias,
                basename='categorias')

#userlogin
router.register(r'sistema/user_login',
                sistema_view.UserLogin,
                basename='user-login')

#filterEmpresas
router.register(r'sistema/filter_empresas',
                sistema_view.FilterTiendas,
                basename='user-login')
#search pedido hash_url
urlpatterns = [
    # Django Admin
    path('', include(router.urls))
]
