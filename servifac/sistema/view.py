"""Rides views."""
# Utilities
from datetime import timedelta
from django.utils import timezone
# Django REST Framework
from rest_framework import mixins, viewsets, status
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.decorators import action
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from django.conf import settings
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.permissions import AllowAny, IsAuthenticated
import sys
import boto3
from servifac.users.models import Rol_propietario, User, Rol_cliente
from rest_framework.authtoken.models import Token
from django.conf import settings
from django_filters.rest_framework import DjangoFilterBackend

from rest_framework import serializers

#Model Serializers

# Models
from servifac.users.models import Rol_propietario, User, Rol_cliente

# serializers
from servifac.sistema.serializer import ProductosModelSerializer, ClientesCerveceriaModelSerializer, CategoriaModelSerializer, CiudadModelSerializer
from servifac.users.serializers import ConsultaPropietarioModelSerializer, FiltroPropietarioModelSerializer
from servifac.empresa.serializers import ViewOrdenCompra, ProductoOrdenModelSerializer, ListaOrdenesCompras, EmpresaModelSerializer, EmpresaModelSerializerTienda, EstablecimientoModelSerializerTienda, ConfiguracionEmpresaSerializerTienda
from servifac.empresa.models import Establecimiento, Empresa, Orden_compras, Producto_orden_compra, ConfiguracionEmpresa
from servifac.sistema.models import ClientesCerveceria, Producto, Categoria, Ciudad
from django.http import JsonResponse
import random
import string


class Productos(mixins.ListModelMixin, viewsets.GenericViewSet,
                mixins.CreateModelMixin, mixins.RetrieveModelMixin,
                mixins.DestroyModelMixin):
    filter_backends = (SearchFilter, )
    permissions = (AllowAny)
    serializer_class = ProductosModelSerializer
    ordering = ('descripcion', )

    def get_queryset(self):
        queryset = {}
        valor_consulta = self.request.query_params.get("descripcion", None)
        if self.action == 'list':
            if valor_consulta != None:
                queryset = Producto.objects.filter(
                    descripcion__icontains=valor_consulta)[:20]
            else:
                queryset = Producto.objects.all()[:20]

        #username = self.request.query_params.get('username', None)
        return queryset

    def create(self, request, *args, **kwargs):
        data_producto = {}
        descripcion = request.data['descripcion']
        card = request.data['card']
        print(type(card))
        try:
            producto = Producto.objects.get(descripcion=descripcion)
            print("ya existe")
        except Producto.DoesNotExist:
            print("llego nuevo ")
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            producto = serializer.save()
            data_producto = ProductosModelSerializer(producto).data

        return Response(data_producto)


from django.core import exceptions


class SlugName(viewsets.ViewSet):
    def create(self, request):
        identi = "{}".format(request.data['id'])
        existe_pro = ClientesCerveceria.objects.filter(identi=identi).exists()
        if existe_pro:
            data = {"estado": "Existe"}
        else:
            request.data['identi'] = request.data['id']
            request.data.pop('id')
            serializers = ClientesCerveceriaModelSerializer(data=request.data)
            serializers.is_valid(raise_exception=True)
            data_cerveza = serializers.save()
            data_procer = ClientesCerveceriaModelSerializer(data_cerveza).data
            data = {"estado": "Creado"}

        return Response(data)

    def list(self, request):
        valor_consulta = self.request.query_params.get("slug_name", None)
        queryset = Rol_propietario.objects.filter(
            slug_name=valor_consulta).exists()
        if queryset:
            data = {
                "estado": queryset,
                "message": "Nombre de tienda registrada"
            }
        else:
            data = {"estado": queryset, "message": "Tienda disponible"}

        return Response(data)


class PhoneNumber(viewsets.ViewSet):

    serializer_class = EmpresaModelSerializer

    def list(self, request):
        phone_number = self.request.query_params.get("phone_number", None)
        tipo_usuario = self.request.query_params.get("tipo_usuario", None)
        code_pais = self.request.query_params.get("code_pais", None)
        data = {}

        if code_pais == "ec":
            if tipo_usuario == "cliente":
                if len(phone_number) < 13:
                    data = {
                        "estado": True,
                        "phone_number": phone_number,
                        "message": "Su número de teléfono es erroneo"
                    }
                else:
                    phone = "+{}".format(phone_number.strip())
                    phone_number_exist = User.objects.filter(
                        phone_number=phone, is_client=True).exists()
                    if phone_number_exist:
                        data = {
                            "estado": True,
                            "phone_number": phone_number,
                            "message":
                            "El número de teléfono ya esta registrado"
                        }
                    else:
                        data = {
                            "estado": False,
                            "phone_number": phone_number,
                            "message": "Número de telefono correcto"
                        }
            if tipo_usuario == "propietario":
                if len(phone_number) < 13:
                    data = {
                        "estado": True,
                        "phone_number": phone_number,
                        "message": "Su número de teléfono es erroneo"
                    }
                else:
                    phone = "+{}".format(phone_number.strip())
                    phone_number_exist = User.objects.filter(
                        phone_number=phone, is_propietario=True).exists()
                    if phone_number_exist:
                        data = {
                            "estado": True,
                            "phone_number": phone_number,
                            "message":
                            "El número de teléfono ya esta registrado"
                        }
                    else:
                        data = {
                            "estado": False,
                            "phone_number": phone_number,
                            "message": "Número de telefono correcto"
                        }
        if code_pais == "pe":
            if tipo_usuario == "cliente":
                if len(phone_number) < 12:
                    data = {
                        "estado": True,
                        "phone_number": phone_number,
                        "message": "Su número de teléfono es erroneo"
                    }
                else:
                    phone = "+{}".format(phone_number.strip())
                    phone_number_exist = User.objects.filter(
                        phone_number=phone, is_client=True).exists()
                    if phone_number_exist:
                        data = {
                            "estado": True,
                            "phone_number": phone_number,
                            "message":
                            "El número de teléfono ya esta registrado"
                        }
                    else:
                        data = {
                            "estado": False,
                            "phone_number": phone_number,
                            "message": "Número de telefono correcto"
                        }
            if tipo_usuario == "propietario":
                if len(phone_number) < 12:
                    data = {
                        "estado": True,
                        "phone_number": phone_number,
                        "message": "Su número de teléfono es erroneo"
                    }
                else:
                    phone = "+{}".format(phone_number.strip())
                    phone_number_exist = User.objects.filter(
                        phone_number=phone, is_propietario=True).exists()
                    if phone_number_exist:
                        data = {
                            "estado": True,
                            "phone_number": phone_number,
                            "message":
                            "El número de teléfono ya esta registrado"
                        }
                    else:
                        data = {
                            "estado": False,
                            "phone_number": phone_number,
                            "message": "Número de telefono correcto"
                        }

        return Response(data)


class Usuarios(viewsets.ViewSet):
    def list(self, request):
        lugar = self.request.query_params.get("lugar", None)
        usuarios = ClientesCerveceria.objects.filter(address__icontains=lugar)
        usuarios_data = ClientesCerveceriaModelSerializer(usuarios,
                                                          many=True).data
        return Response(usuarios_data)


class UserLogin(viewsets.ViewSet):
    def list(self, request):
        data_empresa = {}
        token_user_auth = self.request.query_params.get("token", None)
        try:
            user_token = user = Token.objects.get(key=token_user_auth).user
            if user_token.is_propietario:
                establecimiento = Establecimiento.objects.get(
                    empresa__propietario=user_token.propietario.id)
                data_empresa = {
                    "estado": True,
                    "sesion_iniciada": True,
                    "is_propietario": True,
                    "is_cliente": False,
                    "is_contador": False,
                    "is_empleado": False,
                    "id_propietario": user_token.propietario.id,
                    "id_cliente": None,
                    "id_user": user_token.id,
                    "id_contador": None,
                    "id_empleado": None,
                    "slug_name": user_token.propietario.slug_name,
                    "canton": establecimiento.canton,
                    "parroquia": establecimiento.parroquia,
                    "ciudad": establecimiento.ciudad,
                    'direccion': establecimiento.direccion,
                    'latitud': establecimiento.latitud,
                    'longitud': establecimiento.longitud,
                    "first_name": user_token.first_name,
                    "identificacion": user_token.identificacion,
                    "phone_number": user_token.phone_number,
                    "tipo_usuario": "propietario",
                }
            if user_token.is_client:
                data_empresa = {
                    "estado":
                    True,
                    "sesion_iniciada":
                    True,
                    "is_propietario":
                    False,
                    "is_cliente":
                    True,
                    "is_contador":
                    False,
                    "is_empleado":
                    False,
                    "id_propietario":
                    None,
                    "id_cliente":
                    user_token.cliente.id,
                    "id_user":
                    user_token.id,
                    "id_contador":
                    None,
                    "id_empleado":
                    None,
                    "slug_name":
                    None,
                    "canton":
                    None,
                    'direccion':
                    None,
                    'latitud':
                    None,
                    'longitud':
                    None,
                    "parroquia":
                    None,
                    "ciudad":
                    None,
                    "first_name":
                    user_token.first_name,
                    "identificacion":
                    user_token.identificacion,
                    "phone_number":
                    user_token.phone_number,
                    "tipo_usuario":
                    "cliente",
                    "foto":
                    "https://mitiendavirtual.s3.sa-east-1.amazonaws.com/static/"
                    + str(user_token.foto)
                }

        except Token.DoesNotExist:
            data_empresa = {
                "estado": False,
                "message": "No hay resultado",
            }
        """ token = "{}".format(token_consulta)
        if token_user_auth == token:
            user = Token.objects.get(key=token).user
            data_empresa['sesion_iniciada']=True
            data_empresa['is_propietario'] = user.is_propietario
            usuario_sesion_iniciada = {
                "identificacion": user.identificacion,
                "tipo_usuario":"propietario",
                "first_name":user.first_name,
                "phone_number":user.phone_number,
            }
            data_empresa['is_cliente']=user.is_client
            data_empresa['is_contador']=user.is_contador
            data_empresa['is_empleado']=user.is_empleado          
        else:
            try:                 
                user = Token.objects.get(key=token_user_auth).user
                try:
                    cliente= Rol_cliente.objects.get(user=user)                
                    data_empresa['cliente_id'] = cliente.id
                    usuario_sesion_iniciada = {
                        "tipo_usuario":"cliente",
                        "identificacion":cliente.user.identificacion,
                        "first_name":cliente.user.first_name,
                        "phone_number":cliente.user.phone_number,
                    }
                    data_empresa['sesion_iniciada']=True
                    data_empresa['is_propietario']=False
                    data_empresa['is_cliente']=True
                    data_empresa['is_contador']=False
                    data_empresa['is_empleado']=False
                except Rol_cliente.DoesNotExist:
                    data_empresa['sesion_iniciada'] = False
                    usuario_sesion_iniciada = {
                        "tipo_usuario":None,
                        "identificacion":None,
                        "first_name":None,
                        "phone_number":None,
                    }
                    data_empresa['is_propietario']=False
                    data_empresa['is_cliente']=False
                    data_empresa['is_contador']=False
                    data_empresa['is_empleado'] = False                
            except Token.DoesNotExist:
                data_empresa['sesion_iniciada']=False
                data_empresa['is_propietario'] = False
                usuario_sesion_iniciada = {
                        "tipo_usuario":None,
                        "identificacion":None,
                        "first_name":None,
                        "phone_number":None,
                    }  """
        return Response(data_empresa)


class Categorias(viewsets.GenericViewSet, mixins.ListModelMixin,
                 mixins.RetrieveModelMixin, mixins.CreateModelMixin):
    permissions = (AllowAny)
    serializer_class = CategoriaModelSerializer

    def get_queryset(self):
        valor_consulta = self.request.query_params.get("nombre", None)
        if self.action == 'list':
            if valor_consulta != None:
                queryset = Categoria.objects.filter(
                    nombre__icontains=valor_consulta)
            else:
                queryset = Categoria.objects.all()
        else:
            queryset = Categoria.objects.all()
        #username = self.request.query_params.get('username', None)
        return queryset

    def create(self, request, *args, **kwargs):
        print(request.session['one'])
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        data_categorias = serializer.save()
        data_catagoria = CategoriaModelSerializer(data_categorias).data
        return Response(data_catagoria)


class Ciudades(viewsets.GenericViewSet, mixins.ListModelMixin,
               mixins.RetrieveModelMixin):
    permissions = (AllowAny)
    serializer_class = CiudadModelSerializer

    def get_queryset(self):
        valor_consulta = self.request.query_params.get("nombre", None)
        code_pais = self.request.query_params.get("code_pais", None)
        if self.action == 'list':
            if valor_consulta != None:
                queryset = Ciudad.objects.filter(
                    nombre__icontains=valor_consulta, code_pais=code_pais)
            else:
                queryset = Ciudad.objects.all()
        else:
            queryset = Ciudad.objects.all()
        #username = self.request.query_params.get('username', None)

        return queryset


class Pedidos(viewsets.GenericViewSet, mixins.ListModelMixin):
    serializer_class = ViewOrdenCompra

    def dispatch(self, request, *args, **kwargs):
        """Verify that the circle exists."""
        hash_url = kwargs['hash_url']
        try:
            self.pedido = Orden_compras.objects.get(hash_url=hash_url)
        except Orden_compras.DoesNotExist:
            data = {"estado": False, "message": "No existe este pedido"}
            return JsonResponse(data)
        return super(Pedidos, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        queryset = Orden_compras.objects.get(hash_url=self.pedido.hash_url)
        return queryset

    def list(self, request, *args, **kwargs):
        instance = self.get_queryset()
        pedido = ViewOrdenCompra(instance).data
        establecimiento = Establecimiento.objects.filter(
            id=pedido['establecimiento']).values("empresa__propietario")
        propietario = [{
            "propietario": data['empresa__propietario']
        } for data in establecimiento]
        pedido['propietario'] = propietario[0]['propietario']
        try:
            pedido['estado'] = True
            productos_orden = Producto_orden_compra.objects.filter(
                orden_compras=pedido['id'])
            productos_data = ListaOrdenesCompras(productos_orden,
                                                 many=True).data
            pedido['productos'] = productos_data
        except Producto_orden_compra.DoesNotExist:
            pedido['productos'] = []
        return Response(pedido)


class Identificacion(viewsets.ViewSet):
    def list(self, request):
        identificacion = self.request.query_params.get("identificacion", None)
        tipo_usuario = self.request.query_params.get("tipo_usuario", None)
        code_pais = self.request.query_params.get("code_pais", None)
        dat = {}
        if code_pais == "ec":
            if tipo_usuario == "cliente":
                results = verificardigitos(identificacion)
                cedula_registrada = Rol_cliente.objects.filter(
                    user__identificacion=identificacion,
                    user__is_client=True).exists()
                if results:
                    if cedula_registrada:
                        data = {
                            "cedula_validada": results,
                            "cedula_registrada": cedula_registrada,
                            "message": "Cédula registrada"
                        }
                    else:
                        data = {
                            "cedula_validada": results,
                            "cedula_registrada": False,
                            "message": "Cédula correcta"
                        }

                elif results == False:
                    if cedula_registrada:
                        data = {
                            "cedula_validada": True,
                            "cedula_registrada": cedula_registrada,
                            "message": "Cédula registrada"
                        }
                    else:
                        data = {
                            "cedula_validada": True,
                            "cedula_registrada": False,
                            "message": "Cédula correcta"
                        }

                elif results == None:
                    data = {
                        "cedula_validada": False,
                        "cedula_registrada": False,
                        "message": "Cédula incorrecta"
                    }

            if tipo_usuario == "propietario":
                cedula_registrada = Rol_propietario.objects.filter(
                    user__identificacion=identificacion,
                    user__is_propietario=True).exists()
                results = verificardigitos(identificacion)
                if results:
                    if cedula_registrada:
                        data = {
                            "cedula_validada": results,
                            "cedula_registrada": cedula_registrada,
                            "message": "Cédula registrada"
                        }
                    else:
                        data = {
                            "cedula_validada": results,
                            "cedula_registrada": False,
                            "message": "Cédula correcta"
                        }
                elif results == False:
                    if cedula_registrada:
                        data = {
                            "cedula_validada": True,
                            "cedula_registrada": cedula_registrada,
                            "message": "Cédula registrada"
                        }
                    else:
                        data = {
                            "cedula_validada": True,
                            "cedula_registrada": False,
                            "message": "Cédula incorrecta"
                        }
                elif results == None:
                    data = {
                        "cedula_validada": False,
                        "cedula_registrada": False,
                        "message": "Cédula incorrecta"
                    }

        elif code_pais == "pe":
            if tipo_usuario == "cliente":
                results = verificardigitosperu(identificacion)
                cedula_registrada = Rol_cliente.objects.filter(
                    user__identificacion=identificacion,
                    user__is_client=True).exists()
                if results:
                    if cedula_registrada:
                        data = {
                            "cedula_validada": results,
                            "cedula_registrada": cedula_registrada,
                            "message": "Cédula registrada"
                        }
                    else:
                        data = {
                            "cedula_validada": results,
                            "cedula_registrada": False,
                            "message": "Cédula correcta"
                        }
                elif results == False:
                    if cedula_registrada:
                        data = {
                            "cedula_validada": True,
                            "cedula_registrada": cedula_registrada,
                            "message": "Cédula registrada"
                        }
                    else:
                        data = {
                            "cedula_validada": True,
                            "cedula_registrada": False,
                            "message": "Cédula correcta"
                        }

                elif results == None:
                    data = {
                        "cedula_validada": False,
                        "cedula_registrada": False,
                        "message": "Cédula incorrecta"
                    }
            if tipo_usuario == "propietario":
                results = verificardigitosperu(identificacion)
                cedula_registrada = Rol_propietario.objects.filter(
                    user__identificacion=identificacion,
                    user__is_propietario=True).exists()
                if results:
                    if cedula_registrada:
                        data = {
                            "cedula_validada": results,
                            "cedula_registrada": cedula_registrada,
                            "message": "Cédula registrada"
                        }
                    else:
                        data = {
                            "cedula_validada": True,
                            "cedula_registrada": False,
                            "message": "Cédula correcta"
                        }
                if results == False:
                    if cedula_registrada:
                        data = {
                            "cedula_validada": True,
                            "cedula_registrada": cedula_registrada,
                            "message": "Cédula registrada"
                        }
                    else:
                        data = {
                            "cedula_validada": True,
                            "cedula_registrada": False,
                            "message": "Cédula correcta"
                        }
                elif results == None:
                    data = {
                        "cedula_validada": False,
                        "cedula_registrada": False,
                        "message": "Cédula incorrecta"
                    }
        return Response(data)


def verificardigitos(nro):
    l = len(nro)
    if l == 10 or l == 13:
        return True
    else:
        return None


def verificardigitosperu(nro):
    l = len(nro)
    if l == 8 or l == 8:
        return True
    else:
        return None


def verificar(nro):
    l = len(nro)
    if l == 10 or l == 13:  # verificar la longitud correcta
        cp = int(nro[0:2])
        if cp >= 1 and cp <= 22:  # verificar codigo de provincia
            tercer_dig = int(nro[2])
            if tercer_dig >= 0 and tercer_dig < 6:  # numeros enter 0 y 6
                if l == 10:
                    return __validar_ced_ruc(nro, 0)
                elif l == 13:
                    return __validar_ced_ruc(nro, 0) and nro[
                        10:
                        13] != '000'  # se verifica q los ultimos numeros no sean 000
            elif tercer_dig == 6:
                return __validar_ced_ruc(nro, 1)  # sociedades publicas
            elif tercer_dig == 9:  # si es ruc
                return __validar_ced_ruc(nro, 2)  # sociedades privadas
            else:
                raise serializers.ValidationError(
                    {"message": "Tercer digito invalido"})
        else:
            raise serializers.ValidationError(
                {"message": "Codigo de provincia incorrecto"})
    else:
        return None


def __validar_ced_ruc(nro, tipo):
    total = 0
    if tipo == 0:  # cedula y r.u.c persona natural
        base = 10
        d_ver = int(nro[9])  # digito verificador
        multip = (2, 1, 2, 1, 2, 1, 2, 1, 2)
    elif tipo == 1:  # r.u.c. publicos
        base = 11
        d_ver = int(nro[8])
        multip = (3, 2, 7, 6, 5, 4, 3, 2)
    elif tipo == 2:  # r.u.c. juridicos y extranjeros sin cedula
        base = 11
        d_ver = int(nro[9])
        multip = (4, 3, 2, 7, 6, 5, 4, 3, 2)
    for i in range(0, len(multip)):
        p = int(nro[i]) * multip[i]
        if tipo == 0:
            total += p if p < 10 else int(str(p)[0]) + int(str(p)[1])
        else:
            total += p
    mod = total % base
    val = base - mod if mod != 0 else 0
    return val == d_ver


class RecuperarPassword(viewsets.GenericViewSet, mixins.ListModelMixin):
    serializer_class = ViewOrdenCompra

    def list(self, request, *args, **kwargs):
        identificacion = self.request.query_params.get("identificacion", None)
        tipo_usuario = self.request.query_params.get("tipo_usuario", None)
        data = {}
        if tipo_usuario == "propietario":
            try:
                user_propietario = User.objects.get(
                    identificacion=identificacion,
                    is_propietario=True,
                    is_active=True)
                password_temporal = ''.join(
                    random.choices(string.ascii_lowercase +
                                   string.ascii_uppercase + string.digits,
                                   k=8))
                user_propietario.set_password(password_temporal)
                user_propietario.password_is_temporal = True
                phone_number = user_propietario.phone_number
                status_sms = enviar_password(password_temporal, phone_number)
                if status_sms == True:
                    user_propietario.save()
                    data = {
                        "estado":
                        True,
                        "message":
                        "Se envio una clave temporal a su numero de telefono"
                    }
                else:
                    data = {"estado": False, "message": "Datos incorrectos"}

                print(status_sms)
                #user_propietario.save()
            except User.DoesNotExist:
                return Response({"estado": False, "message": "error"})

        if tipo_usuario == "cliente":
            try:
                user_propietario = User.objects.get(
                    identificacion=identificacion,
                    is_client=True,
                    is_active=True)
                password_temporal = ''.join(
                    random.choices(string.ascii_lowercase +
                                   string.ascii_uppercase + string.digits,
                                   k=8))
                user_propietario.set_password(password_temporal)
                user_propietario.password_is_temporal = True
                phone_number = user_propietario.phone_number
                status_sms = enviar_password(password_temporal, phone_number)
                if status_sms == True:
                    user_propietario.save()
                    data = {
                        "estado":
                        True,
                        "message":
                        "Se envio una clave temporal a su numero de telefono"
                    }
                else:
                    data = {"estado": False, "message": "Datos incorrectos"}
                #user_propietario.save()
            except User.DoesNotExist:
                return Response({"estado": False, "message": "error"})

        return Response(data)


def enviar_password(password, phone_number):
    print(phone_number)
    print(password)
    client = boto3.client('sns',
                          "us-east-1",
                          aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
                          aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY)
    try:
        client.publish(
            PhoneNumber=phone_number,
            Message="Su clave temporal de mi tiendavirtual.ec es: {}".format(
                password))
        print(client)
    except:
        return False
    return True


class FilterTiendas(viewsets.GenericViewSet, mixins.ListModelMixin):
    filter_backends = (SearchFilter, OrderingFilter, DjangoFilterBackend)
    search_fields = ('ciudad', "empresa__propietario__nombre_comercial")
    filter_fields = ('ciudad', "empresa__propietario__nombre_comercial")
    serializer_class = EstablecimientoModelSerializerTienda

    def get_queryset(self):
        queryset = Establecimiento.objects.all()
        return queryset

    """ def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        ciudad = self.request.query_params.get("identificacion", None)
        tipo_usuario = self.request.query_params.get("tipo_usuario", None)
        tiendas = EstablecimientoModelSerializerTienda(queryset,
                                                       many=True).data
        for tienda in tiendas:
            configuracion_empresa = ConfiguracionEmpresa.objects.get(
                empresa=tienda['empresa']['id'])
            print(configuracion_empresa)
            data_configuracion_empresa = ConfiguracionEmpresaSerializerTienda(
                configuracion_empresa).data
            establecimiento = Establecimiento.objects.get(
                empresa=tienda['empresa']['id'])
            data_establecimiento = EstablecimientoModelSerializerTienda(
                establecimiento).data
            tienda['phone_number'] = tienda['empresa']['propietario']['user'][
                'phone_number']
            #tienda.pop('user')
            #tienda.pop('id')
            tienda['slug_name'] = tienda['empresa']['propietario']['slug_name']
            tienda['logo'] = tienda['empresa']['logo']
            tienda['nombre_comercial'] = tienda['empresa']['propietario'][
                'nombre_comercial']
            tienda['descripcion'] = tienda['empresa']['propietario'][
                'descripcion']
            #tienda['ciudad'] = data_establecimiento['ciudad']
            tienda['servicio_domicilio'] = data_configuracion_empresa[
                'servicio_domicilio']
            tienda['horario_atencion'] = data_configuracion_empresa[
                'horario_atencion']
            tienda.pop('empresa')

        return Response(tiendas) """


""" 

else:
                client = boto3.client('sns',settings.AWS_S3_REGION_NAME ,aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
                aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY)
                client.publish(PhoneNumber='+593985874084',Message='Send sms test api') 
                data = {
                "estado":False,
                "phone_number":phone_number,
                "message":"Número de teléfono valido"
                }    """