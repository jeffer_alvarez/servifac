from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class SistemaConfig(AppConfig):
    name = "servifac.sistema"
    verbose_name = _("Sistema")

    
