"""Model, Contiene a Productos"""

# django
from django.db import models
from django.utils.translation import ugettext_lazy as _

# models
from servifac.utils.models import UtilModel
from servifac.storage_backends import StaticStorage


class Producto(models.Model):
    """Model Productos."""
    descripcion = models.CharField(_("Descripcion"), max_length=250)
    categoria = models.CharField(_("Categoria"), max_length=250)
    original = models.FileField(_("original"),
                                storage=StaticStorage(),
                                max_length=500,
                                blank=True,
                                null=True)
    card = models.FileField(_("card"),
                            storage=StaticStorage(),
                            max_length=500,
                            blank=True,
                            null=True)
    thumbnail = models.FileField(_("thumbnail"),
                                 storage=StaticStorage(),
                                 max_length=500,
                                 blank=True,
                                 null=True)

    def __str__(self):
        return self.descripcion

    class Meta:
        db_table = 'sistema_productos'


class LogsSistema(UtilModel):
    slug_name = models.CharField(_("Slug Name"), max_length=150)
    is_propietario = models.BooleanField(_("Es Propietario"), default=False)
    is_client = models.BooleanField(_("Es Cliente"), default=False)
    is_empleado = models.BooleanField(_("Es Empleado"), default=False)
    is_contador = models.BooleanField(_("Es Contador"), default=False)
    identificacion = models.BooleanField(_("Identificacion"), max_length=13)
    token = models.BooleanField(_("Identificacion"), max_length=13)


class ClientesCerveceria(models.Model):
    identi = models.CharField(_("Identi"),
                              max_length=250,
                              blank=True,
                              null=True)
    title = models.CharField(_("Title"), max_length=250, blank=True, null=True)
    lat = models.CharField(_("Lat"), max_length=250, blank=True, null=True)
    lng = models.CharField(_("Lng"), max_length=250, blank=True, null=True)
    address = models.CharField(_("Address"),
                               max_length=250,
                               blank=True,
                               null=True)
    phone = models.CharField(_("Phone"), max_length=250, blank=True, null=True)
    country_code = models.CharField(_("Country_code"),
                                    max_length=250,
                                    blank=True,
                                    null=True)
    icon = models.CharField(_("Icon"), max_length=250, blank=True, null=True)
    code = models.CharField(_("Code"), max_length=250, blank=True, null=True)
    delivery = models.CharField(_("Delivery"),
                                max_length=250,
                                blank=True,
                                null=True)
    store_url = models.CharField(_("Store_url"),
                                 max_length=250,
                                 blank=True,
                                 null=True)
    promo1 = models.CharField(_("Promo1"),
                              max_length=250,
                              blank=True,
                              null=True)
    promo2 = models.CharField(_("Promo2"),
                              max_length=250,
                              blank=True,
                              null=True)
    promo3 = models.CharField(_("Promo3"),
                              max_length=250,
                              blank=True,
                              null=True)


class Categoria(models.Model):
    nombre = models.CharField(_("Categoria"), max_length=150)


class Ciudad(models.Model):
    nombre = models.CharField(_("ciudad"),
                              max_length=250,
                              blank=True,
                              null=True)
    code_pais = models.CharField(_("codigo pais"),
                                 max_length=250,
                                 blank=True,
                                 null=True)
