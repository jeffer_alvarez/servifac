"""Django models utilitis."""

# django
from django.db import models
# utilities
from django.utils.translation import gettext_lazy as _


class UtilModel(models.Model):
    """ Servifac Model base.
     this class will add.-
        +Created.- is datetime
        +Modified.- datetieme.
    """
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )

    created = models.DateTimeField(
        _("Created at"),
        auto_now_add=True,
        help_text='Date Time on when the object was created'
    )

    modified = models.DateTimeField(
        _("Modified at"),
        auto_now=True,
        help_text='Date Time on when the object was Modified'
    )

    class Meta:
        abstract = True
        get_latest_by = 'created'
        ordering = ['-created', '-modified']
