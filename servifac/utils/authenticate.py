from django.contrib.auth.hashers import check_password

from servifac.users.models import User


def AuthenticatePropietario(username=None, password=None):
    
    try:
        user = User.objects.get(identificacion=username, is_propietario=True)
        if user.check_password(password):
            return user

    except User.DoesNotExist:
        return None 

def AuthenticateCliente(username=None, password=None):
    
    try:
        user = User.objects.get(identificacion=username, is_client=True)
        
        if user.check_password(password):
            return user

    except User.DoesNotExist:
        return None 

def AuthenticateContador(username=None, password=None):
    
    try:
        user = User.objects.get(identificacion=username, is_contador=True)
        
        if user.check_password(password):
            return user

    except User.DoesNotExist:
        return None 
